<?php

require_once 'app/controller/SdsController.php';

class AppController extends SdsController {

    protected $precision = 1000;
    protected $microtime = null;

    public function request() {

        $this->microtime = microtime(true) * $this->precision;
        $config = App::getConfig();
        if($config['logs']['recom']) {
            Log::logArquivo('[0.00000] '.$this->logTags().' REQ:['.$_SERVER['REQUEST_URI'].'] DEB:['.str_replace('\/', '/',json_encode(parent::request()->toArray(), true))."]\n", LOG_REC);
        }
        return parent::request();
    }

    public function response($status, $data, $type = Restful::APPLICATION_JSON) {

        $this->microtime = '['.number_format((((microtime(true) * $this->precision) - $this->microtime)/$this->precision), 5, '.', '').']';
        $config = App::getConfig();
        if($config['logs']['recom']) {
            if($_SERVER['REQUEST_URI'] != '/edicao/post/imagem-banner-app64' && $_SERVER['REQUEST_URI'] != '/edicao/post/imagem-banner64') {
                Log::logArquivo($this->microtime.' '.$this->logTags().' RES:['.$_SERVER['REQUEST_URI'].'] DEB:['.str_replace('\/', '/',json_encode($data, true))."]\n", LOG_REC);
            }
        }

        if(!in_array(strtolower($this->action), $this->actionsNoValidateCsrf)) {
            if(is_array($data)) {
                $data['CsrfToken'] = $this->getRegistry('csrf');
            }
        }

        return parent::response($status, $data, $type);
    }

    public function logApp($message, $debug) {
        Log::logArquivo($this->logTags().' Response:['.$_SERVER['REQUEST_URI'].'] ['.Util::utf8D($message).'] DEB:['.str_replace('\/', '/',json_encode($debug, true))."]\n", LOG_APP);
    }

    protected function _getEnviroment() {

        $config = App::getConfig();
        return $config['db']['db'];
    }

    protected function _getBasesId() {

        return array_flip($this->_BASES[$this->_getEnviroment()]);
    }

    protected function _getBaseUf($uf) {

        $ufs = array_flip($this->_UF);
        return $ufs[strtoupper($uf)];
    }

    protected function _getTiposLoteriaId() {

        $config = App::getConfig();
        return array_flip($this->_TIPO_LOTERIA[$this->_getEnviroment()]);
    }

    protected function _getNomeBaseId($idEmpresa) {

        $bases = $this->_getBasesId();
        return $this->_NOMES[$bases[$idEmpresa]];
    }

    protected function _getNomeBaseNome($base) {

        return $this->_NOMES[$base];
    }

    protected function _getNomeTipoLoteriaId($idTipoLoteria) {

        $bases = $this->_getTiposLoteriaId();
        return $this->_NOMES[$bases[$idTipoLoteria]];
    }

    protected function _getNomeSorteioExtraBaseId($idEmpresa) {

        $bases = $this->_getBasesId();
        return $this->_NOMES_SORTEIO_EXTRA[$bases[$idEmpresa]];
    }

    protected function _getNomeSorteioExtraTipoLoteriaId($idTipoLoteria) {

        $bases = $this->_getTiposLoteriaId();
        return $this->_NOMES_SORTEIO_EXTRA[$bases[$idTipoLoteria]];
    }

    protected function _getBaseIdTipoLoteria($idTipoLoteria) {

        $tipoLoteria = array_flip($this->_TIPO_LOTERIA[$this->_getEnviroment()]);
        return $this->_BASES[$this->_getEnviroment()][$tipoLoteria[$idTipoLoteria]];
    }

    protected function _getNameMarketPlace() {

        $marketPlace = '';
        $deviceInfo = $this->getDevice();
        if(isset($deviceInfo['platform'])) {
            $marketPlace = null;
            switch ($deviceInfo['platform']) {
                case 'ios':
                    $marketPlace = 'Apple Store';
                    break;
                case 'android':
                    $marketPlace = 'Google Play';
                    break;
                default:
                    $marketPlace = 'Google Play';
                    break;
            }
        }
        return $marketPlace;
    }

    public function _checkAppVersion($idEmpresa) {

        $config = App::getConfig();
        $bases = $this->_getBasesId();
        $AppVersion = $this->getVersion();
        $base = strtolower($bases[$idEmpresa]);
        $CurrentVersion = $config['app'][$base.'.currentversion'];
        $AppVersionInt = (int) preg_replace('|[^0-9]+|', '', $AppVersion);
        $CurrentVersionInt = (int) preg_replace('|[^0-9]+|', '', $CurrentVersion);
        return ($AppVersionInt >= $CurrentVersionInt);
    }

    /**
     * Retorna a conexão configurada pro pagseguro.
     *
     * @return array
     */
    public static function ProvedorVenda() {

        $config = App::getConfig();
        return $config['pagseguro'];
    }

    /**
     * Método  gerarSaltAleatorio
     * @access protected
     * @param int $tamanho
     * @return string
     */
    protected function geraSaltAleatorio($tamanho = 6) {
        return substr(sha1(mt_rand()), 0, $tamanho);
    }

    /**
     * Método  senhaAleatoria
     * @access protected
     * @param int $tamanho
     * @param boolean $maiusculas
     * @param boolean $numeros
     * @param boolean $simbolos
     * @return string
     */
    protected function senhaAleatoria($tamanho = 6, $maiusculas = true, $numeros = true, $simbolos = false) {

        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num  = '1234567890';
        $simb = '!@#$%*-';

        $retorno = '';
        $caracteres = '';
        $caracteres.= $lmin;

        if($maiusculas){
            $caracteres .= $lmai;
        }
        if($numeros){
            $caracteres .= $num;
        }
        if($simbolos){
            $caracteres .= $simb;
        }

        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }

        return $retorno;
    }

    /**
     * Método  gerarSenha
     * @access protected
     * @param string $senhaInformada
     * @return string
     */
    protected function gerarSenha($senhaInformada) {

        $senha = $senhaInformada;
        $salt = $this->geraSaltAleatorio();
        $md5 = md5($salt.$senha);
        $senhaGerada = $salt.$md5;
        return $senhaGerada;
    }

    /**
     * Método  validarSenha
     * @access protected
     * @param string $senhaArmazenada
     * @param string $senhaInformada
     * @return string
     */
    protected function validarSenha($senhaArmazenada, $senhaInformada) {

        $salt = substr($senhaArmazenada, 0, 6);
        $md5 = md5($salt.$senhaInformada);
        $senhaGerada = $salt.$md5;
        return ($senhaArmazenada === $senhaGerada);
    }

    protected function validarEdicao($idEmpresa, $idEdicao) {

        $bases = $this->_getBasesId();
        $idTipoLoteria = $this->_TIPO_LOTERIA[$this->_getEnviroment()][strtoupper($bases[$idEmpresa])];

        $dadosSDS = $this->SdsObterEdicao($idTipoLoteria, $idEdicao);
        $edicao = $dadosSDS['dados'];
        $edicao['data_hora_fim'] = Util::formatDate($edicao['data_hora_fim'], true);

        $registro = explode(' ', $edicao['data_hora_fim']);

        $data = explode('/', $registro[0]);
        $tempo = explode(':', $registro[1]);

        list($dia, $mes, $ano) = $data;
        list($hora, $min, $seg) = $tempo;

        $hoje = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
        $data_fim = mktime( $hora, $min, $seg, $mes, $dia, $ano);
        $ret = true;

        if($data_fim < $hoje) {
            $ret = false;
        }

        return $ret;
    }

    protected function idade($data) {

        list($dia, $mes, $ano) = explode('/', $data);
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
        return floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
    }

    protected function cepInformadoCorretamente($cep) {

        return strlen($cep) == 8;
    }

    protected function idadeMinima($data) {

        return ($this->idade($data) >= 16);
    }

    protected function validarRegiao($idEmpresa, $cep) {

        $ret = null;
        $regiao = null;
        $config = App::getConfig();
        $bases = $this->_getBasesId();
        $base = strtolower($bases[$idEmpresa]);

        $resposta = -2;
        $tentativas = 0;
        while($tentativas < 3 && $resposta != 1 && $resposta != -1) {
            $regiao = $this->SdsCep($cep);
            $resposta = $regiao['resposta'];
            $tentativas++;
            sleep(1);
        }

        if($regiao['resposta'] == 1) {
            switch ($config['validar'][$base.'.regiao']) {
                case 1:
                    $ret = trim($regiao['estado']) == trim($this->_UF[$bases[$idEmpresa]]) ? 1 : 0;
                    break;

                case 2:
                    $regioes = $this->municipiosBase($idEmpresa);
                    $cidade  = strtolower(Util::retirarAcentos(Util::utf8D($regiao['cidade'])));
                    $cidade  = str_replace("'", '', $cidade);
                    foreach ($regioes as $regiao) {
                        $cidades[] = strtolower(Util::retirarAcentos($regiao));
                    }
                    $ret = in_array($cidade, $cidades) ? 1 : 0;
                    break;
            }
        } else {
            $ret = $regiao['resposta'];
        }
        return $ret;
    }

    protected function quantidadeComprasEdicao($idEdicao, $idCliente) {

        $ret = $this->SdsObterQuantidadeComprasEdicao($idEdicao, $idCliente);
        return isset($ret['dados'][0]) ? $ret['dados'][0] : array();
    }

    protected function municipiosBase($idEmpresa) {

        $cidades = array();
        $bases = $this->_getBasesId();
        $arquivo = APP_PATH.DS.'cidades'.DS.strtolower($bases[$idEmpresa]).'.json';
        if(file_exists($arquivo)) {
            $cidades = Util::utf8D(json_decode(file_get_contents($arquivo)));
        }
        return $cidades;
    }

    protected function datasDiasDaSemana($diaSemana, $mes, $ano) {

        $ano 	      =  ( $ano       ==    '' ? date('Y') : $ano);
        $mes 	      =  ( $mes       ==    '' ? date('m') : $mes);
        $diaEscolhido =  ( $diaSemana == 'dom' ?       '0' : '6' );

        $i = 1;
        $f = date('t', mktime( 0, 0, 0, $mes, '01', $ano));

        while( $i <= $f ){
            $date = $ano.'-'.$mes.'-'.$i;
            $tstamp = strtotime($date);
            $diaSemana = date("w", $tstamp);
            if($diaSemana == $diaEscolhido) {
                $datas[] = str_pad($i, 2, '0', STR_PAD_LEFT).'/'.str_pad($mes, 2, '0', STR_PAD_LEFT).'/'.$ano;
            }
            $i++;
        }
        return $datas;
    }

    protected function datasExtracoes($idTipoLoteria) {

        $dados = array();
        $ret = $this->SdsObterDatasExtracoes((int) $idTipoLoteria);

        $registros = isset($ret['dados']) ? $ret['dados'] : array();
        foreach ($registros as $registro) {
            $data = explode(' ', $registro['data_hora_sorteio']);
            $data[0] = implode('/', array_reverse(explode('-', $data[0])));
            $dados[] = $data[0];
        }
        return $dados;
    }

    protected function limparNome($nome) {

        //LIMPEZA
        $pattern = '|[^A-ZÁ-Úa-zá-ú\s]+|';
        $nome = preg_replace($pattern, '', $nome);

        $pattern = '|\s{2,}|';
        $nome = preg_replace($pattern, ' ', $nome);

        //FORMATACAO
        $nome = strtolower(Util::lowerAcentos($nome));
        $nome = ucwords($nome);

        $pattern = '/\s([da|de|do]{2,2})s?\s/i';
        preg_match_all($pattern, $nome, $matches, PREG_OFFSET_CAPTURE);

        foreach ($matches[0] as $p) {
            $pattern = '/\s(['.$p[0].']{2,2})s?\s/i';
            $nome = preg_replace($pattern, strtolower($p[0]), $nome);
        }

        return $nome;
    }

    protected function primeiraLetraMaiuscula($nome) {

        //FORMATACAO
        $nome = strtolower(Util::lowerAcentos($nome));
        $nome = ucwords($nome);

        $pattern = '/\s([da|de|do]{2,2})s?\s/i';
        preg_match_all($pattern, $nome, $matches, PREG_OFFSET_CAPTURE);

        foreach ($matches[0] as $p) {
            $pattern = '/\s(['.$p[0].']{2,2})s?\s/i';
            $nome = preg_replace($pattern, strtolower($p[0]), $nome);
        }

        return $nome;
    }

    protected function formatarNome($valor) {

        $tmp = '';
        if(is_array($valor) && count($valor) > 0) {
            $tmp = current($valor);
            if(!empty($tmp)) {
                $tmp = $this->limparNome($tmp);
            }
        } else {
            if(!empty($valor) && strlen($valor) > 0) {
                $tmp = $this->limparNome($valor);
            }
        }
        return $tmp;
    }

    protected function formatarLocalidade($valor) {

        $tmp = '';
        if(is_array($valor) && count($valor) > 0) {
            $tmp = current($valor);
            if(!empty($tmp)) {
                $tmp = $this->primeiraLetraMaiuscula($tmp);
            }
        } else {
            if(!empty($valor) && strlen($valor) > 0) {
                $tmp = $this->primeiraLetraMaiuscula($valor);
            }
        }
        return $tmp;
    }

    protected function encrypt($text) {

        $config = App::getConfig();
        return $this->encryptAES($config['encrypt']['key'], $config['encrypt']['iv'], $text);
    }

    protected function decrypt($text) {

        $config = App::getConfig();
        return trim($this->decryptAES($config['encrypt']['key'], $config['encrypt']['iv'], $text));
    }

    protected function _resizeImage($filename, $newname, $width, $height) {

        $array = explode('.', $filename);
        $extension = strtolower(end($array));
        list($width_orig, $height_orig) = getimagesize($filename);

        $ratio_orig = $width_orig/$height_orig;
        if ($width/$height > $ratio_orig) {
            $width = $height*$ratio_orig;
        } else {
            $height = $width/$ratio_orig;
        }

        $image_p = imagecreatetruecolor($width, $height);
        switch (strtolower($extension)) {
            case 'jpg':
                $image = imagecreatefromjpeg($filename);
                break;
            case 'png':
                $image = imagecreatefrompng($filename);
                break;
        }

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
        imagejpeg($image_p, $newname.'.'.$extension, 100);
    }

    /**
     * Método  _getPage
     * @access protected
     * @param string $url
     * @return string
     */
    protected function _getPage($url) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $ret = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $ret;
    }

    /**
     * Método  _getImage
     * @access protected
     * @param string $url
     * @return string
     */
    protected function _getImage($url) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $ret = curl_exec($curl);
        curl_close($curl);
        return $ret;
    }

    /**
     * Método  _getImage
     * @access protected
     * @param string $url
     * @return string
     */
    protected function _getFile($url) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $ret = curl_exec($curl);
        curl_close($curl);
        return $ret;
    }

    /**
     * Método  _getBanner
     * @access protected
     * @param string $site
     * @return string
     */
    protected function _getBanner($site) {

        $site = $this->_SITES[strtoupper($site)];
        $pagina = $this->_getPage($this->getServerProtocol().'://'.$site.'/');
        preg_match_all("/<ul\s?id=\"slideHome\">(\s+|<[^\/][^u][^l][^>]+>|<\/li>)+<\/ul>/", $pagina, $matches);
        $tag = $matches[0][0];
        $imagem = current(array_slice(explode('<li>', $tag), 1, 1));
        $imagem = substr($imagem, strpos($imagem, '"')+1, strlen($imagem));
        $imagem = substr($imagem, 0, strrpos($imagem, '"'));
        return str_replace(' ', '%20', $site.$imagem);
    }

    protected function _getBannerEdicao($idEdicao, $tipoImagem = 2) {

        $bytes = null;
        $ret = $this->SdsObterBannerEdicao($tipoImagem, $idEdicao);

        $dados = isset($ret['dados'][0]) ? $ret['dados'][0] : array();

        if(!empty($dados)) {
            return utf8_decode($dados['imagem']);
        } else {
            return '';
        }
    }

    protected function _getInfoImagemEdicao($idEdicao, $tipoImagem = 2) {

        $ret = $this->SdsObterBannerEdicao($tipoImagem, $idEdicao);
        $dados = isset($ret['dados'][0]) ? $ret['dados'][0] : array();
        unset($dados['imagem']);
        return $dados;
    }

    protected function _getGanhadores($site, $dataEdicao) {

        $site = $this->_SITES[strtoupper($site)];
        $url  = $this->getServerProtocol().'://'.$site;
        $json = $this->_getPage($url . '/' . $dataEdicao . '/resultado.json');
        $data = json_decode($json, true);
        $ganhadores = $this->ganhadoresFormatados($data, $url, $dataEdicao);

        return $ganhadores;
    }

    protected function ganhadoresFormatados($dados_sorteio, $url, $dataEdicao) {

        $arr = array();
        if (isset($dados_sorteio['rodada_da_sorte'])) {
            $arr = $this->addGiroFormatado($dados_sorteio, $url, $arr, $dataEdicao);
            for ($ds = 1; $ds < $dados_sorteio['numero_premios']; $ds++) {
                $premio = $dados_sorteio['premio' . $ds];
                $arr = $this->addPremioFormatado($arr, $premio, $ds, $url, $dataEdicao);
            }
        } else {
            for ($ds = 1; $ds <= $dados_sorteio['numero_premios']; $ds++) {
                $premio = $dados_sorteio['premio' . $ds];
                $arr = $this->addPremioFormatado($arr, $premio, $ds, $url, $dataEdicao);
            }
        }

        return $arr;
    }

    public function addGiroFormatado($dados_sorteio, $url, $arr, $dataEdicao) {

        $mobileFilePrefix = 'mob_';
        for ($rds = 1; $rds <= $dados_sorteio['rodada_da_sorte']['numero_chances']; $rds++) {
            $rds_chance = $dados_sorteio['rodada_da_sorte']['chance' . $rds];
            for ($grds = 1; $grds <= $rds_chance['numero_ganhadores']; $grds++) {
                $rds_ganhador =  $rds_chance['ganhador' . $grds];
                $item = array();
                $item['nome'] = $rds_ganhador['nome'];
                $item['premio'] = "GIRO DÁ SORTE: " . $dados_sorteio['rodada_da_sorte']['descricao'];
                $item['endereco'] = $rds_ganhador['localidade'];
                if ($rds_ganhador['foto'] != null) {
                    $arquivo = explode('/', $rds_ganhador['foto']);
                    $qtd = count($arquivo);
                    $arquivo = $mobileFilePrefix . $arquivo[$qtd - 1];
                    $item['foto'] = $url . "/". $dataEdicao ."/images/ganhadores/" . $arquivo;
                }
                array_push($arr, $item);
            }
        }

        return $arr;
    }

    public function addPremioFormatado($arr, $premio, $ds, $url, $dataEdicao) {

        $mobileFilePrefix = 'mob_';
        for ($ch = 1; $ch <= $premio['numero_chances']; $ch++) {
            $chance = $premio['chance' . $ch];
            for ($g = 1; $g <= $chance['numero_ganhadores']; $g++) {
                $ganhador = $chance['ganhador' . $g];
                $item = array();
                $item['nome'] = $ganhador['nome'];
                $item['premio'] = $ds . '° Prêmio: ' . $premio['descricao'];
                $item['endereco'] = $ganhador['localidade'];
                if ($ganhador['foto'] != null) {
                    $arquivo = explode('/', $ganhador['foto']);
                    $qtd = count($arquivo);
                    $arquivo = $mobileFilePrefix . $arquivo[$qtd - 1];
                    $item['foto'] = $url . "/". $dataEdicao ."/images/ganhadores/" . $arquivo;
                }
                array_push($arr, $item);
            }
        }

        return $arr;
    }

    public static function mascara($mascara, $string) {

        $separadores = array(',','.','-');
        $mascaraArray = str_split($mascara);
        $stringArray = str_split($string);
        $n = 0;
        for($i = 0; $i < count($mascaraArray); $i++) {
            if(!in_array($mascaraArray[$i], $separadores)) {
                $mascaraArray[$i] = $stringArray[$n++];
            }
        }

        return implode('', $mascaraArray);
    }

    public function getHost($withHttp = true) {

        $path = explode('/', $_SERVER['PHP_SELF']);
        if(count($path) > 2) {
            array_pop($path);
            $host = $_SERVER['HTTP_HOST'].'/'.end($path);
        } else {
            $host = $_SERVER['HTTP_HOST'];
        }
        $host = preg_replace('|([/]+)$|', '', $host);
        $env = $this->_getEnviroment();
        if($env == 'PROD') {
            return ($withHttp ? 'https://' : '').$host;
        } else {
            return ($withHttp ? $this->getServerProtocol().'://' : '').$host;
        }
    }

    public function emailNotificacao($idEmpresa, $idCliente, $titulo, $mensagem) {

        $host  = $this->getHost();
        $bases = $this->_getBasesId();
        $base  = $bases[$idEmpresa];

        $dadosSDS = $this->SdsObterUsuario($idCliente);
        $cliente = $dadosSDS['dados'];

        $smarty = $this->Smarty;
        $html = '';

        $smarty->assign('host', $host);
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('entidade', $this->_EMAIL[$base]['entidade']);
        $smarty->assign('auditoria', $this->_EMAIL[$base]['auditoria']);
        $smarty->assign('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $smarty->assign('saudacao', Util::utf8D('Olá '.$cliente['nome']));
        $smarty->assign('corpo', Util::utf8D($mensagem));
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('site', $this->_SITES[$base]);
        $smarty->assign('rodape', '<span style="font-size:10px">'.Util::utf8D($this->_EMAIL[$base]['texto_base']).'</span>');
        $smarty->assign('extra', '');
        $html = $smarty->fetch(APP_PATH.DS.'templates'.DS.'mensagem.html');

        return $this->sendMail($cliente['id_empresa'], $cliente['email'], $cliente['nome'], Util::utf8D($titulo.' - '.$this->_getNomeBaseNome($base)), $html);
    }

    public function emailAtivacaoConta($idEmpresa, $idCliente, $SparkPost = true) {

        $host = $this->getHost();
        $bases = $this->_getBasesId();
        $base = $bases[$idEmpresa];
        $dadosSDS = $this->SdsObterUsuario($idCliente);
        $cliente = $dadosSDS['dados'];

        $smarty = $this->Smarty;
        $html = '';

        $corpo = '<center style="font-size:20px;font-weight:bold">Bem-vindo ao '.$this->_getNomeBaseNome($base).'!</center><br><br>';
        $corpo.= "Seu cadastro foi realizado com sucesso. Por favor, confirme seu e-mail clicando no link abaixo.<br><br>";

        $hash = Util::base64UrlEncode($this->encrypt("{$cliente['id']}"));
        $link = $host."/cliente/".$hash."/confirmar";

        $corpo.= '<a href="'.$link.'">[Confirmar meu endereço de e-mail]</a><br><br>';
        $corpo.= "Não se preocupe, não compartilhamos seu email com terceiros e não enviamos SPAM!<br><br><br>";

        $smarty->assign('host', $host);
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('entidade', $this->_EMAIL[$base]['entidade']);
        $smarty->assign('auditoria', $this->_EMAIL[$base]['auditoria']);
        $smarty->assign('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $smarty->assign('saudacao', Util::utf8D('Olá '.$cliente['nome']));
        $smarty->assign('corpo', Util::utf8D($corpo));
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('site', $this->_SITES[$base]);
        $smarty->assign('rodape', '<span style="font-size:10px">'.Util::utf8D($this->_EMAIL[$base]['texto_base']).'</span>');
        $smarty->assign('extra', '');
        $html = $smarty->fetch(APP_PATH.DS.'templates'.DS.'mensagem.html');

        return $this->sendMail($cliente['id_empresa'], $cliente['email'], $cliente['nome'], Util::utf8D('Ativação de Conta - '.$this->_getNomeBaseNome($base)), $html, true, $SparkPost);
    }

    public function emailEnviarSenha($idEmpresa, $idCliente, $novaSenha, $SparkPost = true) {

        $host  = $this->getHost();
        $bases = $this->_getBasesId();
        $base  = $bases[$idEmpresa];
        $dadosSDS = $this->SdsObterUsuario($idCliente);
        $cliente = $dadosSDS['dados'];
        $smarty = $this->Smarty;
        $html  = '';

        $corpo = '<center style="font-size:20px;font-weight:bold">Bem-vindo ao '.$this->_getNomeBaseNome($base).'!</center><br><br><br>';
        $corpo.= 'Segue a nova senha solicitada: <span style="font-size:14px;font-weight:bold;font-family:Arial">'.$novaSenha.'</span><br><br>
                  Após acessar o sistema, para sua segurança troque sua senha.<br><br><br>';

        $smarty->assign('host', $host);
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('entidade', $this->_EMAIL[$base]['entidade']);
        $smarty->assign('auditoria', $this->_EMAIL[$base]['auditoria']);
        $smarty->assign('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $smarty->assign('saudacao', Util::utf8D('Olá '.$cliente['nome']));
        $smarty->assign('corpo', Util::utf8D($corpo));
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('site', $this->_SITES[$base]);
        $smarty->assign('rodape', '<span style="font-size:10px">'.Util::utf8D($this->_EMAIL[$base]['texto_base']).'</span>');
        $smarty->assign('extra', '');
        $html = $smarty->fetch(APP_PATH.DS.'templates'.DS.'mensagem.html');

        return $this->sendMail($cliente['id_empresa'], $cliente['email'], $cliente['nome'], Util::utf8D(self::$_TITULOS['TITULO_EMAIL_ENVIO_SENHA'].' - '.$this->_getNomeBaseNome($base)), $html, true, $SparkPost);
    }

    public function dispararEmail($idCliente, $idVenda, $idEdicao, $aviso = '', $SparkPost = true) {

        $dadosSDS = $this->SdsObterUsuario($idCliente);
        $cliente = $dadosSDS['dados'];
        $bases = $this->_getBasesId();
        $base = $bases[$cliente['id_empresa']];
        $smarty = $this->Smarty;
        $html = '';

        $html = $this->gerarHtmlCertificado($cliente['id_empresa'], $idVenda, $idEdicao, $aviso);
        $smarty->assign('host', $this->getHost());
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('entidade', $this->_EMAIL[$base]['entidade']);
        $smarty->assign('auditoria', $this->_EMAIL[$base]['auditoria']);
        $smarty->assign('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $smarty->assign('saudacao', Util::utf8D('Olá, '.$cliente['nome']));
        $corpo = "Seus títulos escolhidos, estão logo abaixo:<br><br>".$html;
        $smarty->assign('corpo', Util::utf8D($corpo));
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('site', $this->_SITES[$base]);
        $smarty->assign('rodape', $this->_CONTATO[$base]['texto_suporte']);
        $hash = Util::base64UrlEncode($this->encrypt($idVenda.'_'.$cliente['id'].'_'.$idEdicao));
        $link = $this->getHost()."/newsletter/".$hash."/visualizar-certificado";
        $texto = 'Não consigo visualizar';
        $visualizacao = '<center><a href="'.$link.'" style="font-size:10px">'.Util::utf8D($texto).'</a></center>';
        $smarty->assign('extra', $visualizacao);
        $html = $smarty->fetch(APP_PATH.DS.'templates'.DS.'mensagem.html');
        $idEmpresa = isset($cliente['id_empresa']) ? $cliente['id_empresa'] : $cliente['idEmpresa'];
        return $this->sendMail($idEmpresa, $cliente['email'], $cliente['nome'], Util::utf8D('Seus Títulos - '.$this->_getNomeBaseNome($base)), $html, true, $SparkPost);
    }

    public function dispararEmailShow($dadosSdsUsuario, $edicao, $aviso = '', $SparkPost = true, $vendas = array()) {
//        $dadosSDS = $this->SdsObterUsuario($idCliente);
        $cliente = $dadosSdsUsuario;
        $bases = $this->_getBasesId();
        $idEmpresa = isset($cliente['idEmpresa']) ? $cliente['idEmpresa'] : $cliente['id_empresa'];
        $base = $bases[$idEmpresa];

        $cliente['idEmpresa'] = $idEmpresa;
        $cliente['id_empresa'] = $idEmpresa;
        $smarty = $this->Smarty;
        $html = '';

        $html = $this->gerarHtmlCertificadoShow($cliente, $edicao, $aviso, false, $vendas);
        $edicao = $edicao['dados'];
        $smarty->assign('host', $this->getHost());
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('entidade', $this->_EMAIL[$base]['entidade']);
        $smarty->assign('auditoria', $this->_EMAIL[$base]['auditoria']);
        $smarty->assign('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $smarty->assign('saudacao', Util::utf8D('Olá, '.$cliente['nome']));
        $corpo = "Seus títulos escolhidos, estão logo abaixo:<br><br>".$html;
        $smarty->assign('corpo', Util::utf8D($corpo));
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('site', $this->_SITES[$base]);
        $smarty->assign('rodape', $this->_CONTATO[$base]['texto_suporte']);
        $hash = Util::base64UrlEncode($this->encrypt($vendas['id_transacao'].'_'.$cliente['id'].'_'.$edicao['id'].'_'.$edicao['id_tipo_loteria']));
        $link = $this->getHost()."/newsletter/".$hash."/visualizar-certificado";
        $texto = 'Não consigo visualizar';
        $visualizacao = '<center><a href="'.$link.'" style="font-size:10px">'.Util::utf8D($texto).'</a></center>';
        $smarty->assign('extra', $visualizacao);
        $html = $smarty->fetch(APP_PATH.DS.'templates'.DS.'mensagem.html');

        return $this->sendMail($idEmpresa, $cliente['email'], $cliente['nome'], Util::utf8D('Seus Títulos - '.$this->_getNomeBaseNome($base)), $html, true, $SparkPost);
    }

    public function resultado($idTipoLoteria, $dataSorteio, $fonte = 'sds') {

        $temp = array();
        $dadosformatados = array();
        $dataSorteio = urldecode($dataSorteio);
        $descricaoTipoSorteio = array(1 => 'Prêmio', 2 => $this->_getNomeSorteioExtraTipoLoteriaId($idTipoLoteria));

        if($fonte == 'sds') {
            $registro = $this->SdsConsultarResultado($idTipoLoteria, implode('-',array_reverse(explode('/', $dataSorteio))));
            if($registro['codigo_execucao'] != 0) {
                throw new Exception(self::$_MENSAGENS['DADOS_SDS']);
            }
            if(!empty($registro['dados']['edicoes'])) {
                foreach ($registro['dados']['edicoes'] as $edicao) {
                    if(!empty($edicao['premios']['premio'])) {
                        foreach ($edicao['premios']['premio'] as $premio) {
                            $TituloSorteio = ($premio['tiposorteio'] == 1 ? $premio['numero'].'º '.$descricaoTipoSorteio[$premio['tiposorteio']] : $descricaoTipoSorteio[$premio['tiposorteio']]);
                            $temp = array('tipo' => $premio['tiposorteio'], 'sorteio' => $TituloSorteio, 'premio' => str_replace('|', '<br>', $premio['descpremio']), 'dezenas' => $premio['combdezenas'],'vencedores' => array());
                            if($premio['vencedores']['qtdtitulos'] == 1) {
                                $vencedor = $premio['vencedores']['titulo'];
                                $temp['vencedores'][] = array('sequencial' => $vencedor['sequencial'], 'numero' => $vencedor['numtitulo'], 'nome' => $this->formatarNome($vencedor['nome']), 'localidade' => $this->formatarLocalidade($vencedor['localidade']));
                            } else {
                                foreach ($premio['vencedores']['titulo'] as $vencedor) {
                                    $temp['vencedores'][] = array('sequencial' => $vencedor['sequencial'], 'numero' => $vencedor['numtitulo'], 'nome' => $this->formatarNome($vencedor['nome']), 'localidade' => $this->formatarLocalidade($vencedor['localidade']));
                                }
                            }
                            $dadosformatados[] = $temp;
                        }
                    }
                }
            }

        } else if($fonte == 'site') {

            $tiposLoteria = $this->_getTiposLoteriaId();
            $site = $this->_SITES[$tiposLoteria[$idTipoLoteria]];
            $registro = $this->SiteConsultarResultado($site, implode('-',array_reverse(explode('/', $dataSorteio))));
            if(count($registro) == 0) {
                throw new Exception(self::$_MENSAGENS['DADOS_NAO_DISPONIVEIS']);
            }
            if(isset($registro['rodada_da_sorte'])) {
                for ($x = 1; $x <= $registro['rodada_da_sorte']['numero_chances']; $x++) {
                    $chance = $registro['rodada_da_sorte']['chance'.$x];
                    $temp = array('tipo' => 2, 'sorteio' => $descricaoTipoSorteio[2], 'premio' => $registro['rodada_da_sorte']['descricao'], 'dezenas' => array(),'vencedores' => array());
                    for ($y = 1; $y <= $chance['numero_ganhadores']; $y++) {
                        $ganhador = $chance['ganhador'.$y];
                        $temp['vencedores'][] = array('sequencial' => $ganhador['numero'], 'numero' => $ganhador['numero'], 'nome' => $this->formatarNome($ganhador['nome']), 'localidade' => $this->formatarLocalidade($ganhador['localidade']));
                    }
                    $dadosformatados[] = $temp;
                }
            }
            for ($i = 1; $i <= ($registro['numero_premios'] - (int) isset($registro['rodada_da_sorte'])); $i++) {
                $premio = $registro['premio'.$i];
                $TituloSorteio = $i.'º '.$descricaoTipoSorteio[1];
                $temp = array('tipo' => 1, 'sorteio' => $TituloSorteio, 'premio' => $premio['descricao'], 'dezenas' => implode('-',$premio['bolas']), 'vencedores' => array());
                for ($j = 1; $j <= $premio['numero_chances']; $j++) {
                    $chance = $premio['chance'.$j];
                    for ($w = 1; $w <= $chance['numero_ganhadores']; $w++) {
                        $ganhador = $chance['ganhador'.$w];
                        $temp['vencedores'][] = array('sequencial' => array(), 'numero' => $ganhador['numero'], 'nome' => $this->formatarNome($ganhador['nome']), 'localidade' => $this->formatarLocalidade($ganhador['localidade']));
                    }
                }
                $dadosformatados[] = $temp;
            }
        }

        return $dadosformatados;
    }

    public function labelValorCertificado($valor) {

        preg_match('/[0-9]+(\,|\.)[0-9]+/', $valor, $matches);
        if(count($matches) > 0) {
            $tmp = explode($matches[1], $valor);
            $ret = array('inteiro' => $tmp[0], 'decimal' => $tmp[1]);
        } else {
            $ret = array('inteiro' => $valor, 'decimal' => '00');
        }
        return $ret;
    }

    public function gerarHtmlCertificado($idEmpresa, $codigoVenda, $idEdicao, $aviso = '', $pdf = false) {

        $bases    = $this->_getBasesId();
        $base     = $bases[$idEmpresa];
        $ret      = $this->SdsObterHtmlCertificado($codigoVenda);
        $registro = $this->SdsConsultarDadosVenda($codigoVenda, $idEdicao);

        $vendaCliente = $ret['dados'][0];
        $vendaCliente['cpf_cliente'] = Formato::cpf($vendaCliente['cpf_cliente']);
        //$vendaCliente['telefone_cliente'] = Formato::foneDDD($vendaCliente['telefone_cliente']);
        $vendaCliente['cpf_cliente'] = 'XXX.XXX'.substr($vendaCliente['cpf_cliente'], -7);
        $vendaCliente['telefone_cliente'] = '(XX) XXXX-'.substr($vendaCliente['telefone_cliente'], -4);
        $vendaCliente['nome_cliente'] = strtoupper(Util::retirarAcentos(Util::utf8D($vendaCliente['nome_cliente'])));

        $registro['dados']['edicao'] = current($registro['dados']['edicao']);
        $edicao = $registro['dados']['edicao'];

        $giro = false;
        foreach ($registro['dados']['premios'] as $premio) {
            if($premio['tipo_sorteio'] == 2) {
                $giro = true;
                break;
            }
        }

        $tipoPremio = 1;
        foreach ($registro['dados']['premios'] as $premio) {
            if($premio['tipo_sorteio'] != 2 && !empty($premio['tipo'])) {
                $tipoPremio = $premio['tipo'];
                break;
            }
        }

        $premiosFixo = null;
        $premiosTabelado = null;

        if($tipoPremio == 2) {
            $temp = array();
            foreach ($registro['dados']['premios'] as $premio) {
                $temp[$premio['valor']][] = $premio;
            }
            $premiosTabelado = $temp;
        } else {
            $premiosFixo = $registro['dados']['premios'];
        }

        $dados = array();
        foreach ($registro['dados']['vendas'] as $dado) {
            $dados[$dado['id_venda']]['id_edicao'] = $dado['id_edicao'];
            $dados[$dado['id_venda']]['edicao_valor'] = $dado['valor_venda'];
            $dados[$dado['id_venda']]['edicao_numero'] = $edicao['numero'];
            $dados[$dado['id_venda']]['edicao_autorizacao'] = $edicao['autorizacao'];
            $dados[$dado['id_venda']]['edicao_data_hora_sorteio'] = $edicao['data_hora_sorteio'];
            $dados[$dado['id_venda']]['id_transacao'] = $dado['transacao_id'];
            $dados[$dado['id_venda']]['status_transacao'] = $dado['status_transacao'];
            $dados[$dado['id_venda']]['data_hora'] = Util::formatDate($dado['data_hora_registro'], true);
            $dados[$dado['id_venda']]['data_sorteio'] = Util::formatDate($dado['data_hora_sorteio']);
            $dados[$dado['id_venda']]['data_hora_sorteio'] = Util::formatDate($dado['data_hora_sorteio'],true);
            $dados[$dado['id_venda']]['vendas'][] = array('sequencial_pai' => $dado['sequencial_pai'], 'numero_pai' => $dado['numero_pai'], 'sequencial' => $dado['sequencial'], 'numero' => $dado['numero'], 'dezenas' => $dado['bolas']);
        }

        $certificados = array();
        foreach ($dados as $key => $dado) {
            foreach ($dados[$key]['vendas'] as $key2 => $dado2) {
                $indice = $dado2['sequencial_pai'];
                $certificados[$indice]['sequencial'] = str_pad($dado2['sequencial_pai'], 6, '0', STR_PAD_LEFT);
                $certificados[$indice]['numero'] = $dado2['numero_pai'];
                unset($dado2['sequencial_pai']);
                unset($dado2['numero_pai']);
                $certificados[$indice]['itens'][] = $dado2;
            }
            $dados[$key]['vendas'] = array_values($certificados);
            $certificados = array();
        }

        $html = '';
        $smarty = $this->Smarty;
        $urlImagens = $this->getHost().'/app/img/';

        $i = 0;
        $template = ($pdf ? 'certificadopdf.html' : 'certificado.html');
        foreach ($dados as $dado) {

            $valoresLabels = array();

            foreach ($dado['vendas'] as $venda) {

                if (isset($valoresLabels[$dado['edicao_valor']])) {
                    $labelValor = $valoresLabels[$dado['edicao_valor']];
                } else {
                    $labelValor = $valoresLabels[$dado['edicao_valor']] = $this->labelValorCertificado($dado['edicao_valor']);
                }

                $smarty->assign('url_entidade', $urlImagens.'email/'.$this->_EMAIL[$base]['entidade']);
                $smarty->assign('url_auditoria',$urlImagens.'email/'.$this->_EMAIL[$base]['auditoria']);
                $smarty->assign('url_logo',$urlImagens.'email/'.$this->_EMAIL[$base]['logo']);
                $smarty->assign('url_edicao',$urlImagens.'edicao/'.$dado['id_edicao'].'.jpg');

                $smarty->assign('site', $this->_SITES[$base]);
                $smarty->assign('tv_local', $this->_EMAIL[$base]['tv_local']);
                $smarty->assign('texto_topo', $this->_EMAIL[$base]['texto_topo']);
                $smarty->assign('texto_base', $this->_EMAIL[$base]['texto_base']);
                $smarty->assign('telefone_resultado', $this->_EMAIL[$base]['telefone_resultado']);
                $smarty->assign('giro', $giro);
                $smarty->assign('valor_centavos', $labelValor['decimal']);
                $smarty->assign('numero', $venda['numero']);
                $smarty->assign('sorteio',$dado['data_sorteio']);
                $smarty->assign('sequencial', $venda['sequencial']);
                $smarty->assign('auditoria', $this->_AUDITORIA[$base]);
                $smarty->assign('valor_inteiro', $labelValor['inteiro']);
                $smarty->assign('edicao', str_pad($dado['edicao_numero'], 3, '0', STR_PAD_LEFT).'º');
                $smarty->assign('autorizacao', 'AUTORIZAÇÃO PROCESSO SUSEP Nº '.str_replace('|', ' e ', $dado['edicao_autorizacao']));
                $smarty->assign('cliente', $vendaCliente);

                //$giroDaSorte = null;
                $premiosNormais = array();

                if(!empty($premiosFixo)) {
                    $premios = $premiosFixo;
                } else {
                    $premios = $premiosTabelado[$dado['edicao_valor']];
                }

                foreach ($premios as $key => $premio) {
                    $temp = explode('|', (!empty($premio['descricao_premio_sorteio']) ? $premio['descricao_premio_sorteio'] : $premio['descricao_premio']));
                    $array = array('numero' => $premio['numero'], 'valor' => $temp[0], 'premio' => implode('<br>',array_slice($temp, 1)));
                    if($premio['tipo_sorteio'] == 1) {
                        $premiosNormais[] = $array;
                    } else {
                        //$giroDaSorte = array('numero' => $premio['numero'], 'valor' => $temp[0], 'premio' => implode('<br>',array_slice($temp, 1)));
                    }
                }
                $smarty->assign('premios', $premiosNormais);

                $combinacoes = array();
                $numerosdasorte = array();
                foreach ($venda['itens'] as $item) {
                    $combinacoes[] = explode('-', $item['dezenas']);
                    $numerosdasorte[] = array('numero' => $item['numero'], 'sequencial' => $item['sequencial']);
                }
                $smarty->assign('combinacoes', $combinacoes);
                $smarty->assign('numerosdasorte', $numerosdasorte);

                $smarty->assign('aviso', $aviso);
                $html.= $smarty->fetch(APP_PATH.DS.'templates'.DS.$template);
                $html.= '<br>';
                if($pdf) {
                    if($i < count($dado['vendas'])) {
                        $html.= '<div class="break">&nbsp;</div>';
                    }
                }
                $i++;
            }
        }
        return $html;
    }

    public function gerarHtmlCertificadoShow($cliente, $dadosEdicao, $aviso = '', $pdf = false, $vendas = array()) {

        $bases    = $this->_getBasesId();
        $base     = $bases[$cliente['idEmpresa']];
        $vendaCliente = $cliente;
        $vendaCliente['cpf_cliente'] = Formato::cpf($vendaCliente['cpf']);
        //$vendaCliente['telefone_cliente'] = Formato::foneDDD($vendaCliente['telefone']);
        $vendaCliente['cpf_cliente'] = 'XXX.XXX'.substr($vendaCliente['cpf'], -5);
        $vendaCliente['telefone_cliente'] = '(XX) XXXX-'.substr($vendaCliente['telefone'], -4);
        $vendaCliente['nome_cliente'] = strtoupper(Util::retirarAcentos(Util::utf8D($vendaCliente['nome'])));
        $edicao = $dadosEdicao['dados'];

        $dados = array();
        foreach ($vendas['vendas'] as $dado) {
            $dados[$dado['id_venda']]['edicao_valor'] = $dado['valor'];
            $dados[$dado['id_venda']]['edicao_numero'] = $edicao['numero'];
            $dados[$dado['id_venda']]['edicao_autorizacao'] = $edicao['autorizacao'];
            $dados[$dado['id_venda']]['edicao_data_hora_sorteio'] = (!empty($dado['data_hora_sorteio'])) ? $dado['data_hora_sorteio'] : "";
            $dados[$dado['id_venda']]['id_transacao'] = (!empty($dado['transacao_id'])) ? $dado['transacao_id'] : "";
            $dados[$dado['id_venda']]['status_transacao'] = (!empty($dado['status_transacao'])) ? $dado['status_transacao'] : "";
            $vendaCliente['data_hora_criacao'] = Util::formatDate($dado['data_hora_venda'], true);
            $dados[$dado['id_venda']]['data_hora'] = Util::formatDate($dado['data_hora_venda'], true);
            $dados[$dado['id_venda']]['data_sorteio'] = Util::formatDate((!empty($dado['data_hora_sorteio'])) ? $dado['data_hora_sorteio'] : "");
            $dados[$dado['id_venda']]['data_hora_sorteio'] = Util::formatDate((!empty($dado['data_hora_sorteio'])) ? $dado['data_hora_sorteio'] : "",true);
            $dados[$dado['id_venda']]['vendas'][] = $dado;
        }

        $certificados = array();

        $html = '';
        $smarty = $this->Smarty;
        $urlImagens = $this->getHost().'/app/img/';

        $i = 0;
        $template = ($pdf ? 'certificadopdf.html' : 'certificado.html');

        foreach ($dados as $dado) {

            $valoresLabels = array();

            foreach ($dado['vendas'] as $venda) {

                if (isset($valoresLabels[$dado['edicao_valor']])) {
                    $labelValor = $valoresLabels[$dado['edicao_valor']];
                } else {
                    $labelValor = $valoresLabels[$dado['edicao_valor']] = $this->labelValorCertificado($dado['edicao_valor']);
                }

                $smarty->assign('url_entidade', $urlImagens.'email/'.$this->_EMAIL[$base]['entidade']);
                $smarty->assign('url_auditoria',$urlImagens.'email/'.$this->_EMAIL[$base]['auditoria']);
                $smarty->assign('url_logo',$urlImagens.'email/'.$this->_EMAIL[$base]['logo']);
                $smarty->assign('url_edicao',$urlImagens.'edicao/'.((!empty($dado['id_edicao'])) ? $dado['id_edicao'] : "").'.jpg');

                $smarty->assign('site', $this->_SITES[$base]);
                $smarty->assign('tv_local', $this->_EMAIL[$base]['tv_local']);
                $smarty->assign('texto_topo', $this->_EMAIL[$base]['texto_topo']);
                $smarty->assign('texto_base', $this->_EMAIL[$base]['texto_base']);
                $smarty->assign('telefone_resultado', $this->_EMAIL[$base]['telefone_resultado']);
                $smarty->assign('giro', false);
                $smarty->assign('valor_centavos', $labelValor['decimal']);
                $smarty->assign('numero', $venda['numero_venda']);
                $smarty->assign('sorteio',$dado['data_sorteio']);
                $sequencial = current($venda['bilhetes']);
                $smarty->assign('sequencial', $sequencial['sequencial']);
                $smarty->assign('auditoria', $this->_AUDITORIA[$base]);
                $smarty->assign('valor_inteiro', $labelValor['inteiro']);
                $smarty->assign('edicao', str_pad($dado['edicao_numero'], 3, '0', STR_PAD_LEFT).'º');
                $smarty->assign('autorizacao', 'AUTORIZAÇÃO PROCESSO SUSEP Nº '.str_replace('|', ' e ', $dado['edicao_autorizacao']));
                $smarty->assign('cliente', $vendaCliente);
                $smarty->assign('data_hora_compra', $dado['data_hora']);

                $premiosNormais = array();

                $premios = array();
                $smarty->assign('premios', $premiosNormais);

                $combinacoes = array();
                $numerosdasorte = array();
                if(!empty($venda['itens'])){
                    foreach ($venda['itens'] as $item) {
                        $combinacoes[] = explode('-', $item['dezenas']);
                        $numerosdasorte[] = array('numero' => $item['numero'], 'sequencial' => $item['sequencial']);
                    }
                }
                $smarty->assign('combinacoes', $combinacoes);
                $smarty->assign('numerosdasorte', $numerosdasorte);

                $smarty->assign('aviso', $aviso);
                
                // Texto Final email
                $strTextoTopo = "Recebemos o seu pedido de aquisição do Título. A participação no sorteio está sujeita à aprovação do pagamento.";
                $strTextoRodape = 'AGUARDANDO CONFIRMAÇÃO DO PAGAMENTO PELA OPERADORA DO CARTÃO';
                if ($aviso == 3 || $aviso == 4) {
                    $strTextoTopo = "";
                    $strTextoRodape = 'PAGAMENTO APROVADO';
                }else if ($aviso == 6 || $aviso == 7) {
                    $strTextoTopo = "PEDIDO CANCELADO. VOCÊ NÃO ESTÁ CONCORRENDO AO SORTEIO.";
                    $strTextoRodape = "Pagamento não aprovado pela operadora do cartão. Entre em contato com sua operadora.";
                }
                // Setando o texto de informativo
                $smarty->assign('texto_informativo_topo', $strTextoTopo);
                $smarty->assign('texto_informativo_rodape', $strTextoRodape);
                // Capitalização
                $strTextoRegulamento = "Título de capitalização da modalidade filantropia premiável de pagamento único emitido pela Aplicap Capitalização S/A, CNPJ: 13.122.801/0001-71. Este título permite a cessão voluntária de 100% do direito de resgate à CHILDFUND BRASIL (CNPJ:17.271.925/000170). Antes de contratar, consulte previamente as Condições Gerais. A aquisição implica automática adesão às Condições Gerais do SHOW DA SORTE de Capitalização, as quais o subscritor declara ter tomado ciência e estão disponíveis em https://institucional.showdasorte.com/normas.html. A aprovação deste plano pela Susep não implica, por parte da Autarquia, em incentivo ou recomendação a sua aquisição, representando, exclusivamente, sua adequação às normas em vigor. É proibida a venda de Título de Capitalização a menores de 16 (dezesseis) anos (Art. 3º do Código Civil). Dúvidas via whatsapp Show dá Sorte: (81) 99414-9023";
                $smarty->assign('texto_informativo_regulamento', $strTextoRegulamento);
                
                $html.= $smarty->fetch(APP_PATH.DS.'templates'.DS.$template);
                $html.= '<br>';
                if($pdf) {
                    if($i < count($dado['vendas'])) {
                        $html.= '<div class="break">&nbsp;</div>';
                    }
                }
                $i++;
            }
        }
        return $html;
    }

    public function gerarHtmlResultado($idEmpresa, $data, $pdf = false, $errata = false) {

        $smarty = $this->Smarty;
        $bases = $this->_getBasesId($idEmpresa);
        $base  = $bases[$idEmpresa];
        $smarty->assign('host', $this->getHost());
        $smarty->assign('logo', $this->_EMAIL[$base]['logo']);
        $smarty->assign('email_base', $this->_CONTATO[$base]['email_base']);
        $smarty->assign('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $smarty->assign('rodape', $this->_CONTATO[$base]['texto_suporte']);
        $template = ($pdf ? 'resultadopdf.html' : 'resultado.html');
        $resultados = $this->resultado($this->_TIPO_LOTERIA[$this->_getEnviroment()][$bases[$idEmpresa]], $data);
        if(!$pdf) {
            $resultados = Util::utf8D($resultados);
        }
        foreach ($resultados as $key => $value) {
            if(isset($value['dezenas']) && !empty($value['dezenas'])) {
                $resultados[$key]['dezenas'] = explode('-', $value['dezenas']);
            }
        }
        $smarty->assign('resultados', $resultados);
        $titulo = '';
        $errataTitulo = '';
        if($errata) {
            $errataTitulo = '<p style="color:red;font-weight:bold;font-size:25px">ERRATA</p>';
        }
        if($pdf) {
            $titulo = $errataTitulo.str_replace('@', $data, self::$_MENSAGENS['NEWSLETTER_RESULTADO']);
        } else {
            $titulo = $errataTitulo.Util::utf8D(str_replace('@', $data, self::$_MENSAGENS['NEWSLETTER_RESULTADO']));
        }
        $smarty->assign('titulo', $titulo);
        return $smarty->fetch(APP_PATH.DS.'templates'.DS.$template);
    }

    public function getTermo($base, $plataform) {

        $arquivo = APP_PATH.DS.'termos'.DS.strtolower($base);
        if(file_exists($arquivo)) {
            $conteudo = file_get_contents($arquivo);
            if($plataform == 'ios') {
                $conteudo = str_replace('{$loja}', 'Apple Store', $conteudo);
            } else {
                $conteudo = str_replace('{$loja}', 'Google Play', $conteudo);
            }
            return $conteudo;
        } else {
            return '';
        }
    }

    public function getAppId($base) {

        $config = App::getConfig();
        if(is_numeric($base)) {
            $bases = $this->_getBasesId();
            $appId = $config['onesignal'][strtolower($bases[$base])];
        } else {
            $appId = $config['onesignal'][strtolower($base)];
        }
        return $appId;
    }

    public function getAdesaoCodigo($base) {

        $config = App::getConfig();
        if(is_numeric($base)) {
            $bases = $this->_getBasesId();
            $appId = $config['assinatura'][strtolower($bases[$base])];
        } else {
            $appId = $config['assinatura'][strtolower($base)];
        }
        return $appId;
    }

    function validarReferenciaShowDaSorte($ret) {
        $retorno = true;
        $arr = explode('_', $ret);

        foreach ($arr as $row) {
            if(!$this->validaValorReferencia($row)) {
                $retorno = false;
            }
        }

        return $retorno;
    }

    public function validaValorReferencia($referencia) {
        $valor = trim($referencia);
        if(empty($valor) ) {
            return false;
        }
        return true;
    }

    public function Kabueta($message, $target) {

        Log::logArquivo($this->logTags().' Request:[http://log01:5000] DEBUG: target='.$target.'&message='.$message."\n", LOG_KABUETA);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://log01:5000');
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS,'target='.$target.'&message='.$message);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $ret = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $ret;
    }

    public function KabuetaMessage($tipo, $titulo, $message, $extra = array(), $target = self::KABUETA_GRUPO_PAGSEGURO) {

        $msg = array();
        $msg[]= 'APP-SVC';
        $msg[]= $this->_KABUETA_ICONS[$tipo]['icon'].': '.Util::retirarAcentos(Util::utf8D($titulo));
        $msg[]= 'Mensagem: '. Util::retirarAcentos(Util::utf8D($message));

        foreach($extra as $k => $v) {
            $msg[]= Util::retirarAcentos(Util::utf8D($k)).': '. Util::retirarAcentos(Util::utf8D($v));
        }
        $send = implode("\n", $msg);
        $send = json_encode($send);
        $arrayUtf = array();
        $arrayIcon = array();
        foreach ($this->_KABUETA_ICONS as $icon) {
            $arrayUtf[] = $icon['utf'];
            $arrayIcon[] = $icon['icon'];
        }
        $send = str_replace($arrayUtf, $arrayIcon, $send);
        $this->Kabueta($send, $target);
    }

    /**
     * Método que irá determinar se é pra enviar email ou não
     */
    public function getEnviarEmailConfirmacao(){
        // Seto como padrão para sempre enviar email confirmação
        $bolRetorno = true;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://config.showdasorte.com/config.php');
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $ret = curl_exec($curl);
        curl_close($curl);
        $LINE = __LINE__ + 1;
        // caso tenha retorno
        if(!empty($ret)){
            // cASO PARA PADRONIZAR ARRAY
            $objRetorno = (array) json_decode($ret);
            // verifico se é pra enviar email ou não
            $bolRetorno = ($objRetorno["emailConfirmacao"] == 1);
        }
        // retornando a informação se deve ou não enviar email
        return $bolRetorno;
    }
}
