<?php

require_once 'lib/ControllerRestful.php';

class ConstantesController extends ControllerRestful {

    const DEV = 'DEV';
    const PRO = 'PROD';
    const STG = 'STAGE';

    const L1  = 'LIVE1';
    const L2  = 'LIVE2';
    const L3  = 'LIVE3';
    const L4  = 'LIVE4';
    const L5  = 'LIVE5';

    const L1_NOTIFICATION  = 'L1';
    const L2_NOTIFICATION  = 'L2';
    const L3_NOTIFICATION  = 'L3';
    const L4_NOTIFICATION  = 'L4';
    const L5_NOTIFICATION  = 'L5';

    protected $_BASES = array (

        self::STG => array(self::L1 =>  69, self::L2 =>  69, self::L3 =>  69, self::L4 => 000, self::L5 => 000),
        self::PRO => array(self::L1 =>  59, self::L2 =>  59, self::L3 =>  59, self::L4 => 59, self::L5 => 59),
        self::DEV => array(self::L1 => 197, self::L2 => 199, self::L3 => 200, self::L4 => 201, self::L5 => 202)
    );

    protected $_TIPO_LOTERIA = array (

        self::STG => array(self::L1 => 410, self::L2 => 450, self::L3 => 430, self::L4 => 000, self::L5 => 000),
        self::PRO => array(self::L1 => 400, self::L2 => 401, self::L3 => 402, self::L4 => 403, self::L5 => 404),
        self::DEV => array(self::L1 => 400, self::L2 => 401, self::L3 => 402, self::L4 => 410, self::L5 => 411)
    );

    protected $_TIPO_LOTERIA_NOTIFICATION = array (

        self::STG => array(self::L1_NOTIFICATION => 410, self::L2_NOTIFICATION => 450, self::L3_NOTIFICATION => 430, self::L4_NOTIFICATION => 000, self::L5_NOTIFICATION => 000),
        self::PRO => array(self::L1_NOTIFICATION => 400, self::L2_NOTIFICATION => 401, self::L3_NOTIFICATION => 402, self::L4_NOTIFICATION => 403, self::L5_NOTIFICATION => 404),
        self::DEV => array(self::L1_NOTIFICATION => 400, self::L2_NOTIFICATION => 401, self::L3_NOTIFICATION => 402, self::L4_NOTIFICATION => 410, self::L5_NOTIFICATION => 411)
    );

    protected $_NOMES = array (

        self::L1 => 'Show Da Sorte',
        self::L2 => 'Show Da Sorte',
        self::L3 => 'Show Da Sorte',
        self::L4 => 'Show Da Sorte',
        self::L5 => 'Show Da Sorte'
    );

    protected $_SITES = array (

        self::L1 => 'www.showdasorte.com',
        self::L2 => 'www.showdasorte.com',
        self::L3 => 'www.showdasorte.com',
        self::L4 => 'www.showdasorte.com',
        self::L5 => 'www.showdasorte.com'
    );

    protected $_DOMINIO = array (

        self::STG => 'mobile-stage.redepos.com.br',
        self::PRO => 'www.doecap.com.br',
        self::DEV => 'mobile-dev.redepos.com.br'
    );

    protected $_UF = array (

        self::L1 => 'LV',
        self::L2 => 'LV',
        self::L3 => 'LV',
        self::L4 => 'LV',
        self::L5 => 'LV'
    );

    protected $_AUDITORIA = array (

        self::L1 => 'aplicap',
        self::L2 => 'aplicap',
        self::L3 => 'aplicap',
        self::L4 => 'aplicap',
        self::L5 => 'aplicap'
    );

    protected $_NOMES_SORTEIO_EXTRA = array (

        self::L1 => 'Giro da Sorte',
        self::L2 => 'Giro da Sorte',
        self::L3 => 'Giro da Sorte',
        self::L4 => 'Giro da Sorte',
        self::L5 => 'Giro da Sorte'
    );

    protected $_CONTATO = array (

        self::L1 => array('email_base' => 'faleconosco@showdasorte.com', 'telefone_base' => '11-5181-6434', 'texto_suporte' => ''),
        self::L2 => array('email_base' => 'faleconosco@showdasorte.com', 'telefone_base' => '11-5181-6434', 'texto_suporte' => ''),
        self::L3 => array('email_base' => 'faleconosco@showdasorte.com', 'telefone_base' => '11-5181-6434', 'texto_suporte' => ''),
        self::L4 => array('email_base' => 'faleconosco@showdasorte.com', 'telefone_base' => '81-99414-9023', 'texto_suporte' => ''),
        self::L5 => array('email_base' => 'faleconosco@showdasorte.com', 'telefone_base' => '81-99414-9023', 'texto_suporte' => '')
    );

    protected $_ESTILOS = array (

        self::L1 => array('uf' => 'lv', 'strCorBarra' => '#5b1472', 'strCorTextoTopo' => '#ffffff', 'strCorTexto' => '#ffffff', 'strCorLink' => '#ffffff', 'strCorFundo' => '#ebe4fb', 'tabs' => array('naoselecionado' => array('corfonte' => 'white', 'corfundo' => '#387ef5'), 'selecionado' => array('corfonte' => 'white', 'corfundo' => 'red'))),
        self::L2 => array('uf' => 'lv', 'strCorBarra' => '#5b1472', 'strCorTextoTopo' => '#ffffff', 'strCorTexto' => '#ffffff', 'strCorLink' => '#ffffff', 'strCorFundo' => '#ebe4fb', 'tabs' => array('naoselecionado' => array('corfonte' => 'white', 'corfundo' => '#387ef5'), 'selecionado' => array('corfonte' => 'white', 'corfundo' => 'red'))),
        self::L3 => array('uf' => 'lv', 'strCorBarra' => '#5b1472', 'strCorTextoTopo' => '#ffffff', 'strCorTexto' => '#ffffff', 'strCorLink' => '#ffffff', 'strCorFundo' => '#ebe4fb', 'tabs' => array('naoselecionado' => array('corfonte' => 'white', 'corfundo' => '#387ef5'), 'selecionado' => array('corfonte' => 'white', 'corfundo' => 'red'))),
        self::L4 => array('uf' => 'lv', 'strCorBarra' => '#5b1472', 'strCorTextoTopo' => '#ffffff', 'strCorTexto' => '#ffffff', 'strCorLink' => '#ffffff', 'strCorFundo' => '#ebe4fb', 'tabs' => array('naoselecionado' => array('corfonte' => 'white', 'corfundo' => '#387ef5'), 'selecionado' => array('corfonte' => 'white', 'corfundo' => 'red'))),
        self::L5 => array('uf' => 'lv', 'strCorBarra' => '#5b1472', 'strCorTextoTopo' => '#ffffff', 'strCorTexto' => '#ffffff', 'strCorLink' => '#ffffff', 'strCorFundo' => '#ebe4fb', 'tabs' => array('naoselecionado' => array('corfonte' => 'white', 'corfundo' => '#387ef5'), 'selecionado' => array('corfonte' => 'white', 'corfundo' => 'red')))
    );

    protected $_EMAIL = array (

        self::L1 => array('entidade' => 'apae.jpg',   'auditoria' => 'aplicap.png', 'logo' => 'lv.png', 'telefone_resultado' => '11-5181-6434', 'texto_topo' => 'Ao adquirir este <br><b>Certificado de Contribuição</b>,<br>você está colaborando<br> com a <b>APAE</b> e concorrendo<br>a valiosos prêmios, Acesse:<br>www.apaebrasil.org.br', 'texto_base' => 'Declaro que recebi, li e entendi o regulamento deste Certificado de Contribuição onde concordo com os seus<br>termos e autorizo, sem ônus, a utilização de meu nome, voz e imagem para divulgação desta campanha.', 'tv_local' => ''),
        self::L2 => array('entidade' => 'anasps.jpg', 'auditoria' => 'aplicap.png', 'logo' => 'lv.png', 'telefone_resultado' => '11-5181-6434', 'texto_topo' => 'Ao adquirir este <br><b>Certificado de Contribuição</b>,<br>você está colaborando<br> com a <b>ANAPPS</b> e concorrendo<br>a valiosos prêmios, Acesse:<br>www.anapps.org.br', 'texto_base' => 'Declaro que recebi, li e entendi o regulamento deste Certificado de Contribuição onde concordo com os seus<br>termos e autorizo, sem ônus, a utilização de meu nome, voz e imagem para divulgação desta campanha.', 'tv_local' => ''),
        self::L3 => array('entidade' => 'childfund.png', 'auditoria' => 'aplicap.png', 'logo' => 'lv.png', 'telefone_resultado' => '11-5181-6434', 'texto_topo' => 'Acesse:<br>www.childfundbrasil.org.br', 'texto_base' => 'Declaro que recebi, li e entendi o regulamento deste Certificado de Contribuição onde concordo com os seus<br>termos e autorizo, sem ônus, a utilização de meu nome, voz e imagem para divulgação desta campanha.', 'tv_local' => ''),
        self::L4 => array('entidade' => 'childfund.png',   'auditoria' => 'aplicap.png', 'logo' => 'lv.png', 'telefone_resultado' => '11-5181-6434', 'texto_topo' => 'Acesse:<br>www.childfundbrasil.org.br', 'texto_base' => 'Declaro que recebi, li e entendi o regulamento deste Certificado de Contribuição onde concordo com os seus<br>termos e autorizo, sem ônus, a utilização de meu nome, voz e imagem para divulgação desta campanha.', 'tv_local' => ''),
        self::L5 => array('entidade' => 'childfund.png',   'auditoria' => 'aplicap.png', 'logo' => 'lv.png', 'telefone_resultado' => '11-5181-6434', 'texto_topo' => 'Acesse:<br>www.childfundbrasil.org.br', 'texto_base' => 'Declaro que recebi, li e entendi o regulamento deste Certificado de Contribuição onde concordo com os seus<br>termos e autorizo, sem ônus, a utilização de meu nome, voz e imagem para divulgação desta campanha.', 'tv_local' => '')
    );

    protected $_TERMOS = array (

        self::L1 => array('privacidade' => array('titulo' => 'Políticas de Privacidade', 'link' => 'app/termos/PE_Politica_de_Privacidade_SHOW_DA_SORTE_12_06_2020_VERSAO_FINAL.pdf'), 'termo' => array('titulo' => 'Termos de Uso da Plataforma', 'link' => 'app/termos/Termos_de_Uso_-_SHOW_DA_SORTE_-_12_06_2020-_VERSAO_FINAL.pdf'), 'regulamento' => array('titulo' => 'Regulamento', 'link' => 'app/termos/Regulamento_-_live_SHOW_DA_SORTE.pdf')),
        self::L2 => array('privacidade' => array('titulo' => 'Políticas de Privacidade', 'link' => 'app/termos/PE_Politica_de_Privacidade_SHOW_DA_SORTE_12_06_2020_VERSAO_FINAL.pdf'), 'termo' => array('titulo' => 'Termos de Uso da Plataforma', 'link' => 'app/termos/Termos_de_Uso_-_SHOW_DA_SORTE_-_12_06_2020-_VERSAO_FINAL.pdf'), 'regulamento' => array('titulo' => 'Regulamento', 'link' => 'app/termos/Regulamento_Show_da_Sorte.pdf')),
        self::L3 => array('privacidade' => array('titulo' => 'Políticas de Privacidade', 'link' => 'app/termos/PE_Politica_de_Privacidade_SHOW_DA_SORTE_12_06_2020_VERSAO_FINAL.pdf'), 'termo' => array('titulo' => 'Termos de Uso da Plataforma', 'link' => 'app/termos/Termos_de_Uso_-_SHOW_DA_SORTE_-_12_06_2020-_VERSAO_FINAL.pdf'), 'regulamento' => array('titulo' => 'Regulamento', 'link' => 'app/termos/Regulamento_-_live_SHOW_DA_SORTE.pdf')),
        self::L4 => array('privacidade' => array('titulo' => 'Políticas de Privacidade', 'link' => 'app/termos/PE_Politica_de_Privacidade_SHOW_DA_SORTE_12_06_2020_VERSAO_FINAL.pdf'), 'termo' => array('titulo' => 'Termos de Uso da Plataforma', 'link' => 'app/termos/Termos_de_Uso_-_SHOW_DA_SORTE_-_12_06_2020-_VERSAO_FINAL.pdf'), 'regulamento' => array('titulo' => 'Regulamento', 'link' => 'app/termos/Regulamento_-_live_SHOW_DA_SORTE.pdf')),
        self::L5 => array('privacidade' => array('titulo' => 'Políticas de Privacidade', 'link' => 'app/termos/PE_Politica_de_Privacidade_SHOW_DA_SORTE_12_06_2020_VERSAO_FINAL.pdf'), 'termo' => array('titulo' => 'Termos de Uso da Plataforma', 'link' => 'app/termos/Termos_de_Uso_-_SHOW_DA_SORTE_-_12_06_2020-_VERSAO_FINAL.pdf'), 'regulamento' => array('titulo' => 'Regulamento', 'link' => 'app/termos/Regulamento_-_live_SHOW_DA_SORTE.pdf'))
    );

    protected $_TERMOS_HORA = array (

        self::L1 => array('tempo'=>1552307450,'texto'=>'Fizemos uma atualização em nossa política de privacidade com o objetivo de deixar mais transparente e segura as regras sobre o uso, armazenamento e tratamento dos dados e demais informações coletadas dos usuários na plataforma (site e aplicativo). </br><strong>Basta clicar em "concordo" para prosseguir.</strong>'),
        self::L2 => array('tempo'=>1536081894,'texto'=>'Fizemos uma atualização em nossa política de privacidade com o objetivo de deixar mais transparente e segura as regras sobre o uso, armazenamento e tratamento dos dados e demais informações coletadas dos usuários na plataforma (site e aplicativo). </br><strong>Basta clicar em "concordo" para prosseguir.</strong>'),
        self::L3 => array('tempo'=>1552307450,'texto'=>'Fizemos uma atualização em nossa política de privacidade com o objetivo de deixar mais transparente e segura as regras sobre o uso, armazenamento e tratamento dos dados e demais informações coletadas dos usuários na plataforma (site e aplicativo). </br><strong>Basta clicar em "concordo" para prosseguir.</strong>'),
        self::L4 => array('tempo'=>1552307450,'texto'=>'Fizemos uma atualização em nossa política de privacidade com o objetivo de deixar mais transparente e segura as regras sobre o uso, armazenamento e tratamento dos dados e demais informações coletadas dos usuários na plataforma (site e aplicativo). </br><strong>Basta clicar em "concordo" para prosseguir.</strong>'),
        self::L5 => array('tempo'=>1552307450,'texto'=>'Fizemos uma atualização em nossa política de privacidade com o objetivo de deixar mais transparente e segura as regras sobre o uso, armazenamento e tratamento dos dados e demais informações coletadas dos usuários na plataforma (site e aplicativo). </br><strong>Basta clicar em "concordo" para prosseguir.</strong>')
    );

    protected $_REDES_SOCIAIS = array (

        self::L1 => array('facebook' => array('user' => '/aldasorte',     'link' => 'https://www.facebook.com/aldasorte'),    'instagram' => array('user' => '@alagoasdasortee', 'link' => 'https://www.instagram.com/alagoasdasortee'), 'twitter' =>  array('user' => '@alagoasdasorte', 'link' => 'https://twitter.com/alagoasdasorte')),
        self::L2 => array('facebook' => array('user' => '/amdasorte',     'link' => 'https://www.facebook.com/amdasorte'),    'instagram' => array('user' => '@amazonasdasorte', 'link' => 'https://www.instagram.com/amazonasdasorte'), 'twitter' =>  array('user' => '@amazonasdasorte','link' => 'https://twitter.com/amazonasdasorte')),
        self::L3 => array('facebook' => array('user' => '/bahiadasorte',  'link' => 'https://www.facebook.com/bahiadasorte'), 'instagram' => array('user' => '@bahiadasorte',    'link' => 'https://www.instagram.com/bahiadasorte'),    'twitter' =>  array('user' => '@BahiaDaSorte',   'link' => 'https://twitter.com/BahiaDaSorte')),
        self::L4 => array('facebook' => array('user' => '/bahiadasorte',  'link' => 'https://www.facebook.com/bahiadasorte'), 'instagram' => array('user' => '@bahiadasorte',    'link' => 'https://www.instagram.com/bahiadasorte'),    'twitter' =>  array('user' => '@BahiaDaSorte',   'link' => 'https://twitter.com/BahiaDaSorte')),
        self::L5 => array('facebook' => array('user' => '/bahiadasorte',  'link' => 'https://www.facebook.com/bahiadasorte'), 'instagram' => array('user' => '@bahiadasorte',    'link' => 'https://www.instagram.com/bahiadasorte'),    'twitter' =>  array('user' => '@BahiaDaSorte',   'link' => 'https://twitter.com/BahiaDaSorte'))
    );

    protected $_REDE_SOCIAL = array (

        self::L1 => array('facebook_pixel' => ''),
        self::L2 => array('facebook_pixel' => ''),
        self::L3 => array('facebook_pixel' => ''),
        self::L4 => array('facebook_pixel' => ''),
        self::L5 => array('facebook_pixel' => ''),
    );

    const KABUETA_GRUPO_PAGSEGURO  = 'smvc_pagseguro';
    const KABUETA_GRUPO_NEWSLETTER = 'newsletter_alert';

    protected $_KABUETA_ICONS = array (

        'OK' => array('icon' => "\xE2\x9C\x85", 'utf' => '\u2705'),
        'AL' => array('icon' => "\xE2\x9A\xA0", 'utf' => '\u26a0'),
        'ER' => array('icon' => "\xE2\x9D\x97", 'utf' => '\u2757')
    );

    protected static $_TITULOS = array (

        'TITULO_EMAIL_ENVIO_SENHA' => 'Solicitação de Nova Senha'
    );

    protected static $_MENSAGENS = array (

        'FALHA_SERVIDOR' => 'Erro interno do Servidor',
        'FALHA_DB' => 'Erro de comunicação com a base de dados',
        'FALHA_ARQUIVO_GRAVACAO' => 'Erro na gravação do arquivo',
        'FALHA_ARQUIVO_LEITURA' => 'Erro na leitura do arquivo',
        'DADOS_SDS' => 'Os dados solicitados ao SDS não estão disponíveis',
        'DADOS_SDS_TRANSACAO' => 'Erro ao concluir operação no SDS',
        'DADOS_NAO_DISPONIVEIS' => 'Os dados solicitados não estão disponíveis',
        'CADASTRO_FALHA' => 'Erro ao cadastrar usuário',
        'CADASTRO_SUCESSO' => 'Usuário cadastrado com sucesso!',
        'CADASTRO_SUCESSO_ATUALIZACAO' => 'Usuário atualizado com sucesso.',
        'CADASTRO_SUCESSO_ATUALIZACAO2' => 'Usuário atualizado com sucesso, recomendamos que:<br>Verifique se o e-mail de ativação chegou na sua caixa de e-mail.',
        'VENDA_CONTA_NAO_ATIVADA' => 'Sua conta ainda não foi ativada, recomendamos que:<br><br>1 - Verifique se o e-mail de ativação chegou na sua caixa de Entrada ou Spam;<br><br>2 - Verifique se o e-mail informado está correto em "Meus Dados", caso esteja errado, atualize-o e um novo e-mail será enviado;<br><br>3 - Caso seus dados estejam corretos, clique no botão atualizar para validar os seus dados.',
        'LOGIN_INVALIDO' => 'Login ou Senha inválidos !',
        'LOGIN_NAO_ATIVADO' => 'Sua conta ainda não foi ativada, enviamos um e-mail para confirmação.',
        'ENVIO_SENHA_SUCESSO' => 'Uma nova senha foi enviada para o e-mail informado.',
        'ENVIO_SENHA_EMAIL_NAO_ENCONTRADO' => 'E-mail não encontrado!',
        'ENVIO_SENHA_EMAIL_SMTP' => 'Erro ao enviar e-mail SMTP',
        'CERTIFICADO_LIBERADOS' => 'Certificados Liberados!',
        'CERTIFICADO_BLOQUEADOS' => 'Certificados Bloqueados!',
        'FALHA_PAGSEGURO_ERROR' => 'Erro de comunicação com o pagseguro',
        'FALHA_DB_UPDATE' => 'Erro no update com a base de dados',
        'IDADE_MINIMA' => 'Idade mínima não permitida para contribuir.',
        'SESSAO' => 'Sessão Criada!',
        'SESSAO_CERTIFICADO_ADICIONADO' => 'Certificado(s) Adicionado(s) na Sessão!',
        'SESSAO_CERTIFICADO_REMOVIDO' => 'Certificado(s) Removido(s) na Sessão!',
        'REGIAO_NAO_PERMITIDA' => 'Infelizmente o @ não está disponível em seu município!',
        'CEP_VAZIO' => 'É necessário preencher o CEP para prosseguir!',
        'CEP_INCORRETO' => 'O CEP informado está incorreto',
        'CEP_NAO_ENCONTRADO' => 'CEP não encontrado!',
        'SERVIDOR_INDISPONIVEL' => 'Servidor Indisponível, tente em alguns segundos.',
        'VERSAO_DESATUALIZADA' => 'A versão @1 do @2 está desatualizada, para continuar participando dos nossos sorteios é necessário atualizar o aplicativo.',
        'VERSAO_DESATUALIZADA2' => 'Este aplicativo do @2 está desatualizado. Para continuar participando do Bahia da Sorte, atualize na @3.',
        'EDICAO_NAO_DISPONIVEL' => 'ESTAMOS PREPARANDO UMA NOVA EDIÇÃO CHEIA DE NOVIDADES PARA VOCÊ. AGUARDE!!',
        'EDICAO_ENCERRADA' => 'Período de Contribuição encerrado',
        'NOME_INCOMPLETO' => 'Informe o nome completo para premiação',
        'CPF_INVALIDO' => 'O CPF informado é inválido',
        'CPF_EM_USO' => 'Usuário já possui cadastrado no sistema, por favor tente realizar o login',
        'EMAIL_INVALIDO' => 'E-mail inválido',
        'EMAIL_EM_USO' => 'E-mail já está em uso',
        'DATA_INVALIDA' => 'Data inválida',
        'NEWSLETTER_RESULTADO' => 'Confira em detalhes o resultado do sorteio do dia @, como premiação, dezenas chamadas e os certificados contemplados.',
        'NEWSLETTER_CADASTRO_EMAIL_EM_USO' => 'Este e-mail já está em uso!',
        'NEWSLETTER_CADASTRO_SUCESSO' => 'Seu e-mail foi cadastrado com sucesso!',
        'NEWSLETTER_CADASTRO_FALHA' => 'Ops!, não foi possível cadastrar seu e-mail',
        'NEWSLETTER_EMAIL_ERRO_ENVIO' => 'Ocorreu um erro inesperado, tente novamente',
        'LIMITE_COMPRA' => 'Por motivos de segurança, a compra via internet está limitada a @ certificados por Pagamento.',
        'VALOR_INVALIDO' => 'Não foi possível calcular o valor total da compra. Por favor tente novamente.',
        'QTD_MINIMA' => 'A quantidade mínima deve ser igual OU maior a 1.'
    );
}