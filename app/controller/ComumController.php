<?php

require_once 'lib/Jwt.php';
require_once 'lib/Csrf.php';
require_once 'lib/Sparkpost.php';
require_once 'lib/ControllerRestful.php';
require_once 'app/controller/ConstantesController.php';

class ComumController extends ConstantesController {

    private static $registry;

    /**
     * @var Jwt
     */
    protected $jwt;

    /**
     * @var Csrf
     */
    protected $csrf;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var array
     */
    protected $actionsNoValidateJwt = array();

    /**
     * @var array
     */
    protected $actionsNoValidateCsrf = array();

    /**
     * @var PHPMailer
     */
    protected $PHPMailer = null;

    /**
     * @var SparkPost
     */
    protected $SparkPost = null;

    /**
     * @var Smarty
     */
    protected $Smarty = null;

    public function __construct() {

        parent::__construct();
    }

    protected function getRegistry($key) {

        $ret = null;
        if(self::$registry == null) {
            self::$registry = array();
        }
        if(isset(self::$registry[$key])) {
            $ret = self::$registry[$key];
        }
        return $ret;
    }

    protected function setRegistry($key, $value) {

        if(self::$registry == null) {
            self::$registry = array();
        }
        self::$registry[$key] = $value;
    }

    protected function _beforeCall() {

        if(!in_array(strtolower($this->action), $this->actionsNoValidateJwt)) {
            $this->isValidJwt();
        }

        if(!in_array(strtolower($this->action), $this->actionsNoValidateCsrf)) {
//            $this->isValidCsrf();
        }
    }

    protected function init() {

        parent::init();
        $this->configCsrf();
        $this->configToken();
        $this->configMailPHPMailer();
        $this->configMailSparkPost();
        $this->configSmarty();
    }

    protected function logTags() {

        $time = Log::date('H:i:s.u', microtime(true));
        $version = $this->getVersion();
        $deviceInfo = $this->getDevice();
        $tags = array();
        $tags[]=  $time;
        $tags[]= 'IP:['.$_SERVER['REMOTE_ADDR'].']';
        if(!empty($version)){
            $tags[]= 'AV:['.$version.']';
        }
        if(!is_array($deviceInfo['manufacturer'])) {
            $tags[]= 'MF:['.ucfirst(strtolower($deviceInfo['manufacturer'])).']';
        }
        $tags[]= 'MO:['.$deviceInfo['model'].']';
        $tags[]= 'OS:['.$deviceInfo['platform'].']';
        $tags[]= 'OV:['.$deviceInfo['version'].']';
        if(isset($deviceInfo['uuid'])) {
            $tags[]= 'UD:['.$deviceInfo['uuid'].']';
            $tags[]= 'CO:['.$deviceInfo['sim']['countrycode'].']';
            $tags[]= 'CA:['.$deviceInfo['sim']['carriername'].']';
        }
        if(isset($deviceInfo['geo'])) {
            $tags[]= 'PO:['.$deviceInfo['geo']['latitude'].','.$deviceInfo['geo']['longitude'].']';
        }
        return implode(' ', $tags);
    }

    public function configToken() {

        $token = App::getConfig('token');
        $this->jwt = new Jwt('JWT', 'HS256', $token['key']);
        $this->jwt->setPayload(Jwt::ISS, $token['iss'])->setPayload(Jwt::SUB, $token['sub']);
    }

    public function configCsrf() {

        $csrf = App::getConfig('csrf');
        if(empty($csrf)){
            $this->csrf = new Csrf($csrf['reqs'], $csrf['time']);
        }
    }

    public function configMailPHPMailer() {

        require_once 'lib/phpmailer/class.phpmailer.php';
        require_once 'lib/phpmailer/class.smtp.php';
        $this->PHPMailer = new PHPMailer();
    }

    public function configMailSparkPost() {

        require_once 'lib/Sparkpost.php';
        $this->SparkPost = new SparkPost();
    }

    public function configSmarty() {

        require_once 'lib/smarty/Smarty.class.php';
        $this->Smarty = new Smarty();
    }

    protected function getDevice() {

        $config = App::getConfig();
        $nameKeyDevice = $config['app']['nameKeyDevice'];
        $device = $this->restful->getHeader($nameKeyDevice);
        if ($device == null || $device == 'null') {
            require_once 'lib/UserAgent.php';
            $UserAgent = new UserAgent();
            $device = array();
            $device['manufacturer'] = $UserAgent->__get('manufacturer');
            $device['manufacturer'] = $UserAgent->__get('manufacturer');
            $device['model'] = $UserAgent->__get('browserName');
            $device['platform'] = $UserAgent->__get('osPlatform');
            $device['version'] = $UserAgent->__get('osVersion');
        } else {
            $device = json_decode($device, true);
        }
        return $device;
    }

    protected function getVersion() {

        $config = App::getConfig();
        $nameKeyVersion = $config['app']['nameKeyAppVersion'];
        $version = $this->restful->getHeader($nameKeyVersion);
        if(empty($version)) {
            $request = $this->restful->request()->toArray();
            if(isset($request[$nameKeyVersion]) && !empty($request[$nameKeyVersion])) {
                $version = $request[$nameKeyVersion];
            }
        }
        return $version;
    }

    protected function getToken() {

        $config = App::getConfig();
        $nameKeyToken = $config['token']['nameKeyToken'];
        $token = $this->restful->getHeader($nameKeyToken);
        if(empty($token)) {
            $request = $this->restful->request()->toArray();
            if(isset($request[$nameKeyToken]) && !empty($request[$nameKeyToken])) {
                $token = $request[$nameKeyToken];
            }
        }
        return $token;
    }

    protected function getCsrf() {

        $config = App::getConfig();
        $nameKeyCsrf = $config['csrf']['nameKeyCsrf'];
        $csrf = $this->restful->getHeader($nameKeyCsrf);
        if(empty($csrf)) {
            $request = $this->restful->request()->toArray();
            if(isset($request[$nameKeyCsrf]) && !empty($request[$nameKeyCsrf])) {
                $csrf = $request[$nameKeyCsrf];
            }
        }
        return $csrf;
    }

    public function isValidJwt() {

        $status = null;
        $codigo = null;
        $mensagem = null;
        $token = $this->getToken();

        if(!empty($token)) {

            $tokenStatus = $this->jwt->verify($token);

            switch ($tokenStatus) {
                case Jwt::TKEXP:
                    $status = self::HTTP_UNAUTHORIZED;
                    $codigo = self::HTTP_UNAUTHORIZED.'.'.Jwt::TKEXP;
                    $mensagem = Util::utf8D('Sessão Expirada');
                break;
                case Jwt::TKNBF:
                    $status = self::HTTP_OK;
                    $codigo = self::HTTP_UNAUTHORIZED.'.'.Jwt::TKNBF;
                    $mensagem = Util::utf8D('Sessão não Iniciada');
                break;
                case Jwt::TKSIG:
                    $status = self::HTTP_OK;
                    $codigo = self::HTTP_UNAUTHORIZED.'.'.Jwt::TKSIG;
                    $mensagem = Util::utf8D('Sessão Inválida');
                break;
            }

        } else {

            $status = self::HTTP_OK;
            $codigo = self::HTTP_UNAUTHORIZED.'.4';
            $mensagem = Util::utf8D('Sessão não Autorizada');
        }

        if(!empty($status)) {
            die($this->restful->response($status, array('status' => $status, 'message' => Util::utf8E($mensagem), 'codigo' => $codigo)));
        }
    }

    public function isValidCsrf() {

        $status = null;
        $codigo = null;
        $mensagem = null;
        $dadosSDS = $this->SdsLerDocumentoTransacao('iniciada', $this->getCsrf());
        $csrf = $dadosSDS['dados'];

        if(!empty($csrf)) {

            $this->csrf->setToken($csrf);
            if(!$this->csrf->verify()) {
                 $status = self::HTTP_OK;
                 $codigo = self::HTTP_UNAUTHORIZED;
                 $mensagem = Util::utf8D('Requisição não Autorizada');
             } else {
                 $this->csrf->create();
                 $this->setRegistry('csrf', $this->csrf->getId());
                 $this->SdsCriarDocumentoTransacao('iniciada', $this->csrf->getId(), $this->csrf->toJson());
             }

        } else {

            $status = self::HTTP_OK;
            $codigo = self::HTTP_UNAUTHORIZED;
            $mensagem = Util::utf8D('Requisição não Autorizada');
        }

        if(!empty($status)) {
            die($this->restful->response($status, array('status' => $status, 'message' => Util::utf8E($mensagem), 'codigo' => $codigo)));
        }
    }

    public function getTokenPayload($key = null) {

        $data = null;
        $token = $this->getToken();
        if(!empty($token)) {
            $parts = explode('.', $token);
            $tmp = Util::utf8D(json_decode(base64_decode($parts[1]), true));
            if(!empty($key)) {
                $data = $tmp[$key];
            } else {
                $data = $tmp;
            }
        }
        return $data;
    }

    public function sendMail($idEmpresa, $email, $name, $subject, $body, $ishtml = true, $SparkPost = true) {

        if($SparkPost) {

            $config = App::getConfig();
            $domain = trim(substr($email, strpos($email, '@')+1, strlen($email)));

            if(in_array($domain, $config['phpmailerdomains'])) {
                $retorno = $this->sendMailPHPMailer($idEmpresa, $email, $name, $subject, Util::utf8E($body), $ishtml);
                if(isset($retorno['errors'])) {
                    $retorno = $this->sendMailSparkPost($idEmpresa, $email, $name, $subject, $body);
                }
                return $retorno;
            } else {
                switch ($config['email']['metodo']) {
                    case 'phpmailer':
                        return $this->sendMailPHPMailer($idEmpresa, $email, $name, $subject, Util::utf8E($body), $ishtml);
                    break;
                    case 'sparkpost':
                        return $this->sendMailSparkPost($idEmpresa, $email, $name, $subject, $body);
                    break;
                    default:
                        return $this->sendMailSparkPost($idEmpresa, $email, $name, $subject, $body);
                    break;
                }
            }

        } else {

            $retorno = $this->sendMailPHPMailer($idEmpresa, $email, $name, $subject, Util::utf8E($body), $ishtml);
            if(isset($retorno['errors'])) {
                $retorno = $this->sendMailSparkPost($idEmpresa, $email, $name, $subject, $body);
            }
            return $retorno;
        }
    }

    public function sendMailTracking($idEmpresa, $email, $name, $subject, $body, $ishtml = true) {

        $config = App::getConfig();
        switch ($config['email']['metodo']) {
            case 'phpmailer':
                return $this->sendMailPHPMailerTracking($idEmpresa, $email, $name, $subject, $body, $ishtml);
            break;
            case 'sparkpost':
                return $this->sendMailSparkPostTracking($idEmpresa, $email, $name, $subject, $body);
            break;
            default:
                return $this->sendMailSparkPostTracking($idEmpresa, $email, $name, $subject, $body);
            break;
        }
    }

    public function sendMailPHPMailer($idEmpresa, $email, $name, $subject, $body, $ishtml = true) {

        $config = App::getConfig();
        $bases = $this->_getBasesId();
        $base = strtolower($bases[$idEmpresa]);

        $this->PHPMailer->IsSMTP();
        $this->PHPMailer->SMTPAuth   = $config['phpmailer'][$base.'.smtpauth'];
        $this->PHPMailer->SMTPSecure = $config['phpmailer'][$base.'.smtpsecure'];

        if(!$config['phpmailer'][$base.'.smtpverifyssl']) {
            $this->PHPMailer->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => false
                )
            );
        }

        $this->PHPMailer->CharSet  = 'UTF-8';
        $this->PHPMailer->Host 	   = $config['phpmailer'][$base.'.host'];
        $this->PHPMailer->Port 	   = $config['phpmailer'][$base.'.port'];
        $this->PHPMailer->Username = $config['phpmailer'][$base.'.username'];
        $this->PHPMailer->Password = $config['phpmailer'][$base.'.password'];
        if(!empty($config['phpmailer'][$base.'.sender'])) {
            $this->PHPMailer->From = $config['phpmailer'][$base.'.sender'];
        } else {
            $this->PHPMailer->From = $config['phpmailer'][$base.'.username'];
        }
        $this->PHPMailer->FromName = $config['phpmailer'][$base.'.from'];

        $this->PHPMailer->IsHTML($ishtml);
        $this->PHPMailer->AddAddress($email, $name);
        $this->PHPMailer->Subject = Util::utf8E($subject);
        $this->PHPMailer->Body = $body;
        $return = $this->PHPMailer->send();
        $this->PHPMailer->clearAddresses();
        if(!$return) {
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: PhpMailler => '.$this->PHPMailer->ErrorInfo."\n", LOG_EMAIL);
            $return = array('errors' => $this->PHPMailer->ErrorInfo);
        } else {
            $return = array('errors' => null);
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: PhpMailler => OK'."\n", LOG_EMAIL);
        }
        return $return;
    }

    public function sendMailPHPMailerTracking($idEmpresa, $email, $name, $subject, $body, $ishtml = true) {

    }

    public function sendMailSparkPost($idEmpresa, $email, $name, $subject, $body) {

        $config = App::getConfig();
        $bases = $this->_getBasesId();
        $base = strtolower($bases[$idEmpresa]);

        $this->SparkPost->setKey($config['sparkpost'][$base.'.key']);
        $return = json_decode($this->SparkPost->send(Util::utf8D($config['sparkpost'][$base.'.nome']), $config['sparkpost'][$base.'.email'], $name, $email, $subject, $body), true);

        if(isset($return['errors'])) {
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: SparkPost => '.json_encode($return['errors'])."\n", LOG_EMAIL);
        } else {
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: SparkPost => OK'."\n", LOG_EMAIL);
        }

        return $return;
    }

    public function sendMailSparkPostTracking($idEmpresa, $email, $name, $subject, $body) {

        $config = App::getConfig();
        $bases = $this->_getBasesId();
        $base = strtolower($bases[$idEmpresa]);

        $this->SparkPost->setKey($config['sparkpost'][$base.'.tracking.key']);
        $return = json_decode($this->SparkPost->send(Util::utf8D($config['sparkpost'][$base.'.tracking.nome']), $config['sparkpost'][$base.'.tracking.email'], $name, $email, $subject, $body), true);

        if(isset($return['errors'])) {
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: SparkPost => '.json_encode($return['errors'])."\n", LOG_EMAIL_TRACKING);
        } else {
            Log::logArquivo($this->logTags().' Request:['.$_SERVER['REQUEST_URI'].'] Email:['.$email.'] DEBUG: SparkPost => OK'."\n", LOG_EMAIL_TRACKING);
        }

        return $return;
    }

    public function encryptAES($key, $iv, $text) {

        $key = md5($key);
        $iv  = utf8_encode($iv);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);
    }

    public function decryptAES($key, $iv, $text) {

        $key = md5($key);
        $iv  = utf8_encode($iv);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);
        return utf8_decode($decrypted);
    }

    public function cURL($url, $data = null, $header = null , $customRequest = null) {

        $curl = curl_init();

        if(!empty($customRequest)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $customRequest);
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if($header != null){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        if($data != null && is_array($data)) {

            $data_string = array();
            foreach($data as $key=>$value) {
                $data_string[] = $key.'='.$value;
            }

            $data_string = implode('&',$data_string);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        } elseif(is_string($data)) {

            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        $ret = curl_exec($curl);
        curl_close($curl);

        return $ret;
    }

    public function isValidRequest($identifier, $time = 10) {

        require_once 'lib/Cache.php';
        $path = PATH.DS.'transacoes'.DS.'request';
        $Cache = new Cache($identifier, $path, $time);
        $ret = false;
        if($Cache->expired()) {
            $ret = true;
            $Cache->set('identifier', $identifier);
        }
        return $ret;
    }

    /**
     * Retorna um identificador único
     *
     * @access public
     * @return string
     */
    public function guid() {

        return Util::guid();
    }

    /**
     * Método GET
     * @access public
     * @param string $string
     * @return string
     * @example http://localhost/pedasortesvc/inicio
     */
    public function getAction() {

        $status = self::HTTP_OK;
        $this->response($status, array('message' => $this->restful->getHttpStatusMessage($status), 'data' => 'GET'));
    }

    /**
     * Método POST
     * @access public
     * @param string $string
     * @return string
     * @example http://localhost/pedasortesvc/inicio
     */
    public function postAction() {

        $status = self::HTTP_OK;
        $this->response($status, array('message' => $this->restful->getHttpStatusMessage($status), 'data' => 'POST'));
    }

    /**
     * Método PUT
     * @access public
     * @param string $string
     * @return string
     * @example http://localhost/pedasortesvc/inicio
     */
    public function putAction() {

        $status = self::HTTP_OK;
        $this->response($status, array('message' => $this->restful->getHttpStatusMessage($status), 'data' => 'PUT'));
    }

    /**
     * Método DELETE
     * @access public
     * @param string $string
     * @return string
     * @example http://localhost/pedasortesvc/inicio
     */
    public function deleteAction() {

        $status = self::HTTP_OK;
        $this->response($status, array('message' => $this->restful->getHttpStatusMessage($status), 'data' => 'DELETE'));
    }

    /**
     * Método sqlAll
     * @access public
     * @param string $string
     * @return array
     */
    public function sqlAll($sql) {

        return App::Db()->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Método sqlRow
     * @access public
     * @param string $string
     * @return array
     */
    public function sqlRow($sql) {

        return App::Db()->query($sql)->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Método bindRow
     * @access public
     * @param string $string
     * @return array
     */
    public function bindRow($sql, $binds) {

        return App::Db()->bind($sql, $binds)->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Método sqlExe
     * @access public
     * @param string $string
     */
    public function sqlExe($sql) {

        App::Db()->exec($sql);
    }
}

function XmlToJson ($content) {

    $content = str_replace(array("\n", "\r", "\t"), '', $content);
    $content = trim(str_replace('"', "'", $content));
    $simpleXml = simplexml_load_string($content);
    $json = json_encode($simpleXml);
    return $json;
}

function arrayChangeKeyCase($arr) {

    if(is_array($arr)) {
        return array_change_key_case_recursive($arr);
    } else {
        return $arr;
    }
}

function array_change_key_case_recursive($arr) {

    return array_map(function($item) {
        if(is_array($item))
            $item = array_change_key_case_recursive($item);
            return $item;
    },array_change_key_case($arr));
}
