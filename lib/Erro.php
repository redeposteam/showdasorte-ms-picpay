<?php

/**
 * Tratamento de Erro.
 */

/**
 * Responsável por manipular erros no sistema.
 * 
 * @name		Erro
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
*/
class Erro {

	/**
	 * @var string Data de geração do erro
	 */
    private $data;
    /**
     * @var string Tipo do erro
     */
    private $tipo;
    /**
     * @var string Código do erro
     */
    private $numeroErro;
    /**
     * @var string Mensagem do Erro
     */
    private $mensagemErro;
    /**
     * @var string Nome do arquivo onde ocorreu o erro
     */
    private $nomeArquivo;
    /**
     * @var string Número da linha do arquivo onde ocorreu o erro
     */
    private $numeroLinha;
    /**
     * @var string Variáveis utilizadas durante o erro
     */
    private $variaveis;
    /**
     * @var array|string Erros rastreáveis
     */
    private $errosRastreaveis;

    /**
     * Construtor padrão
     *
     * @param string $numeroErro Data de geração do erro
     * @param string $mensagemErro Mensagem do Erro
     * @param string $nomeArquivo Nome do arquivo onde ocorreu o erro
     * @param string $numeroLinha Número da linha do arquivo onde ocorreu o erro
     * @param string $variaveis Variáveis utilizadas durante o erro
     * @return bool
     */
    public function __construct($numeroErro, $mensagemErro, $nomeArquivo, $numeroLinha, $variaveis = null) {
    	
    	$this->numeroErro   = $numeroErro;
    	$this->mensagemErro = preg_replace('|\[.*\]|','',$mensagemErro);
    	$this->nomeArquivo  = $nomeArquivo;
    	$this->numeroLinha  = $numeroLinha;
    	$this->variaveis    = $variaveis;
    	
    	$this->data = date('d/m/Y H:i:s');
		$this->tipo = array (E_ERROR             => 'Error',
	                  		 E_WARNING           => 'Warning',
	                  		 E_PARSE             => 'Parsing Error',
	                  		 E_NOTICE            => 'Notice',
	                  		 E_CORE_ERROR        => 'Core Error',
	                  		 E_CORE_WARNING      => 'Core Warning',
	                  		 E_COMPILE_ERROR     => 'Compile Error',
	                  		 E_COMPILE_WARNING   => 'Compile Warning',
	                  		 E_USER_ERROR        => 'User Error',
	                  		 E_USER_WARNING      => 'User Warning',
	                  		 E_USER_NOTICE       => 'User Notice',
	                  		 E_STRICT            => 'Runtime Notice',
	                  		 E_RECOVERABLE_ERROR => 'Catchable Fatal Error');
		
		$this->errosRastreaveis = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);	                  		 
    }
    
	/**
	 * Retorna trecho de código gerador da exceção.
	 * @return String
	 */
    public function _getLine(){
    	$lines = file($this->nomeArquivo);
    	return preg_replace('|^\s*|', '', $lines[$this->numeroLinha-1]);
    }
    
	/**
	 * Gera um log de erro.
	 * @return void
	 */
    public function log(){
    	
		$log =Util::utf8D('Nível:').$this->tipo[$this->numeroErro].PHP_EOL;
		$log.=Util::utf8D('Descrição:').$this->mensagemErro.PHP_EOL;
		$log.='Linha('.$this->numeroLinha.'):'.$this->_getLine();
		$log.='Arquivo:'.$this->nomeArquivo.PHP_EOL;
		$log.='Url:'.$_SERVER['REQUEST_URI'];
	    if (in_array($this->numeroErro, $this->errosRastreaveis)) {
	        if(!empty($this->variaveis)){
	           $log.= PHP_EOL.wddx_serialize_value($this->variaveis, "Variables");
	        }
	    }
	    Log::logErro($log);
    }
}