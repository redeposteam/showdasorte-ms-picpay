<?php

/**
 * Configuração de Constantes 
 */
date_default_timezone_set('America/Recife');

$version = explode('.', phpversion());

if(!defined('PHP_MAIJOR_VERSION')) {
    define('PHP_MAIJOR_VERSION', (int) $version[0]);
}
if(!defined('PHP_MINOR_VERSION')) {
    define('PHP_MINOR_VERSION',  (int) $version[1]);
}
if(!defined('PHP_RELEASE_VERSION')) {
    define('PHP_RELEASE_VERSION',(int) $version[2]);
}

define('DS',DIRECTORY_SEPARATOR);

$diretorio = dirname(__FILE__);
define('PATH',substr($diretorio, 0, strrpos($diretorio, DS)));

define('LIB_PATH',PATH.DS.'lib');
define('APP_PATH',PATH.DS.'app');
define('CONFIG_PATH',PATH.DS.'config');
define('LOG_PATH', realpath(PATH.DS.'log'));
define('CACHE_PATH', realpath(PATH.DS.'cache'));
define('LOG_APP', LOG_PATH.DS.date('Y-m-d').'_app.txt');
define('LOG_REC', LOG_PATH.DS.date('Y-m-d').'_rec.txt');
define('LOG_SDS', LOG_PATH.DS.date('Y-m-d').'_sds.txt');
define('LOG_FILE', LOG_PATH.DS.date('Y-m-d').'_log.txt');
define('LOG_EMAIL', LOG_PATH.DS.date('Y-m-d').'_email.txt');
define('LOG_FILE_SQL', LOG_PATH.DS.date('Y-m-d').'_sql.txt');
define('LOG_FILE_ERRO', LOG_PATH.DS.date('Y-m-d').'_erro.txt');
define('LOG_JUNO', LOG_PATH.DS.date('Y-m-d').'_juno.txt');
define('LOG_PICPAY', LOG_PATH.DS.date('Y-m-d').'_picpay.txt');
define('LOG_AIXMOBIL', LOG_PATH.DS.date('Y-m-d').'_aixmobil.txt');
define('LOG_PAGSEGURO', LOG_PATH.DS.date('Y-m-d').'_pagseguro.txt');
define('LOG_TRANSACAO', LOG_PATH.DS.date('Y-m-d').'_transacao.txt');
define('LOG_NOTIFICACAO', LOG_PATH.DS.date('Y-m-d').'_notificacao.txt');
define('LOG_RESGATE', LOG_PATH.DS.date('Y-m-d').'_resgate.txt');
define('LOG_FILE_EXCEPTION', LOG_PATH.DS.date('Y-m-d').'_exception.txt');
define('LOG_EMAIL_TRACKING', LOG_PATH.DS.date('Y-m-d').'_email_tracking.txt');
define('LOG_KABUETA', LOG_PATH.DS.date('Y-m-d').'_kabueta.txt');

set_include_path(implode(PATH_SEPARATOR, array(PATH.DS.'lib',get_include_path())));