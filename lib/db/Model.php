<?php

/**
 * ORM - Model.
 */

/**
 * Esta classe é um modelo representativo de atributos de uma tabela.
 *
 * @name		Model
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Model {

	/**
	 * Construtor padrão.
	 *
	 * @return void
	 */
	public function __construct() {
		
	}

	/**
	 * Extrai os atributos do objeto devolvendo em formato de array associativo.
	 *
	 * @return array
	 */
	public function toArray() {

		$properties = array();
		$reflection = new ReflectionObject($this);
		$propriedades = $reflection->getProperties();
		foreach ($propriedades as $propriedade) {
			if(!in_array($propriedade->getName(), array('_campos_'))) {
				$propriedade->setAccessible(true);
				if(is_object($propriedade->getValue($this)) && get_class($propriedade->getValue($this)) == 'OCI-Lob') {
					$properties[$propriedade->getName()] = $propriedade->getValue($this)->load();
				} else {
					$properties[$propriedade->getName()] = $propriedade->getValue($this);
				}
			}
		}
		return $properties;
	}
}