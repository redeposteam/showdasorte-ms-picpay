<?php

/**
 * Métodos reaproveitáveis.
 */

/**
 * Classe concentradora de métodos uteis.
 *
 * @name		Util
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Util {

	/**
	 * Retorna dado codificado em utf8.
	 * @param mixed
	 * @return mixed
	 */
	public static function utf8E($mixed) {
		
		if(is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = self::utf8E($value);
			}
			return $mixed;
		}else{
			$mixed = utf8_encode($mixed);
		}
		return $mixed;
	}
	
	/**
	 * Retorna dado decodificado em utf8.
	 * @param mixed
	 * @return mixed
	 */
	public static function utf8D($mixed) {
		
		if(is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = self::utf8D($value);
			}
			return $mixed;
		}else{
			$mixed = utf8_decode($mixed);
		}
		return $mixed;
	}

	public static function retirarAcentos($mixed) {
	    
	    if(is_array($mixed)) {
	        foreach ($mixed as $key => $value) {
	            $mixed[$key] = self::retirarAcentos($value);
	        }
	        return $mixed;
	    }else{
	        $mixed = strtr($mixed, utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
	    }
	    return $mixed;
	}
	
	public static $LOWER_CHARS = array(
	    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
	    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
	    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
	    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
	    "ь", "э", "ю", "я"
	);
	
	public static $UPPER_CHARS = array(
	    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
	    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
	    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
	    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
	    "Ь", "Э", "Ю", "Я"
	);
	
	public static function upperAcentos($string) {
	    
	    return str_replace(self::$LOWER_CHARS, self::$UPPER_CHARS, $string);
	}
	
	public static function lowerAcentos($string) {

	    return str_replace(self::$UPPER_CHARS, self::$LOWER_CHARS, $string);
	}
	
	/**
	 * Retorna data no formato dd/mm/yyyy.
	 * @param String
	 * @return String
	 */
	public static function formatDate($data, $time = false) {
	    
	    $timestamp = strtotime($data);
	    if($time) {
	        return date('d/m/Y H:i:s', $timestamp);
	    } else {
	        return date('d/m/Y', $timestamp);
	    }
	}

	/**
	 * Retorna data no formato yyyy-mm-dd.
	 * @param String
	 * @return String
	 */
	public static function formatDateDb($data, $time = false) {

		$tmp = explode(' ', $data);
		if(count($tmp) > 1) {
			$tmp[0] = implode('-', array_reverse(explode('/', $tmp[0])));
			$data   = implode(' ', $tmp);
		} else {
			$data = implode('-', array_reverse(explode('/', $data)));
		}
		
		$timestamp = strtotime($data);
		if($time) {
				return date('Y-m-d H:i:s', $timestamp);
		} else {
				return date('Y-m-d', $timestamp);
		}
	}	

	public static function guid() {
	    
	    $hyphen = chr(45);
	    $charid = md5(uniqid(rand(), true));
	    $uuid = substr($charid, 0, 8).$hyphen
	    .substr($charid, 8, 8).$hyphen
	    .substr($charid,12, 8).$hyphen
	    .substr($charid,16, 8);
	    return $uuid;
	}
	
	public static function executionTime($start = null) {
	    
	    $mtime = microtime();
	    $mtime = explode(' ',$mtime);
	    $mtime = $mtime[1] + $mtime[0];
	    if ($start == null) {
	        return $mtime;
	    } else {
	        return round($mtime - $start, 2);
	    }
	}
	
	public static function base64UrlEncode($input) {
	    
	    return strtr(base64_encode($input), '+/=', '-_,');
	}
	
	public static function base64UrlDecode($input) {
	    
	    return base64_decode(strtr($input, '-_,', '+/='));
	}
}