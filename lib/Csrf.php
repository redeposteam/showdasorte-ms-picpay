<?php

/**
 * Validação atraves de token de CSRF.
 */

/**
 * Classe responsável pela geração de tokens CSRF.
 * 
 * @name		Csrft
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Csrf {
    
    protected $csrf;
    protected $time;
    const CSRF_REQS = '__csrf__reqs__';
    const CSRF_TIME = '__csrf__time__';
    
    public function __construct($reqs, $time) {
        
        $this->reqs = $reqs;
        $this->time = $time;
    }

    public function gui() {

        return bin2hex(openssl_random_pseudo_bytes(16));
    }

    public function getId() {

        return key($this->csrf);
    }

    public function getToken() {

        return $this->csrf;
    }

    public function toJson() {

        return json_encode(Util::utf8E($this->csrf));
    }

    public function setToken($csrf) {

        if(!empty($csrf) && !is_array($csrf)) {
            $csrf = json_decode($csrf, true);
        }
        return $this->csrf = $csrf;
    }

    public function set($key, $val) {

        $this->csrf[$this->getId()][$key] = $val;
    }   

    public function get($key) {

        return $this->csrf[$this->getId()][$key];
    }

    public function setReqs($reqs) {

        $this->csrf[$this->getId()][self::CSRF_REQS] = $reqs;
    }

    public function getReqs() {

        return $this->csrf[$this->getId()][self::CSRF_REQS];
    }

    public function getTime() {

        return $this->csrf[$this->getId()][self::CSRF_TIME];
    }

    public function create($data = array()) {

        (!isset($data[self::CSRF_REQS]) ? $data[self::CSRF_REQS] = 0 : null);
        (!isset($data[self::CSRF_TIME]) ? $data[self::CSRF_TIME] = time() : null);
        $this->csrf = array($this->gui() => $data);
        return $this;
    }

    public function regenerate() {
        
        $csrf = current($this->csrf);
        $this->create();
        $this->csrf[$this->getId()] = $csrf;
        $this->csrf[$this->getId()][self::CSRF_REQS] = 0;
        $this->csrf[$this->getId()][self::CSRF_TIME] = time();
    }

    public function maxreqs() {

        $maxreqs = false;
        if(!empty($this->csrf)) {
            $reqs = (int) $this->getReqs();
            if($reqs > $this->reqs) {
                $maxreqs = true;
            } else {
                $reqs++;
                $this->setReqs($reqs);
            }
        }
        return $maxreqs;
    }    

    public function expired() {

        $expired = false;
        if(!empty($this->csrf) && $this->time > 0) {
            if(((int) $this->getTime() + $this->time) < time()) {
                $expired = true;
            }
        }
        return $expired;
    }    

    public function verify() {

        $ret = true;
        if(!empty($this->csrf)) {
            if($this->maxreqs() || $this->expired()) {
                $ret = false;
            }
        } else {
            $ret = false;
        }
        return $ret;
    }
}