<?php

/**
 * Encapsula expressões SQL complexas.
 */

/**
 * Classe responsável por guardar uma expressão SQL.
 *
 * @name		Expression
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Expression {
	
	/**
	 * Guarda a expressão SQL.
	 *
	 * @var string
	 */
	protected $_expression;

	/**
	 * Instancia uma expressão, que é apenas uma seqüência armazenada como
	 * uma variável de membro de instância.
	 *
	 * @param string $expression A string contendo a expressão SQL.
	 */
	public function __construct($expression) {
		$this->_expression = (string) $expression;
	}

	/**
	 * Retorna uma expressão processada.
	 * 
	 * @return string A string contendo a expressão SQL guardada neste objeto.
	 */
	public function __toString() {
		
		return $this->_expression;
	}
}