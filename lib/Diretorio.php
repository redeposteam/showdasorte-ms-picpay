<?php

/**
 * Utilizada para operações com diretórios.
 */

/**
 * Classe responsável por guardar o diretório atual de forma estática.
 *
 * @name		Diretorio
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Diretorio {
	
	/**
	 * Guarda o diretório atual de alguma operação de leitura.
	 *
	 * @static
	 * @access public
	 * @name string $currentDirectory
	 */
	public static $currentDirectory;
	
}