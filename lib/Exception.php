<?php

/**
 * Tratamento de Exceção.
 */

/**
 * Responsável por manipular exceções no sistema.
 * 
 * @name		Exception
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
*/
class Exceptionn extends Exception {

	/**
	 * @var string Arquivo onde ocorreu a exceção
	 */
	private $mensagem;
	
	/**
	 * @var string Arquivo onde ocorreu a exceção
	 */
	private $arquivo;
	
	/**
	 * @var string Linha do arquivo onde ocorreu a exceção
	 */
	private $linha;
	
	/**
	 * @var string Código da exceção
	 */
	private $codigo;
	
	/**
	 * Construtor padrão
	 *
	 * @param string $mensagem Mensagem da exceção
	 * @param string $codigo Código da mensagem da exceção
	 * @param string $arquivo Nome do arquivo onde ocorreu o erro
	 * @param string $linha Número da linha do arquivo onde ocorreu o erro
	 * @return bool
	 */
	public function __construct($mensagem, $codigo, $arquivo, $linha) {
		
		if($codigo == 0){
			$codigo = 1;	
		}
		$this->codigo = $codigo;
		$this->linha   = $linha;
		$this->arquivo = $arquivo;
		$this->mensagem = $mensagem;
	}

	/**
	 * Retorna o código de erro da exceção.
	 * @return integer
	 */
	public function obterCodigo() {
		
		return $this->codigo;		
	}
	
	/**
	 * Retorna a mensagem de erro da exceção.
	 * @return string
	 */
	public function obterMensagem() {
		
		return $this->mensagem;
	}

	/**
	 * Retorna o nome do arquivo onde aconteceu a exceção.
	 * @return string
	 */
	public function obterArquivo() {
		
		return $this->arquivo;
	}

	/**
	 * Retorna o numero da linha onde aconteceu a exceção.
	 * @return int
	 */
	public function obterLinha() {
		
		return $this->linha;
	}

	/**
	 * Retorna trecho de código gerador da exceção.
	 * @return string
	 */
    public function _getLine() {
    	$lines = file($this->obterArquivo());
    	return preg_replace('|^\s*|', '', $lines[$this->obterLinha()-1]);
    }

	/**
	 * Gera um log de exceção.
	 * @return void
	 */
    public function log() {
    	
		$log =Util::utf8D('Código:').$this->obterCodigo().PHP_EOL;
		$log.=Util::utf8D('Descrição:').$this->obterMensagem().PHP_EOL;
		$log.='Linha('.$this->obterLinha().'):'.$this->_getLine();
		$log.='Arquivo:'.$this->obterArquivo().PHP_EOL;
		$log.='Url:'.$_SERVER['REQUEST_URI'];
	    Log::logExcecao($log);
    }
}