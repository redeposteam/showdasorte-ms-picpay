<?php

/**
 * Validação atraves de token de segurança.
 */

/**
 * Classe responsável pela geração de tokens Jwt.
 * 
 * @name		Jwt
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Jwt {

   /**
    * O emissor do JWT
	* @var string
	*/
	const ISS = 'iss';
	
	/**
	 * O assunto do JWT
	 * @var string
	 */
	const SUB = 'sub';
		
	/**
	 * A visibilidade do JWT
	 * @var string
	 */
	const AUD = 'aud';
		
	/**
	 * Tempo de expiração do JWT definido no tempo Unix.
	 * @var string
	 */
	const EXP = 'exp';
		
	/**
	 * "Não antes", identifica a partir de que data/hora
	 *  o JWT deve ser aceito para processamento.
	 * @var string
	 */
	const NBF = 'nbf';
		
	/**
	 * O tempo que o JWT foi emitido. Pode ser utilizado 
	 * para determinar a idade do JWT.
	 * @var string
	 */
	const IAT = 'iai';
		
	/**
	 * Identificador único para o JWT. Pode ser usado 
	 * para prevenir que o JWT seja repetido. Isto é 
	 * útil para garantir que o JWT será usado apenas uma vez.
	 * @var string
	 */
	const JTI = 'jti';
	
	/**
	 * Constante que representa token válido.
	 * @var int
	 */
	const TKVAL = 0;
	
	/**
	 * Constante que representa token expirado.
	 * @var int
	 */
	const TKEXP = 1;
	
	/**
	 * Constante que representa token fora do tempo inicio de validade.
	 * @var int
	 */
	const TKNBF = 2;
	
	/**
	 * Constante que representa token com assinatura inválida.
	 * @var int
	 */
	const TKSIG = 3;
		
	/**
	 * @var string Chave utilizada na criptografia
	 */
	private $key;
	/**
	 * @var string Cabecalho do token em formato base64
	 */
	private $headerBase64;
	/**
	 * @var string Informações do token em formato base64
	 */
	private $payloadBase64;
	/**
	 * @var string Assinatura do token em formato base64
	 */
	private $signatureBase64;
	
	/**
	 * @var array Informações do cabecalho do token
	 */
	private $header = array();
	/**
	 * @var array Informações do token
	 */
	private $payload = array();
	
	/**
	 * @var array Algorítimos possíveis de serem utilizados
	 */
	private $algorithms = array('HS256'=>'sha256');

	/**
	 * Construtor padrão
	 *
	 * @param string $typ Tipo da Hash
	 * @param string $alg Algoritimo de criptografia
	 * @param string $key Chave utilizada na criptografia
	 * @return bool
	 */
	public function __construct($typ, $alg, $key = null) {

		$this->key = $key;
		$this->header['typ'] = $typ;
		$this->header['alg'] = $alg;
	}
	
	/**
	 * Defini um atributo Payload
	 *
	 * @param string $key Chave do atributo Payload a ser adicionado.
	 * @param string $value Valor do atributo Payload a ser adicionado.
	 * @return bool
	 */
	public function setPayload($key, $value) {
		
		$this->payload[$key] = $value;
		return $this;
	}

	public function base64UrlEncode($input) {
			
		return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($input));
	}

	/**
	 * Cria a assinatura do token
	 *
	 * @param string $key Chave privada de assinatura do token.
	 * @return Jwt
	 */
	public function sign($key = null) {
		
		if(!empty($key)) {
			$this->key = $key;
		}
		$this->headerBase64    = $this->base64UrlEncode(json_encode($this->header));
		$this->payloadBase64   = $this->base64UrlEncode(json_encode($this->payload));
		$this->signatureBase64 = $this->base64UrlEncode(hash_hmac($this->algorithms[strtoupper($this->header['alg'])], $this->headerBase64.'.'.$this->payloadBase64, $this->key, true));
		return $this;
	}

	/**
	 * Retorna o token assinado
	 *
	 * @param string $key Chave privada de assinatura do Jwt.
	 * @return string
	 */
	public function token() {

		return $this->headerBase64.".".$this->payloadBase64.".".$this->signatureBase64;
	}
	
	/**
	 * Verifica se o token é válido observando não só a assinatura
	 * mas também, tempo de expiração e tempo de início de validade do token.
	 *
	 * @param string $token Token a ser validado.
	 * @param string $key Chave privada de assinatura.
	 * @return boolean
	 */
	public function verify($token, $key = null) {

		if(!empty($key)) {
			$this->key = $key;
		}
		$parts = explode('.', $token);
		$this->header = json_decode(base64_decode($parts[0]), true);
		$this->payload = json_decode(base64_decode($parts[1]), true);

		if(isset($this->payload[self::EXP])) {
			if((int) $this->payload[self::EXP] < time()) {
				return self::TKEXP;
			}
		}
		
		if(isset($this->payload[self::NBF])) {
			if((int) $this->payload[self::NBF] > time()) {
				return self::TKNBF;
			}
		}

		if($this->sign()->token() != $token) {
		    return self::TKSIG;
		}
		
		return 0;
	}
}