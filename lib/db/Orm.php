<?php

/**
 * Mapeamento ORM.
 */

/**
 * Classe responsável pela criação do arquivos de mapeamento ORM.
 *
 * @name		Orm
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Orm {

	/**
	 * Método para formatação de nome de classe
	 * obtido pelo nome da tabela
	 *
	 * @access public
	 * @param string $valor Nome da Tabela
	 * @return string
	 */
	public function objectName($valor) {
	
		$array = explode('_', strtolower($valor));
		$array = array_map('ucfirst',$array);
		$valor = implode('',$array);
	
		return $valor;
	}

	/**
	 * Método para formatação de nome de atributo
	 * obtido pelo nome da campo da tabela
	 *
	 * @access public
	 * @param string $valor Nome do Campo
	 * @return string
	 */
	public function propertieName($valor) {
	
		$array = explode('_',strtolower($valor));
		$array = array_map('ucfirst',$array);
		reset($array);
		$array[key($array)] = strtolower(current($array));
		$valor = implode('',$array);
	
		return $valor;
	}

	/**
	 * Método para criação de modelos abstratos
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela 
	 * @param string $metadataTable Metadados da Tabela
	 * @return string
	 */
	public function createAbstractModel($nameTable, $metadataTable) {
		
		$fks = $metadataTable['FKS'];
		$metadataTable = $metadataTable['FIELDS'];
		
		$tabela = $this->objectName($nameTable);
		$tabela2 = str_replace('abstract_', '', $nameTable);
		 
		$classe = "<?php\n\nabstract class ".$tabela." extends Model {\n\n{propriedades}";
		$classe.= "{construtor}\n";

		$argumentos = '';
		$propriedades = '';
		$propriedades_classe = '';
		$propriedades_padrao = '';
		
		foreach ($metadataTable as $field) {

			$campo_tipo = $field['DATA_TYPE'];
			$campo_nome = $field['COLUMN_NAME'];
			$campo_nome_metodo = ucfirst($this->propertieName($field['COLUMN_NAME']));
				
			$metodoSet = "set".$campo_nome_metodo;
			$metodoGet = "get".$campo_nome_metodo;
			$metodoExtra1 = "addCol".$campo_nome_metodo;
			$metodoExtra2 = "delCol".$campo_nome_metodo;
			
			############
			# METODO SET
			############
			
			$classe.="  /**\n";
			$classe.="   * Seta a propriedade $".$campo_nome."\n";
			$classe.="   *\n";
			$classe.="   * @param ".$campo_tipo." \$".$campo_nome."\n";
			$classe.="   * @return ".$this->objectName($tabela2)."\n";
			$classe.="   */\n";
			$classe.= "   public function ".$metodoSet."($".$campo_nome.") {\n";
			$classe.= "       $"."this->".$campo_nome." = "."$".$campo_nome.";\n";
			$classe.= "       return $"."this;\n   }\n\n";
			
			############
			# METODO GET
			############
			
			$classe.="  /**\n";
			$classe.="   * Retorna a propriedade $".$campo_nome."\n";
			$classe.="   *\n";
			$classe.="   * @return ".$campo_tipo."\n";
			$classe.="   */\n";
			$classe.= "   public function ".$metodoGet."() {\n";
			$classe.= "       return $"."this->".$campo_nome.";\n   }\n\n";
			
			########################
			# PROPRIEDADES DA CLASSE
			########################
			
			$propriedades.="  /**\n";
			$propriedades.="   * Propriedade $".$campo_nome."\n";
			$propriedades.="   *\n";
			$propriedades.="   * @protected ".$campo_tipo."\n";
			$propriedades.="   */\n";
			$propriedades.="   protected $".$campo_nome.";\n\n";
			
			#####################################################
			# SETANDO VALORES DEFAULT PARA OS ATRIBUTOS DA CLASSE
			#####################################################
			
			$propriedades_padrao.="      $"."this->".$campo_nome." = null;\n";
			
			################################
			# CRIANDO O CONSTRUTOR DA CLASSE
			################################
	
			$argumentos.="$".$campo_nome.",";
			$propriedades_classe.="              $"."this->".$campo_nome." = $".$campo_nome.";\n";
		}
		
		#################################################################
		# SUBSTITUINDO A STRING {properties} PELAS PROPRIEDADES DA CLASSE
		#################################################################
		
		$classe = str_replace("{propriedades}",$propriedades,$classe);
		
		########################
		# MONTAGEM DO CONSTRUTOR
		########################
		
		$construtor = "   public function __construct() {\n\n".$propriedades_padrao."   }\n";
		
		#####################################################
		# SUBSTITUINDO A STRING {construtor} PELOS ARGUMENTOS
		#####################################################
							
		$classe = str_replace("{construtor}", $construtor, $classe);
		
		###################
		# FECHANDO A CLASSE
		###################
		
		$classe.="}";
		
		return $classe;
	}
	
	/**
	 * Método para criação de repositórios abstratos
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela
	 * @param string $metadataTable Metadados da Tabela
	 * @return string
	 */
	public function createAbstractRepository($nameTable, $metadataTable) {

		$foreignKeys = $metadataTable['FKS'];
		$metadataTable = $metadataTable['FIELDS'];
		
		$tabela = $this->objectName($nameTable);
		
		$tabela2 = str_replace('abstract_', '', $nameTable);
		$tabela2 = $this->objectName(str_replace('_repositorio', '', $tabela2));
		
		$tabela3 = str_replace('abstract_', '', $nameTable);
		$tabela3 = strtolower(str_replace('_repositorio', '', $tabela3));
			
		$classe = "<?php\n\n";
		
		$fksChecked = array();
		foreach ($foreignKeys as $fk) {
			if(!in_array($fk['tabela_referencia'], $fksChecked)) {
				$classe.= "require_once 'app/abstract/repository/Abstract".$this->objectName($fk['tabela_referencia'])."Repositorio.php';\n";
				$fksChecked[] = $fk['tabela_referencia'];
			}
		}
		
		$classe.= "\nabstract class ".$tabela." extends Repository {\n\n{propriedades}";
		$classe.= "{construtor}\n";

		$classe.="  /**\n";
		$classe.="   * @return ".$this->objectName($tabela2)."\n";
		$classe.="   */\n";
		$classe.= "\tpublic function createModel() {\n\n";
		$classe.= "\t\treturn new ".$this->objectName($tabela2)."();\n";
		$classe.= "\t}\n\n";
		
		$first = current($metadataTable);
		
		$pk = '';
		$fk = '';
		$campos = 'protected $campos = array(\''.implode('\',\'', array_keys($metadataTable)).'\');';
		$nomeTabela = 'protected $tabela = \''.$tabela3.'\';';
		$esquema = "protected \$esquema = '".strtolower($first['SCHEMA_NAME'])."';";
		$sequence = '';
		
		$pks = array();
		foreach ($metadataTable as $field) {
			
			if(!empty($field['PRIMARY'])) {
				$pks[] = $field['COLUMN_NAME'];
			}
			
			if(strpos($field['DEFAULT'], 'next') !== false) {
				preg_match_all('|\'[a-zA-Z\_]+\'|', $field['DEFAULT'], $matches);
				$sequence = 'protected $sequence = '.current($matches[0]).';';
			}
		}
		$pk = 'protected $pk = array(\''.implode('\',\'',$pks).'\');';
		
		$fks = array();
		$repositorios = '';
		$propriedades_padrao ='';
		
		$fksChecked = array();
		foreach ($foreignKeys as $fk) {
			$fks[] =  "'".strtolower($fk['campo_origem'])."'=>array('".strtolower($fk['tabela_referencia'])."'=>'".strtolower($fk['campo_referencia'])."')";
			if(!in_array($fk['tabela_referencia'], $fksChecked)) {
				
				$repositorios.="\tprotected $".$this->objectName($fk['tabela_referencia'])."Repositorio;\n";
				$propriedades_padrao.="\t\t\$this->".$this->objectName($fk['tabela_referencia'])."Repositorio = new ".$this->objectName($fk['tabela_referencia'])."Repositorio();\n";
				
				$classe.="  /**\n";
				$classe.="   * Retorna o repositorio $".$this->objectName($fk['tabela_referencia'])."Repositorio\n";
				$classe.="   *\n";
				$classe.="   * @return ".$this->objectName($fk['tabela_referencia'])."Repositorio\n";
				$classe.="   */\n";
				$classe.= "   public function get".$this->objectName($fk['tabela_referencia'])."Repositorio() {\n";
				$classe.= "       return $"."this->".$this->objectName($fk['tabela_referencia'])."Repositorio;\n   }\n\n";
				
				$fksChecked[] = $fk['tabela_referencia'];
			}
		}
		$fk = 'protected $fk = array('.implode(',',$fks).');';

		$propriedades = '';
		$propriedades.= "\t".$pk."\n";
		$propriedades.= "\t".$fk."\n";
		$propriedades.= "\t".$campos."\n";
		$propriedades.= "\t".$nomeTabela."\n";
		$propriedades.= "\t".$esquema."\n";
		$propriedades.= "\t".$sequence."\n";
		$propriedades.= $repositorios."\n";

		foreach ($metadataTable as $field) {
		
			$campo_nome = $field['COLUMN_NAME'];
			$campo_nome_metodo = ucfirst($this->propertieName($field['COLUMN_NAME']));
			$metodoExtra1 = "addCol".$campo_nome_metodo;
			$metodoExtra2 = "delCol".$campo_nome_metodo;
			$metodoExtra3 = "keyJoin".$campo_nome_metodo;

			#################
			# METODO KEY JOIN
			#################
			
			$classe.="\t/**\n";
			$classe.="\t* Retorna a propriedade $".$campo_nome."\n";
			$classe.="\t*\n";
			$classe.="\t* @return void\n";
			$classe.="\t*/\n";
			$classe.= "\tpublic function ".$metodoExtra3."() {\n";
			$classe.= "\t\t$"."this->keyJoin('".$campo_nome."');\n";
			$classe.= "\t\t"."return \$this;\n   }\n\n";
				
			###################
			# METODO ADD COLUMM
			###################
				
			$classe.="\t/**\n";
			$classe.="\t* Retorna a propriedade $".$campo_nome."\n";
			$classe.="\t*\n";
			$classe.="\t* @return void\n";
			$classe.="\t*/\n";
			$classe.= "\tpublic function ".$metodoExtra1."(\$column = null, \$alias = null) {\n\n";
			$classe.= "\t\tif(is_null(\$column)) {\n";
			$classe.= "\t\t\t\$column = '".$campo_nome."';\n";
			$classe.= "\t\t}\n";
			$classe.= "\t\t$"."this->addColumm(\$column, \$alias);\n   }\n\n";
				
			###################
			# METODO DEL COLUMM
			###################
				
			$classe.="\t/**\n";
			$classe.="\t* Retorna a propriedade $".$campo_nome."\n";
			$classe.="\t*\n";
			$classe.="\t* @return void\n";
			$classe.="\t*/\n";
			$classe.= "\tpublic function ".$metodoExtra2."(\$column = '".$campo_nome."') {\n";
			$classe.= "\t\t$"."this->delColumm(\$column);\n   }\n\n";
		}
		
		$classe.="  /**\n";
		$classe.="   * Retorna um objeto $".ucfirst($tabela2)."\n";
		$classe.="   *\n";
		$classe.="   * @param Model $".ucfirst($tabela2)."\n";
		$classe.="   * @return ".ucfirst($tabela2)."\n";
		$classe.="   */\n";
		$classe.= "   public function selecionar($".ucfirst($tabela2).", \$orWhere = false) {\n";
		$classe.= "       return parent::selecionar($".ucfirst($tabela2).");\n   }\n\n";
		
		$classe.="  /**\n";
		$classe.="   * Inseri um objeto $".ucfirst($tabela2)."\n";
		$classe.="   *\n";
		$classe.="   * @param Model $".ucfirst($tabela2)."\n";
		$classe.="   * @return int Id gerado para o objeto na base de dados.\n";
		$classe.="   */\n";
		$classe.= "   public function inserir($".ucfirst($tabela2).") {\n";
		$classe.= "       return parent::inserir($".ucfirst($tabela2).");\n   }\n\n";
		
		$classe.="  /**\n";
		$classe.="   * Atualiza um objeto $".ucfirst($tabela2)."\n";
		$classe.="   *\n";
		$classe.="   * @param Model $".ucfirst($tabela2)."\n";
		$classe.="   * @return int Número de linhas afetadas\n";
		$classe.="   */\n";
		$classe.= "   public function atualizar($".ucfirst($tabela2).") {\n";
		$classe.= "       return parent::atualizar($".ucfirst($tabela2).");\n   }\n\n";
		
		$classe.="  /**\n";
		$classe.="   * Exclui um objeto $".ucfirst($tabela2)."\n";
		$classe.="   *\n";
		$classe.="   * @param Model $".ucfirst($tabela2)."\n";
		$classe.="   * @return int Número de linhas afetadas\n";
		$classe.="   */\n";
		$classe.= "   public function excluir($".ucfirst($tabela2).") {\n";
		$classe.= "       return parent::excluir($".ucfirst($tabela2).");\n   }\n\n";
		
		$classe.="  /**\n";
		$classe.="   * Retorna uma coleção de objetos $".ucfirst($tabela2)."\n";
		$classe.="   *\n";
		$classe.="   * @param Model $".ucfirst($tabela2)."\n";
		$classe.="   * @param int \$count\n";
		$classe.="   * @param int \$offset\n";
		$classe.="   * @return Model[]\n";
		$classe.="   */\n";
		$classe.= "   public function todos($".ucfirst($tabela2)." = null, \$orWhere = true, \$count = null, \$offset = 0, \$objectFormat = false) {\n";
		$classe.= "       return parent::todos($".ucfirst($tabela2).", \$orWhere, \$count, \$offset, \$objectFormat);\n   }\n\n";
		
		########################
		# MONTAGEM DO CONSTRUTOR
		########################
		
		$construtor = "   public function __construct() {\n\n\t\tparent::__construct();\n".$propriedades_padrao."   }\n";
		
		#################################################################
		# SUBSTITUINDO A STRING {properties} PELAS PROPRIEDADES DA CLASSE
		#################################################################
		
		$classe = str_replace("{propriedades}", $propriedades, $classe);
		
		#####################################################
		# SUBSTITUINDO A STRING {construtor} PELOS ARGUMENTOS
		#####################################################
			
		$classe = str_replace("{construtor}", $construtor, $classe);
		
		###################
		# FECHANDO A CLASSE
		###################
		
		$classe.="}";
		
		return $classe;
	}
	
	/**
	 * Método para criação de controladores abstratos
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela
	 * @param string $appcontroller Nome do Controlador
	 * @return string
	 */
	public function createAbstractController($nameTable, $appcontroller) {

		$tabela  = $this->objectName($nameTable);
		$tabela2 = str_replace('Abstract', '', $tabela);
		$tabela2 = str_replace('Controller', '', $tabela2);
			
		$classe = "<?php\n\n";
		$classe.= "require_once 'app/controller/".$appcontroller."Controller.php';\n\n";
		$classe.= "abstract class ".$tabela." extends ".$appcontroller."Controller {\n\n";
		
		$classe.="\t/**\n";
		$classe.="\t* @var ".$tabela2."\n";
		$classe.="\t*/\n";
		$classe.= "\tprotected \$model;\n\n";
		
		$classe.="\t/**\n";
		$classe.="\t* @var ".$tabela2."Repositorio\n";
		$classe.="\t*/\n";
		$classe.="\tprotected \$repository;\n\n";
		
		$classe.= "   public function __construct() {\n\n";
		$classe.= "      parent::__construct();\n";
		$classe.= "      \$this->model = new $tabela2();\n";
		$classe.= "      \$this->repository = new $tabela2"."Repositorio();\n";
		$classe.= "   }\n";
		$classe.= "}";

		return $classe;
	}
	
	/**
	 * Método para criação de modelos
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela
	 * @return string
	 */
	public function createModel($nameTable) {
	
		$tabela = $this->objectName($nameTable);
			
		$classe = "<?php\n\n";
		$classe.= "require_once 'app/abstract/model/Abstract".$tabela.".php';\n\n";
		$classe.= "class ".$tabela." extends Abstract".$tabela." {\n\n";
		$classe.= "}";
	
		return $classe;
	}

	/**
	 * Método para criação de repositórios
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela
	 * @param string $metadataTable Metadados da Tabela
	 * @return string
	 */
	public function createRepository($nameTable, $metadataTable) {

		$foreignKeys = $metadataTable['FKS'];
		$metadataTable = $metadataTable['FIELDS'];
		
		$tabela = $this->objectName($nameTable);
			
		$classe = "<?php\n\n";
		
		foreach ($foreignKeys as $fk) {
			$classe.= "require_once 'app/model/".$this->objectName($fk['tabela_referencia']).".php';\n";
			$classe.= "require_once 'app/repository/".$this->objectName($fk['tabela_referencia'])."Repositorio.php';\n";
		}
		
		$classe.= "require_once 'app/model/".$tabela.".php';\n";
		$classe.= "require_once 'app/abstract/repository/Abstract".$tabela."Repositorio.php';\n\n";
		$classe.= "class ".$tabela."Repositorio extends Abstract".$tabela."Repositorio {\n\n";
		$classe.= "}";
	
		return $classe;
	}
	
	/**
	 * Método para criação de controladores
	 *
	 * @access public
	 * @param string $nameTable Nome da Tabela
	 * @return string
	 */
	public function createController($nameTable) {
	
		$tabela = $this->objectName($nameTable);
	
		$classe = "<?php\n\n";
		$classe.= "require_once 'app/repository/".$tabela."Repositorio.php';\n";
		$classe.= "require_once 'app/abstract/controller/Abstract".$tabela."Controller.php';\n\n";
		
		$classe.= "class ".$tabela."Controller extends Abstract".$tabela."Controller {\n\n";
		$classe.= "}";
	
		return $classe;
	}
	
	/**
	 * Método para criação de controladores
	 *
	 * @access public
	 * @param string $appcontroller Nome do Controlador
	 * @return string
	 */
	public function createAppController($appcontroller) {
	
		$classe = "<?php\n\n";
		$classe.= "require_once 'lib/ControllerRestful.php';\n\n";
		$classe.= "class ".$appcontroller."Controller extends ControllerRestful {\n\n";
		$classe.= "}";
	
		return $classe;
	}
}