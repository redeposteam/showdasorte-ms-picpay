<?php

require_once 'app/controller/ComumController.php';

class SdsController extends ComumController
{

    public function getServerProtocol()
    {
//        return "https";
        return (isset($_SERVER['HTTPS']) ? 'https' : 'http');
    }

    public function getSds($conf = 'sds')
    {

        $config = App::getConfig();
        return $config[$conf]['ip'] . '/' . $config[$conf]['servico'] . '?login=' . $config[$conf]['login'] . '&senha=' . $config[$conf]['senha'];
    }

    private function getCachePath($dir)
    {

        return CACHE_PATH . (!empty($dir) ? DS . $dir : '');
    }

    private function timeCacheSds($dir, $file)
    {

        $time = null;
        $dir = $this->getCachePath($dir);
        $file = $dir . DS . $file;
        if (file_exists($file)) {
            $time = filectime($file);
        }
        return $time;
    }

    private function removeCacheSds($dir, $file)
    {

        $dir = $this->getCachePath($dir);
        $file = $dir . DS . $file;
        if (file_exists($file)) {
            unlink($file);
        }
    }

    private function setCacheSds($dir, $file, $dados)
    {

        $dir = $this->getCachePath($dir);
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $path = $dir . DS . $file;
        if (file_exists($path)) {
            unlink($path);
        }
        file_put_contents($path, json_encode(Util::utf8E($dados)));
    }

    private function getCacheSds($dir, $file, $cacheconf)
    {

        $dados = null;
        $time = time();
        $path = $this->getCachePath($dir) . DS . $file;

        if (isset($cacheconf['dayweek']) && $cacheconf['dayweek'] != date('w')) {
            return $dados;
        }

        if (isset($cacheconf['initime']) && isset($cacheconf['endtime'])) {
            if (date('d') > date('d', $this->timeCacheSds($dir, $file)) || $time > $cacheconf['endtime']) {
                $this->removeCacheSds($dir, $file);
            }
        }

        if (file_exists($path) && filesize($path) > 0) {

            $tim = true;
            $init = true;
            $endt = true;
            $tempo = $time - filectime($path);

            if (isset($cacheconf['time']) && $cacheconf['time'] <= $tempo) {
                $tim = false;
            }

            if (isset($cacheconf['initime']) && $time < $cacheconf['initime']) {
                $init = false;
            }

            if (isset($cacheconf['endtime']) && $time > $cacheconf['endtime']) {
                $endt = false;
            }

            if ($tim && $init && $endt) {
                $dados = json_decode(file_get_contents($path), true);
            }
        } else if (file_exists($path)) {
            $dados = array('codigo_execucao' => 0, 'dados' => array());
        }

        return $dados;
    }

    public function getSdsServico($params, $conf = 'sds', $type = null, $cacheconf = null)
    {

        $precision = 1000;
        $microtime1 = microtime(true) * $precision;
        $protocol = $this->getServerProtocol();
        if ($conf === "sdscep") {
            $protocol = "http";
        }
        $ret = null;

        if (!empty($cacheconf)) {
            $dir = $cacheconf['dir'];
            $file = $cacheconf['key'] . '.json';
            $url = 'cache://' . (!empty($dir) ? $dir . '/' : '') . $file;
            $ret = $this->getCacheSds($dir, $file, $cacheconf);
        }

        if (empty($ret)) {
            if (empty($type)) {
                $parametros = array();
                foreach ($params as $key => $val) {
                    if (is_array($val)) {
                        $parametros[] = $key . '=' . rawurlencode(json_encode($val));
                    } else {
                        $parametros[] = $key . '=' . rawurlencode($val);
                    }
                }
                $url = $protocol . '://' . $this->getSds($conf) . '&' . implode('&', $parametros);
                $ret = $this->cURL($url);
            } else {
                $sdsUrl = explode('?', $this->getSds($conf));
                $url = $protocol . '://' . $sdsUrl[0];
                $auth = explode('&', $sdsUrl[1]);
                foreach ($auth as $value) {
                    $tmp = explode('=', $value);
                    $params[$tmp[0]] = $tmp[1];
                }
                $ret = $this->cURL($url, $params, null, $type);
            }
            if ($conf === "sdscep") {
                $ret = strtr($ret, array("\n" => ''));
            }
            if (!empty($cacheconf)) {
                $ret2 = json_decode($ret, true);
                if (!empty($cacheconf['initime']) && !empty($cacheconf['endtime'])) {
                    $now = time();
                    if ($cacheconf['initime'] < $now && $now < $cacheconf['endtime']) {
                        if ($ret2['codigo_execucao'] == 0) {
                            $this->setCacheSds($dir, $file, $ret);
                        }
                    }
                } else {
                    if ($ret2['codigo_execucao'] == 0) {
                        $this->setCacheSds($dir, $file, $ret);
                    }
                }
            }
        }
        $microtime2 = microtime(true) * $precision;
        $executiontime = '[' . number_format((($microtime2 - $microtime1) / $precision), 5, '.', '') . ']';
        if (App::getConfig('logs', 'sds')) {
            Log::logArquivo($executiontime . ' ' . $this->logTags() . ' RES:[' . $url . '] ' . ($type == 'POST' ? 'POST:[' . json_encode($params) . ']' : '') . ' DEB:[' . str_replace('\/', '/', str_replace('\"', '"', $ret)) . "]\n", LOG_SDS);
        }
        return $ret;
    }

    //TRANSACOES DO SDS

    const SDS_TRANSACAO_OBTER_SEQUENCIAL = 1;
    const SDS_TRANSACAO_CONSULTAR_RESULTADO = 3;
    const SDS_TRANSACAO_LIBERAR_SEQUENCIAL = 4;
    const SDS_TRANSACAO_OBTER_EDICAO_ATUAL = 5;
    const SDS_TRANSACAO_OBTER_PROXIMA_EDICAO_NAO_INICIADA = 6;
    const SDS_TRANSACAO_OBTER_EDICAO = 7;
    const SDS_TRANSACAO_REGISTRAR_VENDA = 8;
    const SDS_TRANSACAO_CONSULTAR_VENDA = 9;
    const SDS_TRANSACAO_TRANSACOES_CLIENTE = 11;
    const SDS_TRANSACAO_TRANSACOES_PENDENTE_CLIENTE = 12;
    const SDS_TRANSACAO_BLOQUEAR_CERTIFICADOS = 13;
    const SDS_TRANSACAO_ATUALIZAR_TRANSACAO_PAGAMENTO = 14;
    const SDS_TRANSACAO_TRANSACOES_AGUARDANDO_PAGAMENTO_CLIENTE = 15;
    const SDS_TRANSACAO_ANULAR_CERTIFICADO = 16;
    const SDS_TRANSACAO_CONFIRMAR_CERTIFICADO = 17;
    const SDS_TRANSACAO_TRANSACOES_CANCELADO_CLIENTE = 18;
    const SDS_TRANSACAO_SESSAO_CRIAR = 19;
    const SDS_TRANSACAO_SESSAO_ADICIONAR = 20;
    const SDS_TRANSACAO_SESSAO_REMOVER = 21;
    const SDS_TRANSACAO_CONSULTAR_DADOS_VENDA = 22;
    const SDS_TRANSACAO_CONSULTAR_CEP = 1;
    const SDS_TRANSACAO_SALVAR_DADOS_CARTAO = 23;
    const SDS_TRANSACAO_CONSULTAR_DADOS_CARTAO = 24;
    const SDS_TRANSACAO_CRIAR_DOCUMENTO_TRANSACAO = 29;
    const SDS_TRANSACAO_LER_DOCUMENTO_TRANSACAO = 30;
    const SDS_TRANSACAO_MOVER_DOCUMENTO_TRANSACAO = 31;
    const SDS_TRANSACAO_LOGIN_USUARIO = 25;
    const SDS_TRANSACAO_OBTER_USUARIO = 26;
    const SDS_TRANSACAO_INSERIR_USUARIO = 1;
    const SDS_TRANSACAO_ATUALIZAR_USUARIO = 2;
    const SDS_TRANSACAO_OBTER_USUARIO_EMAIL = 4;
    const SDS_TRANSACAO_ATUALIZAR_SENHA_USUARIO = 3;
    const SDS_TRANSACAO_TRANSACOES_USUARIO = 27;
    const SDS_TRANSACAO_OBTER_BANNER_EDICAO = 32;
    const SDS_TRANSACAO_OBTER_QUANTIDADE_COMPRAS_EDICAO = 33;
    const SDS_TRANSACAO_OBTER_DATAS_EXTRACOES = 34;
    const SDS_TRANSACAO_OBTER_HTML_CERTIFICADO = 35;
    const SDS_TRANSACAO_PAGSEGURO_TRANSACAO_REGISTRAR = 36;
    const SDS_TRANSACAO_PAGSEGURO_TRANSACAO_CANCELAR = 37;
    const SDS_TRANSACAO_PAGSEGURO_TRANSACAO_CONSULTAR = 38;
    const SDS_TRANSACAO_PAGSEGURO_TRANSACAO_ATUALIZAR = 39;
    const SDS_TRANSACAO_OBTER_SEQUENCIAL_QTD = 40;
    const SDS_TRANSACAO_ATUALIZAR_PLAYER_ID = 41;
    const SDS_TRANSACAO_VENDA_RANDOMICA = 42;
    const SDS_TRANSACAO_GERAR_ID_VENDA = 43;
    const SDS_TRANSACAO_CADASTRA_RESGATE_DOACAO = 44;
    const SDS_TRANSACAO_CADASTRA_RESGATE_DOACAO_V2 = 47;

    //=================================
    //  CERTIFICADOS
    //=================================

    public function SdsObterSequencial($idEdicao, $dezenas = array())
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_SEQUENCIAL, 'idEdicao' => $idEdicao, 'dezenas' => $dezenas);
        return json_decode($this->getSdsServico($params), true);
    }

    public function SdsObterSequencialQtd($idEdicao, $qtd = 2, $dezenas = '')
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_SEQUENCIAL_QTD, 'idEdicao' => $idEdicao, 'dezenas' => $dezenas, 'qtd' => $qtd);
        return json_decode($this->getSdsServico($params), true);
    }

    public function SdsLiberarSequencial($idEdicao, $sequencial)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_LIBERAR_SEQUENCIAL, 'idEdicao' => $idEdicao, 'sequencial' => $sequencial);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsBloquearSequencial($idEdicao, $bilhetes)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_BLOQUEAR_CERTIFICADOS, 'idEdicao' => $idEdicao, 'bilhetes' => $bilhetes);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    //=================================
    //  EDICAO
    //=================================

    public function SdsObterEdicaoAtual($idTipoLoteria)
    {

        $conf = 'sds';
        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_EDICAO_ATUAL, 'idTipoLoteria' => $idTipoLoteria);
        //REGRA DE CACHE EDICAO ATUAL
        //===============================================================================
        $cacheconf = null;
        if (App::getConfig('cachesds', 'edicao_atual') == true) {
            $dir = 'edicao_atual';
            $key = $conf . '_transacao' . $params['tipoTransacao'] . '_' . $params['idTipoLoteria'];
            $file = $key . '.json';
            $cache = $this->getCacheSds($dir, $file, array());
            if (!empty($cache)) {
                $dados = json_decode($cache, true);
                if (isset($dados['dados']['DATA_HORA_FIM']) && time() > strtotime($dados['dados']['DATA_HORA_FIM'])) {
                    $this->removeCacheSds($dir, $file);
                }
            }
            $cacheconf = array('dir' => $dir, 'key' => $key);
        }
        //===============================================================================
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, $conf, null, $cacheconf), true));
    }

    public function SdsObterEdicaoProxima($idTipoLoteria)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_PROXIMA_EDICAO_NAO_INICIADA, 'idTipoLoteria' => $idTipoLoteria);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsObterEdicao($idTipoLoteria, $idEdicao)
    {

        $conf = 'sds';
        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_EDICAO, 'idTipoLoteria' => $idTipoLoteria, 'idEdicao' => $idEdicao);
        $cacheconf = null;
        if (App::getConfig('cachesds', 'edicao') == true) {
            $cacheconf = array('dir' => 'edicao', 'key' => $conf . '_transacao' . $params['tipoTransacao'] . '_' . $params['idTipoLoteria'] . '_' . $params['idEdicao']);
        }
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, $conf, null, $cacheconf), true));
    }

    //=================================
    //  VENDA
    //=================================

    public function SdsConsultarVenda($params)
    {

        $params['tipoTransacao'] = self::SDS_TRANSACAO_CONSULTAR_VENDA;
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsRegistrarVenda($params)
    {

        $params['tipoTransacao'] = self::SDS_TRANSACAO_REGISTRAR_VENDA;
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsCadastrarResgateDoacao($dados, $idTipoLoteria, $dataFormatada, $codigoVenda, $idEdicao) {

        $params['tipoTransacao'] = self::SDS_TRANSACAO_CADASTRA_RESGATE_DOACAO_V2;
        $params['idTipoLoteria'] = $idTipoLoteria;
        $params['dataSorteio']   = $dataFormatada;
        $params['dados']         = $dados;
        $params['codigoVenda']   = $codigoVenda;
        $params['tipoVenda']     = 1;
        $params['idEdicao']      = $idEdicao;
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsObterTransacoesCliente($idCliente)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_TRANSACOES_CLIENTE, 'idCliente' => $idCliente);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsObterTransacoesPendenteCliente($idCliente, $idEdicao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_TRANSACOES_PENDENTE_CLIENTE, 'idCliente' => $idCliente, 'idEdicao' => $idEdicao);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsObterTransacoesCanceladoCliente($idCliente, $idEdicao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_TRANSACOES_CANCELADO_CLIENTE, 'idCliente' => $idCliente, 'idEdicao' => $idEdicao);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsObterTransacoesClienteTipo($idCliente, $situacao, $idEdicao = null)
    {

        $conf = 'sds';
        $params = array('tipoTransacao' => self::SDS_TRANSACAO_TRANSACOES_USUARIO, 'idCliente' => $idCliente, 'situacao' => $situacao);
        (!empty($idEdicao) ? $params['idEdicao'] = $idEdicao : null);
        //REGRA DE CACHE EDICAO ATUAL
        //===============================================================================
        $cacheconf = null;
        $config = App::getConfig('cachesds');
        if ($config['cliente_cert'] == true && $config['cliente_cert_dayweek'] == date('w')) {
            $dir = 'cliente_cert';
            $key = $conf . '_transacao' . $params['tipoTransacao'] . '_' . $params['idCliente'] . '_' . $params['situacao'] . (!empty($params['idEdicao']) ? '_' . $params['idEdicao'] : '');
            $cacheconf = array();
            $cacheconf['dir'] = $dir;
            $cacheconf['key'] = $key;
            $cacheconf['dayweek'] = $config['cliente_cert_dayweek'];
            $dayweek = ($cacheconf['dayweek'] == 0 ? 6 : $cacheconf['dayweek'] - 1);
            $week = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
            $cacheconf['initime'] = strtotime('last ' . $week[$dayweek] . ' +24 hour ' . $config['cliente_cert_initime']);
            $cacheconf['endtime'] = strtotime('last ' . $week[$dayweek] . ' +24 hour ' . $config['cliente_cert_endtime']);
        }
        //===============================================================================
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, $conf, null, $cacheconf), true));
    }

    public function SdsObterTransacoesAguardandoPagamentoCliente($idCliente, $idEdicao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_TRANSACOES_AGUARDANDO_PAGAMENTO_CLIENTE, 'idCliente' => $idCliente, 'idEdicao' => $idEdicao);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsAtualizarTransacaoPagamento($idTransacao, $idVendaConsolidada)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_ATUALIZAR_TRANSACAO_PAGAMENTO, 'idTransacao' => $idTransacao, 'idVendaConsolidada' => $idVendaConsolidada);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsConfirmarCertificado($codigoVenda, $idTransacaoPagSeguro = null)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CONFIRMAR_CERTIFICADO, 'codigoVenda' => $codigoVenda);
        (!empty($idTransacaoPagSeguro) ? $params['idTransacaoPagSeguro'] = $idTransacaoPagSeguro : null);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsAnularCertificado($codigoVenda, $idTransacaoPagSeguro = null)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_ANULAR_CERTIFICADO, 'codigoVenda' => $codigoVenda);
        (!empty($idTransacaoPagSeguro) ? $params['idTransacaoPagSeguro'] = $idTransacaoPagSeguro : null);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsConsultarDadosVenda($codigoVenda, $idEdicao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CONSULTAR_DADOS_VENDA, 'codigoVenda' => $codigoVenda, 'idEdicao' => $idEdicao, 'idVenda' => $codigoVenda);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsConsultarResultado($idTipoLoteria, $dataSorteio)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CONSULTAR_RESULTADO, 'idTipoLoteria' => $idTipoLoteria, 'dataSorteio' => $dataSorteio);
        $dados = json_decode($this->getSdsServico($params), true);
        (count($dados['dados']) > 0 ? $dados['dados'] = json_decode(XmlToJson(str_replace("\n", '', $dados['dados'])), true) : null);
        return arrayChangeKeyCase($dados);
    }

    public function SdsCriarDocumentoTransacao($strSituacao, $strNomeArquivo, $strDados)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CRIAR_DOCUMENTO_TRANSACAO, 'strSituacao' => $strSituacao, 'strNomeArquivo' => $strNomeArquivo, 'strDados' => $strDados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds', 'POST'), true));
    }

    public function SdsLerDocumentoTransacao($strSituacao, $strNomeArquivo)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_LER_DOCUMENTO_TRANSACAO, 'strSituacao' => $strSituacao, 'strNomeArquivo' => $strNomeArquivo);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsMoverDocumentoTransacao($strSituacao, $strNomeArquivo, $strSituacaoNova)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_MOVER_DOCUMENTO_TRANSACAO, 'strSituacao' => $strSituacao, 'strNomeArquivo' => $strNomeArquivo, 'strSituacaoNova' => $strSituacaoNova);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsVendaRandomica($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_VENDA_RANDOMICA), $dados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsGerarIdVenda()
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_GERAR_ID_VENDA);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    //=================================
    //  CLIENTE
    //=================================

    protected $confSdsCliente = 'sds';

    public function formatarDadosCliente($dados)
    {

        unset($dados['_tipo_documento']);
        unset($dados['_data_hora_criacao_documento']);
        unset($dados['_data_hora_atualizacao_documento']);
        $dados['data_nascimento'] = Util::formatDate($dados['data_nascimento'], true);
        $dados['data_hora_criacao'] = Util::formatDate($dados['data_hora_criacao'], true);
        $dados['data_hora_atualizacao'] = Util::formatDate($dados['data_hora_atualizacao'], true);
        $dados['data_hora_inclusao_mailee'] = Util::formatDate($dados['data_hora_inclusao_mailee'], true);
        $dados['data_hora_ativacao'] = Util::formatDate($dados['data_hora_ativacao'], true);
        return $dados;
    }

    public function SdsObterUsuario($idCliente, $formatarDados = true)
    {

        $conf = $this->confSdsCliente;
        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_USUARIO, 'idCliente' => $idCliente);
        $cacheconf = null;
        if (App::getConfig('cachesds', 'cliente') == true) {
            $cacheconf = array('dir' => 'cliente', 'key' => $conf . '_transacao' . $params['tipoTransacao'] . '_' . $params['idCliente']);
        }
        $dados = arrayChangeKeyCase(json_decode($this->getSdsServico($params, $conf, null, $cacheconf), true));
        (isset($dados['dados']) && $formatarDados ? $dados['dados'] = $this->formatarDadosCliente($dados['dados']) : null);
        return $dados;
    }

    public function SdsObterUsuarioEmail($idEmpresa, $email)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_USUARIO_EMAIL, 'id_empresa' => $idEmpresa, 'email' => $email);
        $dados = arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sdscliente'), true));
        (isset($dados['dados']) ? $dados['dados'] = $this->formatarDadosCliente($dados['dados']) : null);
        return $dados;
    }

    public function SdsInserirUsuario($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_INSERIR_USUARIO), $dados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sdscliente', 'POST'), true));
    }

    public function SdsAtualizarUsuario($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_ATUALIZAR_USUARIO), $dados);
        if (App::getConfig('cachesds', 'cliente') == true) {
            $this->removeCacheSds('cliente', $this->confSdsCliente . '_transacao' . self::SDS_TRANSACAO_OBTER_USUARIO . '_' . $dados['id'] . '.json');
        }
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sdscliente', 'POST'), true));
    }

    public function SdsAtualizarSenhaUsuario($idCliente, $senhaAlfaNumerica)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_ATUALIZAR_SENHA_USUARIO, 'id' => $idCliente, 'senha_alfanumerica' => $senhaAlfaNumerica);
        if (App::getConfig('cachesds', 'cliente') == true) {
            $this->removeCacheSds('cliente', $this->confSdsCliente . '_transacao' . self::SDS_TRANSACAO_OBTER_USUARIO . '_' . $params['id'] . '.json');
        }
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sdscliente'), true));
    }

    public function SdsAtualizarPlayerId($idCliente, $playerId)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_ATUALIZAR_PLAYER_ID, 'idCliente' => $idCliente, 'chaveNotificacao' => $playerId);
        if (App::getConfig('cachesds', 'cliente') == true) {
            $this->removeCacheSds('cliente', $this->confSdsCliente . '_transacao' . self::SDS_TRANSACAO_OBTER_USUARIO . '_' . $params['idCliente'] . '.json');
        }
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsLoginUsuario($idEmpresa, $cpf, $senhaUsuario, $chaveNotificacao, $tipo)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_LOGIN_USUARIO, 'idEmpresa' => $idEmpresa, 'cpf' => $cpf, 'senhaUsuario' => $senhaUsuario, $tipo => 'tipo', 'chave_notificacao' => $chaveNotificacao);
        $dados = arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
        (isset($dados['data']) ? $dados['data']['data_nascimento'] = Util::formatDate($dados['data']['data_nascimento']) : null);
        return $dados;
    }

    //=================================
    //  PAGSEGURO
    //=================================

    public function SdsPagseguroInserirTransacao($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_PAGSEGURO_TRANSACAO_REGISTRAR), $dados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds', 'POST'), true));
    }

    public function SdsPagseguroCancelarTransacao($idVenda, $StatusTransacao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_PAGSEGURO_TRANSACAO_CANCELAR, 'id' => $idVenda, 'status_transacao' => $StatusTransacao);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsPagseguroAtualizarTransacao($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_PAGSEGURO_TRANSACAO_ATUALIZAR), $dados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds', 'POST'), true));
    }

    public function SdsPagseguroConsultarTransacao($idVenda)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_PAGSEGURO_TRANSACAO_CONSULTAR, 'id' => $idVenda);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    //=================================
    //  SITE
    //=================================

    public function SdsObterBannerEdicao($tipo, $idEdicao)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_BANNER_EDICAO, 'tipo' => $tipo, 'idEdicao' => $idEdicao);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds'), true));
    }

    public function SdsObterQuantidadeComprasEdicao($idEdicao, $idCliente)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_QUANTIDADE_COMPRAS_EDICAO, 'idEdicao' => $idEdicao, 'idCliente' => $idCliente);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds'), true));
    }

    public function SdsObterDatasExtracoes($idTipoLoteria)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_DATAS_EXTRACOES, 'idTipoLoteria' => $idTipoLoteria);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds'), true));
    }

    public function SdsObterHtmlCertificado($codigoVenda)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_OBTER_HTML_CERTIFICADO, 'codigoVenda' => $codigoVenda);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params, 'sds'), true));
    }

    public function SdsCep($cep)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CONSULTAR_CEP, 'CEP' => $cep);
        return arrayChangeKeyCase(json_decode(XmlToJson($this->getSdsServico($params, 'sdscep')), true));
    }

    public function SiteConsultarResultado($site, $dataSorteio)
    {

        return json_decode($this->cURL($this->getServerProtocol() . '://' . $site . '/' . $dataSorteio . '/resultado.json'), true);
    }

    //=================================
    //  PAGAMENTOS
    //=================================

    public function SdsSessaoCriar()
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_SESSAO_CRIAR);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsSessaoRemover($bilhetes)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_SESSAO_ADICIONAR, 'bilhetes' => $bilhetes);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsSessaoAdicionar($bilhetes)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_SESSAO_REMOVER, 'bilhetes' => $bilhetes);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsSalvarDadosCartao($dados)
    {

        $params = array_merge(array('tipoTransacao' => self::SDS_TRANSACAO_SALVAR_DADOS_CARTAO), $dados);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

    public function SdsConsultarDadosCartao($cpf)
    {

        $params = array('tipoTransacao' => self::SDS_TRANSACAO_CONSULTAR_DADOS_CARTAO, 'cpf_cliente' => $cpf);
        return arrayChangeKeyCase(json_decode($this->getSdsServico($params), true));
    }

}
