<?php

class PicpayApi {

    protected static function cURL($url, $data = null, $header = null , $post = false, $customRequest = null, $typeArray = true) {

        $precision = 1000;
        $microtime1 = microtime(true) * $precision;

        $curl = curl_init();
        $url = PicpayApi::getApiPath().$url;

        if(!empty($customRequest)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $customRequest);
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if($header != null) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        if($data != null && is_array($data)) {

            $data_string = array();
            foreach($data as $key=>$value) {
                $data_string[] = $key.'='.$value;
            }

            $data_string = implode('&',$data_string);

            if(!$post) {
                $url .= '?'.$data_string;
            } else {
                curl_setopt($curl, CURLOPT_POST, $post);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            }

        } else if(is_string($data)) {

            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        $ret = curl_exec($curl);
        curl_close($curl);
        $microtime2 = microtime(true) * $precision;
        $executiontime = '['.number_format((($microtime2 - $microtime1)/$precision), 5, '.', '').']';
        if(App::getConfig('logs', 'picpay')) {
            Log::logArquivo($executiontime.' RES:['.$url.'] - DADOS: ['.print_r($data, true).']  -  DEB:['.str_replace(array('\/', '/', "\n"), array('\"','"',''), Util::utf8D($ret))."]\n", LOG_PICPAY);
        }
        return ($typeArray ? Util::utf8D(json_decode($ret, true)) : $ret);
    }

    protected static function getApiPath() {

        return App::getConfig('picpay', 'path');
    }

    protected static function getApiHeader() {

        $config = App::getConfig('picpay');
        return array('Content-Type: application/json', 'x-picpay-token: '.$config['picpayToken']);
    }

    public static function criarPagamento($dados) {

        $url = '';
        $config = App::getConfig('picpay');
        $data = array
        (
            'referenceId' => $dados['referenceId'],
            'callbackUrl' => $config['callBackUrl'],
            'returnUrl'   => isset($config['returnUrl']) ? $config['returnUrl'] : '',
            'value'       => $dados['valor'],
            'buyer'       => array 
            (
            'firstName'   => $dados['nomeCliente'],
            'lastName'    => $dados['sobreNomeCliente'],
            'document'    => $dados['cpf'],
            'email'       => $dados['email'],
            'phone'       => $dados['telefone']
            )
        );

        $json = json_encode($data);
        return self::cURL($url, $json, self::getApiHeader(), false, 'POST');
    }

    public static function consultarPagamento($referenceId) {

        $url = '/'.$referenceId.'/status';
        return self::cURL($url, null, self::getApiHeader(), true, 'GET');
    }

    public static function cancelarPagamento($dados) {

        $json = null;
        $url = '/'.$dados['referenceId'].'/cancellations';
        if(isset($dados['authorizationId'])) {
            unset($dados['referenceId']);
            $json = json_encode($dados);
        }
        return self::cURL($url, $json, self::getApiHeader(), true, 'POST');
    }
}