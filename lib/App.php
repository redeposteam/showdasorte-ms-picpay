<?php

/**
 * Acesso global a principais objetos da aplicação.
 */

/**
 * Classe responsável pela distribuição de recursos para aplicação.
 *
 * @name 			 App
 * @version		 1.0
 * @access		 public
 * @package		 Remcom
 * @subpackage Lib
 * @copyright	 Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		 Ricardo Minzé <ricardominze@gmail.com>
 */
class App {

	private static $config;

	/**
	 * Retorna as configurações da aplicação.
	 *
	 * @return array
	 */
	public static function getConfig($conf = null, $prop = null) {

		$return = null;
		if(self::$config == null) {
			$arquivo = CONFIG_PATH.DS.'config.ini.php';
			$dados = parse_ini_file($arquivo, true);
			foreach ($dados as $key => $value) {
				foreach ($value as $key2 => $value2) {
					$dados[$key][$key2] = trim($value2);
				}
			}
			self::$config = $dados;
		} else {
			$dados = self::$config;
		}
		if(!empty($conf)) {
			if(!empty($prop)) {
				$return = (isset($dados[$conf][$prop]) ? $dados[$conf][$prop] : null);
			} else {
				$return = (isset($dados[$conf]) ? $dados[$conf] : null);
			}
		} else {
			$return = $dados;
		}
		return $return;
	}

	/**
	 * Retorna a conexão configurada para aplicação.
	 *
	 * @return Db
	 */
	public static function Db($new = false) {

		$config = self::getConfig();
		$db = $config['db'];
		$database = null;

		//CONEXÕES PDO

		if(strpos($db['sgbd'], 'pdo') !== false) {

		    switch ($db['sgbd']) {
		        case 'pdomysql':
		            $classeDb = 'DbPdoMysql';
		        case 'pdooci':
		            $classeDb = 'DbPdoOracle';
		            break;
		        case 'pdopgsql':
		            $classeDb = 'DbPdoPgsql';
		            break;
		    }

		    if($db['sgbd'] == 'pdooci') {
		        $database = new $classeDb(str_replace('pdo', '', $db['sgbd']).':dbname=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = '.$db['server'].')(PORT = '.$db['port'].')) ) (CONNECT_DATA = (SERVICE_NAME = '.$db['db'].') ));charset=UTF8', $db['user'], $db['password'], $db['log']);
		        $database->exec("alter session set nls_date_format = 'dd/mm/yyyy hh24:mi:ss'");
		    } else {
		        $database = new $classeDb(str_replace('pdo', '', $db['sgbd']).':host='.$db['server'].';port='.$db['port'].';dbname='.$db['db'], $db['user'], $db['password'], $db['log']);
		    }

	    //CONEXÕES ANTIGAS

		} else {

		    switch ($db['sgbd']) {
		        case 'mysql':
		            $classeDb = 'DbMysql';
		        case 'oci':
		            $classeDb = 'DbOracle';
		            break;
		        case 'pgsql':
		            $classeDb = 'DbPgsql';
		            break;
		    }

		    if($db['sgbd'] == 'oci') {
				$method = 'getInstance';
				if($new){
					$method = 'newInstance';
				}
		        $database = $classeDb::$method($db['server'].':'.$db['port'].'/'.$db['db'], $db['user'], $db['password'], $db['log'], array('charset' => 'AL32UTF8'));
		        $database->exec("alter session set nls_date_format = 'dd/mm/yyyy hh24:mi:ss'");
		    } else {
		        $database = $classeDb::getInstance($db['sgbd'].':host='.$db['server'].';port='.$db['port'].';dbname='.$db['db'], $db['user'], $db['password'], $db['log']);
		    }
		}

		return $database;
	}

	/**
	 * Gera um log de informação, no arquivo (/log/log.txt).
	 *
	 * @param string $info
	 * @return void
	 */
	public static function log($info) {

		Log::logArquivo($info);
	}
}
