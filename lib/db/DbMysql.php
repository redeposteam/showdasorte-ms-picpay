<?php

/**
 * Abstração de Classe de Dados.
 */

/**
 * Db
 */

require_once 'lib/db/Db.php';

/**
 * Responsável pela comunicação exclusiva com MySQL.
 */

/**
 * Extensão da Classe Db responsável pela comunicação com bases de dados, voltada para MySQL.
 *
 * @name		DbMysql
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class DbMysql extends Db {
	
	/**
	 * Lista as tabelas do banco de dados atual
	 *
	 * @return array
	 */
	public function listTables($esquema = null) {
		
		return $this->getPDO()->query("SHOW FULL TABLES")->fetchAll(PDO::FETCH_COLUMN, 0);
	}

	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 * 
	 * @param string $tabela Nome da tabela
	 * @return array
	 */
	public function metadataFk($tabela) {
		
		$config = App::getConfig();
		
		return $this->query("SELECT
									table_name AS tabela_origem,
									column_name AS campo_origem,
									referenced_table_name AS tabela_referencia,
									referenced_column_name AS campo_referencia,
									constraint_name AS 'constraint'
							   FROM
									information_schema.key_column_usage
							   WHERE
									referenced_table_name IS NOT NULL
									AND constraint_schema = '".$config['db']['db']."'
									AND table_name = '".$tabela."'
							   ORDER BY
									table_name, referenced_table_name")->fetchAll(PDO::FETCH_ASSOC);		
	}

	/**
	 * Lista os metadados da tabela passada como parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @param string $esquema Nome do esquema
	 * @return array
	 */
	public function metadataTable($tabela, $esquema = null) {

		if ($esquema) {
			$sql = 'DESCRIBE ' . $esquema.'.'.$tabela;
		} else {
			$sql = 'DESCRIBE ' . $tabela;
		}
		$result = $this->queryFetchAllNum($sql);
		
		$field   = 0;
		$type    = 1;
		$null    = 2;
		$key     = 3;
		$default = 4;
		$extra   = 5;
		
		$desc = array();
		$i = 1;
		$p = 1;
		foreach ($result as $row) {
			list($length, $scale, $precision, $unsigned, $primary, $primaryPosition, $identity) = array(null, null, null, null, false, null, false);
			if (preg_match('/unsigned/', $row[$type])) {
				$unsigned = true;
			}
			if (preg_match('/^((?:var)?char)\((\d+)\)/', $row[$type], $matches)) {
				$row[$type] = $matches[1];
				$length = $matches[2];
			} else if (preg_match('/^decimal\((\d+),(\d+)\)/', $row[$type], $matches)) {
				$row[$type] = 'decimal';
				$precision = $matches[1];
				$scale = $matches[2];
			} else if (preg_match('/^((?:big|medium|small|tiny)?int)\((\d+)\)/', $row[$type], $matches)) {
				$row[$type] = $matches[1];
			}
			if (strtoupper($row[$key]) == 'PRI') {
				$primary = true;
				$primaryPosition = $p;
				if ($row[$extra] == 'auto_increment') {
					$identity = true;
				} else {
					$identity = false;
				}
				++$p;
			}
			$desc[strtolower($row[$field])] = array(
					'SCHEMA_NAME'      => null,
					'TABLE_NAME'       => strtolower($tabela),
					'COLUMN_NAME'      => strtolower($row[$field]),
					'COLUMN_POSITION'  => $i,
					'DATA_TYPE'        => $row[$type],
					'DEFAULT'          => $row[$default],
					'NULLABLE'         => (bool) ($row[$null] == 'YES'),
					'LENGTH'           => $length,
					'SCALE'            => $scale,
					'PRECISION'        => $precision,
					'UNSIGNED'         => $unsigned,
					'PRIMARY'          => $primary,
					'PRIMARY_POSITION' => $primaryPosition,
					'IDENTITY'         => $identity
			);
			++$i;
		}
		
		$fks = $this->metadataFk($tabela);
		
		return array('FIELDS' => $desc, 'FKS' => $fks);
	}
	
	/**
	 * Adiciona a cláusula LIMIT na consulta.
	 *
	 * @param mixed $sql
	 * @param int $count
	 * @param int $offset
	 * @return string
	 */
	public function limit($sql, $count, $offset = 0) {
	
		$count = intval($count);
	
		if ($count <= 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT count=$count não é válido");
		}
	
		$offset = intval($offset);
	
		if ($offset < 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT offset=$offset não é válido");
		}
	
		$sql .= " LIMIT $count";
	
		if ($offset > 0) {
			$sql .= " OFFSET $offset";
		}
	
		return $sql;
	}
	
	/**
	 * Retorna o ID da última linha inserida ou valor de sequência.
	 *
	 * @param string $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public function lastSequenceId($sequenceName) {
		
		return null;
	}

	/**
	 * Gera um novo valor para sequência especificada e a retorna.
	 *
	 * @param string $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public function nextSequenceId($sequenceName) {
		
		return null;
	}
	
	/**
	 * Gera um novo valor para sequência especificada e a retorna.
	 *
	 * @param string $tableName Nome da tabela.
	 * @param string $primaryKey Nome da chave primaria da tabela.
	 * @return int
	 */
	public function lastInsertId($tableName = null, $primaryKey = null, $esquema = null, $sequence = null) {

		return $this->getPDO()->lastInsertId($tableName);
	}
	
}