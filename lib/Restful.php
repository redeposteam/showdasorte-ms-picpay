<?php

/**
 * Tratamento de Requisições HTTP.
 */

/**
 * Classe responsável pelo tratamento de requisições Restful.
 *
 * @name		Restful
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Restful {

	const HTTP_VERSION 	   = 'HTTP/1.1';
	const METHOD_GET       = 'GET';
	const METHOD_PUT       = 'PUT';
	const METHOD_POST      = 'POST';
	const METHOD_DELETE    = 'DELETE';
	const APPLICATION_PNG  = 'image/png';
	const APPLICATION_JPG  = 'image/jpg';
	const APPLICATION_XML  = 'application/xml';
	const APPLICATION_JSON = 'application/json';
	const APPLICATION_MPFD = 'multipart/form-data';
	const APPLICATION_FORM = 'application/x-www-form-urlencoded';
	
	/**
	 * Guarda possíveis estados de mensagem HTTP/1.0
	 * @var array
	 */
	public static $HTTP_STATUS = array(
			
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported'
	);

	/**
	 *
	 * @var mixed Dados da requisição
	 */
	private $data;
	
	/**
	 * Guarda dados adicionais para requisicao antes do tratamento final
	 *
	 * @return array
	 */
	protected $dataRequest;
	
	/**
	 * Retorna a mensagem correspondente ao código passado como argumento
	 *
	 * @param int $status código da mensagem de status HTTP/1.0
	 * @return string Nome do status
	 */
	public function getHttpStatusMessage($status) {
	
		return self::$HTTP_STATUS[$status];
	}

	/**
	 * Seta o código de status no cabeçalho
	 *
	 * @param int $status código da mensagem de status HTTP/1.0
	 * @return void
	 */
	public function setHttpHeader($status) {
	    
	    header(self::HTTP_VERSION.' '.$status.' '.$this->getHttpStatusMessage($status));
	}
	
	/**
	 * Seta uma variavel no cabeçalho
	 *
	 * @param string $name nome da variável
	 * @param string $value valor da variável
	 * @return void
	 */
	public function setHttpVarHeader($name, $value) {
	    
	    header($name.': '.$value);
	}
	
	/**
	 * Seta o tipo de conteúdo da mensagem e o charset no cabeçalho
	 *
	 * @param int $type Tipo de conteúdo
	 * @param int $charset Códificação
	 * @return void
	 */
	public function setHttpContetType($type, $charset = 'utf-8') {

	    $this->setHttpVarHeader('Content-Type', $type.'; charset='.$charset);
	}
	
	/**
	 * Converte os dados recebidos da requisição em array
	 *
	 * @return array
	 */
	public function toArray() {
		
		$return = $this->data;
		if(!is_array($this->data)) {
			$array = array();
			$fields = explode('&', $this->data);
			foreach ($fields as $field) {
				$temp2 = explode('=', $field);
				if(isset($temp2[1])){
					$array[$temp2[0]] = $temp2[1];
				} else {
					$array[$temp2[0]] = null;
				}
			}
			$return = $array;
		}
		return $return;
	}	

	/**
	 * Adiciona dados na requisicao ante do tratamento final
	 *
	 * @param string $key Nome do novo atributo
	 * @param string $value Valor do novo atributo
	 * @return Restful
	 */
	public function setDataRequest($key, $value) {

		$this->dataRequest[$key] = $value;
		return $this;
	}
	
	/**
	 * Trata a requisição HTTP e manda os dados para o atributo $data
	 *
	 * @return Restful
	 */
	public function request() {
		
		switch ($_SERVER['REQUEST_METHOD']) {
			
			case self::METHOD_GET:
				
				$this->data = $_GET;
				
			break;
			
			case self::METHOD_PUT: case self::METHOD_POST: case self::METHOD_DELETE:

			    if(isset($_SERVER['CONTENT_TYPE'])) {
			    
    				switch ($_SERVER['CONTENT_TYPE']) {
    
    					case self::APPLICATION_FORM:
    					
    						$this->data = file_get_contents('php://input');
    					
    					break;
    					
    					case self::APPLICATION_JSON:
    						
    						$this->data = json_decode(file_get_contents('php://input'), true);
    						
    					break;
    					
							default:
							
								if(strpos($_SERVER['CONTENT_TYPE'], self::APPLICATION_FORM) !== false) {

									$this->data = file_get_contents('php://input');

								} else if(strpos($_SERVER['CONTENT_TYPE'], self::APPLICATION_MPFD) !== false) {

									$this->data = $_FILES;

								} else {

									$this->data = json_decode(file_get_contents('php://input'), true);
								}

    					break;
    				}
    				
			    } else {
			        
			        $this->data = json_decode(file_get_contents('php://input'), true);
			    }
				
			break;

			default:
							
				if(strpos($_SERVER['CONTENT_TYPE'], self::APPLICATION_FORM) !== false) {
					$this->data = file_get_contents('php://input');
				} else {
					$this->data = json_decode(file_get_contents('php://input'), true);
				}

			break;
		}
		
		if(!empty($this->dataRequest)) {
			if(!empty($this->data)){
				$this->data = array_merge($this->data, $this->dataRequest);
			} else {
				$this->data = $this->dataRequest;
			}
		}
		
		return $this;
	}

	/**
	 * Formata uma resposta HTTP
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $data Dados a serem enviados
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function response($status, $data, $type = Restful::APPLICATION_JSON) {

		$this->setHttpHeader($status);
		
		$this->setHttpContetType($type);
		
		switch ($type) {
			
			case self::APPLICATION_JSON:
				
				$data = json_encode($data);
				
			break;
			
			case self::APPLICATION_XML:
				
				$data = json_encode($data);
				
			break;
		}
		
		return $data;
	}
	
	/**
	 * Formata uma resposta HTTP para arquivos
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $file Caminho do arquivo a ser processado
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function responseImage($status, $file) {

	    $this->setHttpHeader($status);
	    $this->setHttpVarHeader('Content-Type', 'image/'.pathinfo($file, PATHINFO_EXTENSION));
	    $this->setHttpVarHeader('Content-Length', filesize($file));
	    
		return file_get_contents($file);
	}
	
	/**
	 * Formata uma resposta HTTP para arquivos
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $file Caminho do arquivo a ser processado
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function responseImage64($status, $file) {

	    $type = pathinfo($file, PATHINFO_EXTENSION);
	    $data = file_get_contents($file);
	    $dataUri = 'data:image/'.$type.';base64,'.base64_encode($data);
		
	    return $dataUri;
	}

	/**
	 * Obtem o cabeçalho da resposta HTTP
	 *
	 * @param string $header Se passado, irá procurar por esta chave no cabeçalho e retornar seu valor
	 * @return string|array Trecho ou todo o cabeçalho
	 */
	public function getHeader($header = null) {

	    $headers = array();
        
	    foreach ($_SERVER as $name => $value) {
	        if (substr($name, 0, 5) == 'HTTP_') {
	            $name = strtoupper(substr($name, 5));
	            $headers[$name] = $value;
	        } else if ($name == "CONTENT_TYPE") {
	            $headers["Content-Type"] = $value;
	        } else if ($name == "CONTENT_LENGTH") {
	            $headers["Content-Length"] = $value;
	        }
	    }

		if(!empty($header)) {
			return (isset($headers[strtoupper($header)]) ? $headers[strtoupper($header)] : null);
		} else {
			return $headers;
		}
	}

	/**
	 * Obtem o método utilizado na requisição HTTP
	 *
	 * @return string Desolve o REQUEST_METHOD
	 */
	public function getRequestMethod() {
		
		return $_SERVER['REQUEST_METHOD'];
	}

	/**
	 * Converte os dados para o formato POST string
	 * 
	 * Exemplo:
	 * nome=Joao&idade=35
	 *
	 * @return string Desolve o REQUEST_METHOD
	 */
	public function __toString() {
	
		$string = $this->data;
		if(is_array($this->data)) {
			$array = array();
			foreach ($this->data as $field => $value) {
				$array[] = $field.'='.$value;
			}
			$string = implode('&', $array);
		}
	
		return $string;
	}
}
