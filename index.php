<?php

ini_set('max_execution_time', 0);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, AccessToken, CsrfToken, AppVersion, Device');
header('Access-Control-Max-Age: 86400');

if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
	exit(0);
}

include 'config/constantes.php';include LIB_PATH.DS.'App.php';

$config = App::getConfig();
define('SERVER_VER', $config['server']['version']);
define('SERVER_DEV', $config['server']['development']);
define('MVC_ON', $config['mvc']['ativo']);
define('MODULE_ACTIVE', $config['mvc']['module_active']);
define('CONTROLLER_DIR',$config['mvc']['controller_dir']);

include LIB_PATH.DS.'Util.php';
include LIB_PATH.DS.'Log.php';
include LIB_PATH.DS.'Erro.php';
include LIB_PATH.DS.'Formato.php';
include LIB_PATH.DS.'Exception.php';
include LIB_PATH.DS.'Diretorio.php';
include LIB_PATH.DS.'Bootstrap.php';
include LIB_PATH.DS.'db'.DS.'DbOracle.php';
include LIB_PATH.DS.'db'.DS.'DbPdoOracle.php';

if(MVC_ON){
    include LIB_PATH.DS.'Controller.php';
} else {
    include LIB_PATH.DS.'ControllerRestful.php';
}

if(SERVER_DEV) {
    include LIB_PATH.DS.'db'.DS.'Orm.php';
}

if(PHP_MAIJOR_VERSION < 7) {

   /**
     * Função manipuladora de erros do sistema.
     *
     * @name		Error_Handler
     * @version		1.0
     * @access		public
     * @package		Remcom
     * @subpackage	Lib
     * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
     * @author		Ricardo Minzé <ricardominze@gmail.com>
     *
     * @param int $errno
     * @param string $errmsg
     * @param string $filename
     * @param string $linenum
     * @param string $vars
     */
    function Error_Handler($errno, $errmsg, $filename, $linenum, $vars) {
        $Error = new Erro($errno, html_entity_decode(Util::utf8D($errmsg)), $filename, $linenum, $vars);
        $Error->log();
    }
    set_error_handler('Error_Handler', E_ALL);
    
    function Error_Handler_Shutdown() {
        $last_error = error_get_last();
        if ($last_error['type'] === E_ERROR) {
            Error_Handler(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line'], null);
        }
    }    
    register_shutdown_function('Error_Handler_Shutdown');
    
    /**
     * Função manipuladora de exceções do sistema.
     *
     * @name		Exception_Handler
     * @version		1.0
     * @access		public
     * @package		Remcom
     * @subpackage	Lib
     * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
     * @author		Ricardo Minzé <ricardominze@gmail.com>
     *
     * @param Exception $e
     */
    function Exception_Handler(Exception  $e) {
        $Exception = new Exceptionn(Util::utf8D($e->getMessage()), $e->getCode(), $e->getFile(), $e->getLine());
        $Exception->log();
    }
    set_exception_handler('Exception_Handler');

} else {
    
    /**
     * Função manipuladora de exceções do sistema PHP 7.
     *
     * @name		Exception_Handler2
     * @version		1.0
     * @access		public
     * @package		Remcom
     * @subpackage	Lib
     * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
     * @author		Ricardo Minzé <ricardominze@gmail.com>
     *
     * @param Exception $e
     */
    function Exception_Handler2(Throwable  $e) {
        $Exception = new Exceptionn(Util::utf8D($e->getMessage()), $e->getCode(), $e->getFile(), $e->getLine());
        $Exception->log();
    }
    set_exception_handler('Exception_Handler2');
}

Bootstrap::dispatch();
