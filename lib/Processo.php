<?php

class Processo {

	const LIN = 1;
	const WIN = 2;

	private $ini;
	private $params;
	private $sistema;
	private $executavel;
	private $includePath;
	private $kill = array();
	private $processos = array();

	public function __construct($ini = null, $includePath = null, $params = array()) {

		date_default_timezone_set('America/Recife');

		$this->ini = $ini;
		$this->params = $params;
		$this->includePath = $includePath;

		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$this->sistema = self::WIN;
		} else {
			$this->sistema = self::LIN;
		}

		$this->processos = array(
			self::LIN => 'ps -f -C php | awk \'{print $1 "," $2 "," $8 " " $9 " " $10 " " $11 " " $12 " " $13 " " $14}\'',
			self::WIN => 'WMIC PROCESS WHERE (Name=\'php.exe\') GET ProcessId, CommandLine /format:csv'
		);

		$this->kill = array(
				self::LIN => 'kill -9 ',
				self::WIN => 'taskkill /pid '
		);
	}

	public function sistemaOperarional() {

		return $this->sistema;
	}

	private function exec($command) {

		return exec($command);
	}

	private function execShell($command) {

		return shell_exec($command);
	}

	private function formatarProcessos() {

		$processos = array();
		$dados = $this->execShell($this->processos[$this->sistema]);

		if($this->sistema == self::LIN) {

			$linhas = explode("\n", $dados);
			for($i = 1 ; $i < count($linhas)-1 ; $i++) {
				$processo = explode(',',$linhas[$i]);
				preg_match('|(/[^/]*)+\\.php\s(\s-f)?|', $processo[2], $matches);
				$command = $matches[0];
				preg_match('|(-idp=[0-9]+)|', $processo[2], $matches);
				if(isset($matches[0])){
					$idp = substr($matches[0], 5,strlen($matches[0]));
				} else {
					$idp = 0;
				}
				$processos[$idp] = array('pid' => $processo[1], 'user' => $processo[0], 'cmd' => $command);
			}
				
		} else if($this->sistema == self::WIN) {

			$linhas = str_replace('"','',explode("\n", $dados));
			for($i = 2 ; $i < count($linhas)-1 ; $i++) {
				$processo = explode(',',$linhas[$i]);
				preg_match('|([A-Z]:\\\[^\\\]*)+\\.php\s(\s-f)?|', $processo[1], $matches);
				if(isset($matches[0])){
					$command = $matches[0];
				} else {
					$command = $processo[1];
				}
				preg_match('|(-idp=[0-9]+)|', $processo[1], $matches);
				$idp = substr($matches[0], 5,strlen($matches[0]));
				if(isset($matches[0])){
					$idp = substr($matches[0], 5,strlen($matches[0]));
				} else {
					$idp = 0;
				}
				$processos[$idp] = array('pid' => $processo[2], 'user' => $processo[0], 'cmd' => $command);
			}
		}
		return $processos;
	}

	public function obterProcessos() {
			
		return $this->formatarProcessos();
	}

	public function iniciarProcesso($script, $return = false) {

		$params = '';
		foreach ($this->params as $key => $val) {
			$params.= ' --'.$key.'='.$val;
		}

		$idp = date('YmdHis');

		if($this->sistema == self::LIN) {

			$commandString = 'php -c '.$this->ini.(!empty($this->includePath) ? ' -d  include_path='.$this->includePath.' ' : ' ').$script.' --idp='.$idp.' '.$params;

			if($return) {
				return $commandString;
			} else {
				$this->exec($commandString);
			}

		} else if($this->sistema == self::WIN) {

			$commandString = 'start /b php -c '.$this->ini.(!empty($this->includePath) ? ' -d  include_path='.$this->includePath.' ' : ' ').$script.' --idp='.$idp.' '.$params;

			if($return) {
				return $commandString;
			} else {
				pclose(popen($commandString, "r"));
			}
		}

		return $idp;
	}

	public function terminarProcesso($pid) {
			
		$this->execShell($this->kill[$this->sistema].$pid.($this->sistema == self::WIN ? ' /f' : ''));
	}
}