<?php

/**
 * Abstração de Classe de Dados.
 */

/**
 * Db
 */

require_once 'lib/db/Db.php';

/**
 * Responsável pela comunicação exclusiva com PostgreSQL.
 */

/**
 * Extensão da Classe Db responsável pela comunicação com bases de dados, voltada para PostgreSQL.
 *
 * @name		DbPgsql
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class DbPgsql extends Db {
	
	/**
	 * Lista as tabelas do banco de dados atual
	 *
	 * @return array
	 */
	public function listTables($esquema = null) {

		$sql = "SELECT 
					c.relname AS table_name
				FROM 
					pg_class c, pg_user u
				WHERE 
					c.relowner = u.usesysid 
				AND c.relkind = 'r'
				AND NOT EXISTS (SELECT 1 FROM pg_views WHERE viewname = c.relname)
				AND c.relname !~ '^(pg_|sql_)'
				UNION
				SELECT 
					c.relname AS table_name
				FROM 
					pg_class c
				WHERE 
					c.relkind = 'r'
				AND NOT EXISTS (SELECT 1 FROM pg_views WHERE viewname = c.relname)
				AND NOT EXISTS (SELECT 1 FROM pg_user WHERE usesysid = c.relowner)
				AND c.relname !~ '^pg_'";

		return $this->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);
		
	}
	
	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @param string $esquema Nome do esquema
	 * @return array
	 */
	public function metadataTable($tabela, $esquema = null) {

		$sql = "SELECT
                a.attnum,
                n.nspname,
                c.relname,
                a.attname AS colname,
                t.typname AS type,
                a.atttypmod,
                FORMAT_TYPE(a.atttypid, a.atttypmod) AS complete_type,
                d.adsrc AS default_value,
                a.attnotnull AS notnull,
                a.attlen AS length,
                co.contype,
                ARRAY_TO_STRING(co.conkey, ',') AS conkey
            FROM 
				pg_attribute AS a
                JOIN pg_class AS c ON a.attrelid = c.oid
                JOIN pg_namespace AS n ON c.relnamespace = n.oid
                JOIN pg_type AS t ON a.atttypid = t.oid
                LEFT OUTER JOIN pg_constraint AS co ON (co.conrelid = c.oid
                AND a.attnum = ANY(co.conkey) AND co.contype = 'p')
                LEFT OUTER JOIN pg_attrdef AS d ON d.adrelid = c.oid AND d.adnum = a.attnum
            WHERE 
				a.attnum > 0 AND c.relname = ".$this->quote($tabela);
		
		if ($esquema) {
			$sql .= " AND n.nspname = ".$this->quote($esquema);
		}
		$sql .= ' ORDER BY a.attnum';

		$result = $this->queryFetchAllNum($sql);
		
		$attnum        = 0;
		$nspname       = 1;
		$relname       = 2;
		$colname       = 3;
		$type          = 4;
		$atttypemod    = 5;
		$complete_type = 6;
		$default_value = 7;
		$notnull       = 8;
		$length        = 9;
		$contype       = 10;
		$conkey        = 11;
		
		$desc = array();
		
		foreach ($result as $key => $row) {
			
			if ($row[$type] == 'varchar') {
				if (preg_match('/character varying(?:\((\d+)\))?/', $row[$complete_type], $matches)) {
					if (isset($matches[1])) {
						$row[$length] = $matches[1];
					} else {
						$row[$length] = null;
					}
				}
			}
			
			list($primary, $primaryPosition, $identity) = array(false, null, false);
			
			if ($row[$contype] == 'p') {
				$primary = true;
				$primaryPosition = array_search($row[$attnum], explode(',', $row[$conkey])) + 1;
				$identity = (bool) (preg_match('/^nextval/', $row[$default_value]));
			}
			
			$desc[strtolower($row[$colname])] = array(
					'SCHEMA_NAME'      => strtolower($row[$nspname]),
					'TABLE_NAME'       => strtolower($row[$relname]),
					'COLUMN_NAME'      => strtolower($row[$colname]),
					'COLUMN_POSITION'  => $row[$attnum],
					'DATA_TYPE'        => $row[$type],
					'TYPE'    		   => $row[$complete_type],
					'DEFAULT'          => $row[$default_value],
					'NULLABLE'         => (bool) ($row[$notnull] != 't'),
					'LENGTH'           => $row[$length],
					'SCALE'            => null,
					'PRECISION'        => null,
					'UNSIGNED'         => null,
					'PRIMARY'          => $primary,
					'PRIMARY_POSITION' => $primaryPosition,
					'IDENTITY'         => $identity
			);
		}
		return $desc;		
	}

	/**
	 * Retorna o ID da última linha inserida ou valor de sequência.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public function lastSequenceId($sequenceName) {
	
		return $this->query("SELECT CURRVAL(".$this->quote($sequenceName).")")->fetchColumn();
	}
	
	/**
	 * Gera um novo valor para sequência especificada e a retorna.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public function nextSequenceId($sequenceName) {
	
		return $this->query("SELECT NEXTVAL(".$this->quote($sequenceName).")")->fetchColumn();
	}
	
	/**
	 * Retorna o ID da última linha inserida.
	 *
	 * @param string $tableName   OPTIONAL Nome da tabela.
	 * @param string $primaryKey  OPTIONAL Nome da coluna chave primaria.
	 * @return string
	 */
	public function lastInsertId($tableName = null, $primaryKey = null, $esquema = null, $sequence = null)	{
	
	    $sequenceName = null;
	    if($tableName != null) {
	        if($sequence == null) {
	            $sequenceName = $esquema.'.'.$tableName.'_seq';
	            if($primaryKey){
	                $sequenceName.= '_'.$primaryKey;
	            }
	        } else {
	            $sequenceName = $esquema.'.'.$sequence;
	        }
	        return $this->lastSequenceId($sequenceName);
	    }
	    return $this->getPDO()->lastInsertId(($esquema ? $esquema.'.' : '').$tableName);
	}
}