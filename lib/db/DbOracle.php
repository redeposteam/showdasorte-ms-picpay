<?php

/**
 * Abstração de Classe de Dados.
 */

/**
 * Db
 */

require_once 'lib/db/Db.php';

/**
 * Criação de Consultas SQL.
 */

/**
 * Select
 */

require_once 'lib/db/Select.php';

/**
 * Criação de Expresões SQL.
 */

/**
 * Expression
 */

require_once 'lib/db/Expression.php';

/**
 * Model
 */

require_once 'lib/db/Model.php';

/**
 * Repository
 */

require_once 'lib/db/Repository.php';

/**
 * Define de forma abstrata os métodos que deverão ser 
 * implementados pelas classes filhas.
 */

/**
 * Classe responsável pela comunicação com bases de dados.
 * 
 * @name		Db
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class DbOracle extends Db {
	
	private $queryStid;
	private $connection;
	private $autoTransaction;
	
	/**
	 * Construtor da classe
	 *
	 * @param String $dsn
	 * @param String $username Nome do usuário do base de dados.
	 * @param String $password Senha do usuário do base de dados.
	 * @param bool 	 $log Ativa o log de registro de consultas.
	 * @param array  $options
	 * @return bool
	 */
	private function __construct($dsn, $username = null, $password = null, $log = false, $options = array()) {

	    self::$log = $log;
	    $this->autoTransaction = OCI_COMMIT_ON_SUCCESS;
	    $this->connection = oci_connect($username, $password, $dsn, (isset($options['charset']) ? $options['charset'] : 'WE8ISO8859P1'));
	}

	/**
	 * Retorna a classe DbOracle.
	 *
	 * @return DbOracle
	 */
	public static function getInstance($dsn, $username = null, $password = null, $log = false, $options = array()) {
	    
	    if(!self::$Instance) {
	       $c = __CLASS__; 
	       self::$Instance = new $c($dsn, $username, $password, $log, $options);
	    }
		return self::$Instance;
	}

	/**
	 * Retorna a classe DbOracle.
	 *
	 * @return DbOracle
	 */
	public static function newInstance($dsn, $username = null, $password = null, $log = false, $options = array()) {
	    
		$c = __CLASS__; 
	 	return new $c($dsn, $username, $password, $log, $options);
	}
	
	public static function array_change_key_case_recursive($array, $case = CASE_LOWER) {
	    
	    if(!empty($array)) {
    	    return array_map(function($item) use ($case) {
    	        if(is_array($item)){
    	            $item = DbOracle::array_change_key_case_recursive($item, $case);
    	        }
    	        return $item;
    	    },array_change_key_case($array, $case));
	    } else {
	        return $array;
	    }
	}
	
	/**
	 * Inicia um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public function beginTransaction() {
	 
	    $this->autoTransaction = OCI_NO_AUTO_COMMIT;
	}

	/**
	 * Confirma finaliza um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public function commit() {
	    
	    oci_commit($this->connection);
	}

	/**
	 * Reverte uma transação.
	 *
	 * @return bool
	 */
	public function rollBack(){
	    
	    oci_rollback($this->connection);
	}
	
	/**
	 * Busca o SQLSTATE associado com a última operação no manipulador da base de dados.
	 *
	 * @return string
	 */
	public function errorCode() {
	    
	    $error = oci_error($this->connection);
	    return array('code' => $error['code'], 'message' => $error['message']);
	}

	/**
	 * Busca informações sobre o erro associado com a última operação no manipulador da base de dados.
	 *
	 * @return array
	 */
	public function errorInfo() {
	    
	    $error = oci_error($this->queryStid);
	    return array('code' => $error['code'], 'message' => $error['message']);
	}

	/**
	 * Executa uma instrução SQL e retorna o número de linhas afetadas.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return int Número de linhas afetadas.
	 */
	public function exec($sql) {
	    
	    self::log($sql);
	    $this->queryStid = oci_parse($this->connection, $sql);
	    return oci_execute($this->queryStid, $this->autoTransaction);
	}

	/**
	 * Executa uma instrução SQL, retornando um conjunto de resultados como um objeto PDOStatement.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return DbOracle
	 */
	public function query($sql) {

	    self::log($sql);
	    $this->queryStid = oci_parse($this->connection, $sql);
	    oci_execute($this->queryStid, $this->autoTransaction);
	    return $this;
	}

	public function fetch($mode = null) {

	    $res = oci_fetch_array($this->queryStid, OCI_ASSOC);
	    oci_free_statement($this->queryStid);
	    return $this->array_change_key_case_recursive($res);
	}
	
	public function fetchColumn($numberColumn = 0) {

	    $res = oci_fetch_row($this->queryStid);
	    oci_free_statement($this->queryStid);
	    return $res[$numberColumn];
	}

	public function fetchAll($mode) {

	    oci_fetch_all($this->queryStid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
	    oci_free_statement($this->queryStid);
	    return $this->array_change_key_case_recursive($res);
	}
	
	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchAllNum($sql) {

	    $this->query($sql);
	    oci_fetch_all($this->queryStid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_NUM);
	    oci_free_statement($this->queryStid);
	    return $this->array_change_key_case_recursive($res);
	}
	
	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchAllAssoc($sql) {

	    $this->query($sql);
	    oci_fetch_all($this->queryStid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
	    oci_free_statement($this->queryStid);
	    return $this->array_change_key_case_recursive($res);
	}

	/**
	 * Executa a consulta e retorna uma linha em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchRowAssoc($sql){

	    $this->query($sql);
	    oci_fetch_all($this->queryStid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
	    oci_free_statement($this->queryStid);
	    return current($this->array_change_key_case_recursive($res));
	}

	/**
	 * Executa a consulta e seleciona apenas uma coluna.
	 *
	 * @param  string $sql Instrução SQL válida para bancos de dados.
	 * @return mixed
	 */
	public function queryFetchColAssoc($sql) {
	    
	    $this->query($sql);
	    $res = oci_fetch_assoc($this->queryStid);
	    oci_free_statement($this->queryStid);
	    return array_slice($this->array_change_key_case_recursive($res),0,1);
	}

	public function bind($sql, $binds) {

	    self::log($sql."\n".print_r($binds, true));
	    $this->queryStid = oci_parse($this->connection, $sql);
	    foreach($binds as $key => $bind) {
	        oci_bind_by_name($this->queryStid, $key, $binds[$key]);
	    }
	    oci_execute($this->queryStid);
	    return $this;
	}
	
	/**
	 * Coloca entre aspas expressões necessárias da consulta.
	 *
	 * @param string $input
	 * @param int $parameter_type
	 * @return string
	 */
	public function quote($input, $parameter_type = 0) {
	    return "'".$input."'";
	}

	public function foldCase($key) {
	    return strtolower((string) $key);
	}
	
	/**
	 * Lista as tabelas do banco de dados atual
	 *
	 * @return array
	 */
	public function listTables($esquema = null) {
	    
		$sql = "SELECT table_name FROM all_tables".($esquema ? " WHERE OWNER = '".$esquema."'" : "");
		return $this->queryFetchAllNum($sql);
	}
	
	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @return array
	 */
	public function metadataFk($tabela) {
	    
	    $config = App::getConfig();
	    
	    return $this->queryFetchAllAssoc("SELECT * FROM (
                                            SELECT
                                              a.table_name AS tabela_origem,
                                              rtrim(MAX(DECODE(c.position,1,c.column_name)) ||',' || MAX(DECODE(c.position,2,c.column_name)) ||','|| MAX(DECODE(c.position,3,c.column_name)) ||','|| MAX(DECODE(c.position,4,c.column_name)),',') AS campo_origem,
                                              b.table_name AS tabela_referencia,
                                              rtrim(MAX(DECODE(d.position,1,d.column_name)) ||','|| MAX(DECODE(d.position,2,d.column_name)) ||',' || MAX(DECODE(d.position,3,d.column_name)) ||',' || MAX(DECODE(d.position,4,d.column_name)),',') AS campo_referencia
                                            FROM
                                              all_constraints a,
                                              all_constraints b,
                                              all_cons_columns c,
                                              all_cons_columns d
                                            WHERE
                                              a.r_constraint_name = b.constraint_name
                                              AND a.constraint_name = c.constraint_name
                                              AND b.constraint_name = d.constraint_name
                                              AND a.constraint_type = 'R'
                                              AND b.constraint_type IN ('P', 'U')
                                              AND a.table_name = '".$tabela."'
                                            GROUP BY a.table_name, b.table_name ORDER BY 1
                                          ) FOO ORDER BY tabela_origem, tabela_referencia");
	}
	
	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @param string $esquema Nome do esquema
	 * @return array
	 */
	public function metadataTable($tabela, $esquema = null) {
	    
	    $sql = "SELECT
                    TC.TABLE_NAME,
		            TB.OWNER,
		            TC.COLUMN_NAME,
		            TC.DATA_TYPE,
                    TC.DATA_DEFAULT,
		            TC.NULLABLE,
		            TC.COLUMN_ID,
		            TC.DATA_LENGTH,
                    TC.DATA_SCALE,
		            TC.DATA_PRECISION,
		            C.CONSTRAINT_TYPE,
		            CC.POSITION
            FROM ALL_TAB_COLUMNS TC
            LEFT JOIN (ALL_CONS_COLUMNS CC JOIN ALL_CONSTRAINTS C ON
		              (CC.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND CC.TABLE_NAME = C.TABLE_NAME AND C.CONSTRAINT_TYPE = 'P'))
		              ON TC.TABLE_NAME = CC.TABLE_NAME AND TC.COLUMN_NAME = CC.COLUMN_NAME
            JOIN ALL_TABLES TB ON (TB.TABLE_NAME = TC.TABLE_NAME AND TB.OWNER = TC.OWNER)
            WHERE UPPER(TC.TABLE_NAME) = UPPER(".$this->quote($tabela).")";
	    
	    if ($esquema) {
	        $sql .= " AND UPPER(TB.OWNER) = UPPER(".$this->quote($esquema).")";
	    }
	    $sql.= ' ORDER BY TC.COLUMN_ID';
	    
	    $result = $this->queryFetchAllNum($sql);
	    
	    $table_name      = 0;
	    $owner           = 1;
	    $column_name     = 2;
	    $data_type       = 3;
	    $data_default    = 4;
	    $nullable        = 5;
	    $column_id       = 6;
	    $data_length     = 7;
	    $data_scale      = 8;
	    $data_precision  = 9;
	    $constraint_type = 10;
	    $position        = 11;
	    
	    $desc = array();
	    
	    foreach ($result as $key => $row) {
	        list ($primary, $primaryPosition, $identity) = array(false, null, false);
	        if ($row[$constraint_type] == 'P') {
	            $primary = true;
	            $primaryPosition = $row[$position];
	            /**
	             * Oracle does not support auto-increment keys.
	             */
	            $identity = false;
	        }
	        $desc[$this->foldCase($row[$column_name])] = array(
	            'SCHEMA_NAME'      => $this->foldCase($row[$owner]),
	            'TABLE_NAME'       => $this->foldCase($row[$table_name]),
	            'COLUMN_NAME'      => $this->foldCase($row[$column_name]),
	            'COLUMN_POSITION'  => $row[$column_id],
	            'DATA_TYPE'        => $row[$data_type],
	            'DEFAULT'          => $row[$data_default],
	            'NULLABLE'         => (bool) ($row[$nullable] == 'Y'),
	            'LENGTH'           => $row[$data_length],
	            'SCALE'            => $row[$data_scale],
	            'PRECISION'        => $row[$data_precision],
	            'UNSIGNED'         => null,
	            'PRIMARY'          => $primary,
	            'PRIMARY_POSITION' => $primaryPosition,
	            'IDENTITY'         => $identity
	        );
	    }
	    
	    $fks = $this->metadataFk($tabela);
	    
	    return array('FIELDS' => $desc, 'FKS' => $fks);
	}
	
	/**
	 * Adiciona a cláusula LIMIT na consulta.
	 *
	 * @param mixed $sql
	 * @param int $count
	 * @param int $offset
	 * @return string
	 */
	public function limit($sql, $count, $offset = 0) {
	
		$count = intval($count);
	
		if ($count <= 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT count=$count não é válido");
		}
	
		$offset = intval($offset);
	
		if ($offset < 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT offset=$offset não é válido");
		}
	
		$sql .= " LIMIT $count";
	
		if ($offset > 0) {
			$sql .= " OFFSET $offset";
		}
	
		return $sql;
	}

	/**
	 * {@inheritDoc}
	 * @see Db::lastSequenceId()
	 */
	public function lastSequenceId($sequenceName) {
	    
	    return $this->query('SELECT '.$sequenceName.'.CURRVAL FROM dual')->fetchColumn();
	}
	
	/**
	 * {@inheritDoc}
	 * @see Db::nextSequenceId()
	 */
	public function nextSequenceId($sequenceName) {
	    
	    return $this->query('SELECT '.$sequenceName.'.NEXTVAL FROM dual')->fetchColumn();
	}
	
	/**
	 * {@inheritDoc}
	 * @see Db::lastInsertId()
	 */
	public function lastInsertId($tableName = null, $primaryKey = null, $esquema = null, $sequence = null) {
	    
	    $sequenceName = null;
	    if($tableName != null) {
	        if($sequence == null) {
	            $sequenceName = $esquema.'.seq_'.$tableName;
	            if($primaryKey){
	                $sequenceName.= '_'.current($primaryKey);
	            }
	        } else {
	            $sequenceName = $esquema.'.'.$sequence;
	        }
	        return $this->lastSequenceId($sequenceName);
	    }
	    return null;
	}
}