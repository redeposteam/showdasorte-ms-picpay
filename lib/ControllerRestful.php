<?php

/**
 * HTTP 1.0
 */

/**
 * Restful 
 */

require_once 'lib/Restful.php';

/**
 * Classe responsável pelo roteamento de requisições Restful.
 * 
 * @name		ControllerRestful
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class ControllerRestful {

	/**
	 * Constante representando o tipo de erro de Servico
	 * @var int 1
	 */
	const ERROR_SERVICE = 1;

	/**
	 * Constante representando o tipo de erro de Aplicação
	 * @var int 2
	 */
	const ERROR_APPLICATION = 2;
	
	/**
	 * Mensagem representando o "OK" HTTP/1.0
	 * @var int 200
	 */
	const HTTP_OK = 200;
	
	/**
	 * Mensagem representando o "Created" HTTP/1.0
	 * @var int 201
	 */
	const HTTP_CREATED = 201;
	
	/**
	 * Mensagem representando o "No Content" HTTP/1.0
	 * @var int 204
	 */
	const HTTP_NO_CONTENT = 204;
	
	/**
	 * Mensagem representando o "Not Modified" HTTP/1.0
	 * @var int 304
	 */
	const HTTP_NOT_MODIFIED = 304;
	
	/**
	 * Mensagem representando o "Bad Request" HTTP/1.0
	 * @var int 400
	 */
	const HTTP_BAD_REQUEST = 400;

	/**
	 * Mensagem representando o "Unauthorized" HTTP/1.0
	 * @var int 401
	 */
	const HTTP_UNAUTHORIZED = 401;
	
	/**
	 * Mensagem representando o "Not Found" HTTP/1.0
	 * @var int 404
	 */
	const HTTP_NOT_FOUND = 404;
	
	/**
	 * Mensagem representando o "Internal Server Error" HTTP/1.0
	 * @var int 500
	 */
	const HTTP_INTERNAL_SERVER_ERROR = 500;
	
	/**
	 * Objeto encapsulador de dados DAO
	 * @var Model
	 */
	protected $model;
	
	/**
	 * Objeto tratador de requisicões e respostas HTTP
	 * @var Restful
	 */
	protected $restful;

	/**
	 * Objeto comunicador com a base de dados
	 * @var Repository
	 */
	protected $repository;
	
	/**
	 * Metodo para inicialização de variáveis do controller.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init() {

		$this->restful = new Restful();
	}

	/**
	 * Construtor da classe chamando o metodo init()
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		$this->init();
	}

	/**
	 * Método padrão para recepção de requisições GET
	 *
	 * @access public
	 * @return string JSON
	 */
	public function getAction() {

		$pks = array();
		$dados = null;
		$status = null;
		$pk = $this->repository->getPk();
		
		for ($i = 0; $i < count($_GET); $i++) {
			$pks[$pk[$i]] = $_GET['id'][$i];
		}
		
		$data = $this->restful->request()->toArray();
		$this->repository->setProperties($this->model, $pks);
		$this->model = $this->repository->selecionar($this->model);

		if(count($this->restful->request()->toArray()) == 0) {
			$status = self::HTTP_BAD_REQUEST;
		} else {
			if(empty($this->model)) {
				$status = self::HTTP_NOT_FOUND;
			} else {
				$status = self::HTTP_OK;
				$dados = $this->model->toArray();
			}
		}
		$data = array('message' => $this->restful->getHttpStatusMessage($status));
		
		if(!empty($dados)){
			$data['data'] = $dados;
		}
	
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para recepção de requisições POST
	 *
	 * @access public
	 * @return string JSON
	 */
	public function postAction() {
	
		$dados = null;
		$status = null;
		$this->repository->setProperties($this->model, $this->restful->request()->toArray());
		
		try {
			$status = self::HTTP_CREATED;
			$id = $this->repository->inserir($this->model);
			$dados = array('id' => $id);
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		} catch (Exception $e) {
			$status = self::HTTP_INTERNAL_SERVER_ERROR;
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status), 'err' => self::ERROR_SERVICE, 'msg' => $e->getMessage());
			Log::logExcecao($e->getMessage());
		}
		
		if(!empty($dados)){
			$data['response'] = $dados;
		}
	
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para recepção de requisições PUT
	 *
	 * @access public
	 * @return string JSON
	 */
	public function putAction() {

		$status = null;
		$this->repository->setProperties($this->model, $this->restful->request()->toArray());
		
		try {
			$status = self::HTTP_OK;
			$this->repository->atualizar($this->model);
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		} catch (Exception $e) {
			$status = self::HTTP_INTERNAL_SERVER_ERROR;
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status), 'err' => self::ERROR_SERVICE, 'msg' => $e->getMessage());
			Log::logExcecao($e->getMessage());
		}
		
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para recepção de requisições PUT
	 *
	 * @access public
	 * @return string JSON
	 */
	public function deleteAction() {
	
		$status = null;
		$this->repository->setProperties($this->model, $this->restful->request()->toArray());
		
		try {
			$status = self::HTTP_OK;
			$this->repository->excluir($this->model);
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		} catch (Exception $e) {
			$status = self::HTTP_INTERNAL_SERVER_ERROR;
			$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status), 'err' => self::ERROR_SERVICE, 'msg' => $e->getMessage());
			Log::logExcecao($e->getMessage());
		}
		
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para listagem de dados via GET
	 *
	 * @access public
	 * @return string JSON
	 */
	public function getListAction() {
	
		$dados = null;
		$status = null;
		$limits = $this->restful->request()->toArray();
		$dados = $this->repository->todos($this->model, true, $limits['id'][1], $limits['id'][0]);

		if(count($dados) > 0) {
			$status = self::HTTP_OK;
		} else {
			$status = self::HTTP_NOT_FOUND;
		}
		$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		
		if(count($dados) > 0){
			$data['response'] = $dados;
		}
		
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para listagem de dados via POT
	 *
	 * @access public
	 * @return string JSON
	 */
	public function postListAction() {
	
		$dados = null;
		$status = null;
		$parametros = $this->restful->request()->toArray();
		
		$limit = (isset($parametros['limit']) ? $parametros['limit'] : null); 
		$offset = (isset($parametros['offset']) ? $parametros['offset'] : null);
		
		$this->repository->setProperties($this->model, $parametros);
		$dados = $this->repository->todos($this->model, $limit, $offset);

		if(count($dados) > 0) {
			$status = self::HTTP_OK;
		} else {
			$status = self::HTTP_NOT_FOUND;
		}
		$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		
		if(count($dados) > 0){
			$data['response'] = $dados;
		}
		
		die($this->restful->response($status, $data));
	}

	/**
	 * Método padrão para listagem de dados via GET
	 * onde acontece agrupamento de chaves compostas
	 *
	 * @access public
	 * @return string JSON
	 */
	public function groupAction() {

		$keys = $this->restful->request()->toArray();
		
		$properties = array_keys($this->repository->getProperties($this->model));
		$parametros = array();
		
		foreach ($keys as $key => $value) {

			if(in_array($key, $properties)) {
				$parametros[$key] = $value;
			}
			
			if(in_array('id'.$key, $properties)) {
				$parametros['id'.$key] = $value;
			}
			
			if(in_array('id_'.$key, $properties)) {
				$parametros['id_'.$key] = $value;
			}
		}
		
		$this->repository->setProperties($this->model, $parametros);
		$dados = $this->repository->todos($this->model, false);

		if(count($dados) > 0) {
			$status = self::HTTP_OK;
		} else {
			$status = self::HTTP_NOT_FOUND;
		}
		$data = array('status' => $status, 'message' => $this->restful->getHttpStatusMessage($status));
		
		if(count($dados) > 0){
			$data['response'] = $dados;
		}
		
		die($this->restful->response($status, $data));
	}

	/**
	 * Obtem o cabeçalho da resposta HTTP
	 *
	 * @param string $header Se passado, irá procurar por esta chave no cabeçalho e retornar seu valor
	 * @return string|array Trecho ou todo o cabeçalho
	 */
	public function getHeader($header = null) {
	    
	    return $this->restful->getHeader($header);
	}
	
	/**
	 * Trata a requisição HTTP e manda os dados para o atributo $data
	 *
	 * @return Restful
	 */
	public function request() {
	
	    return $this->restful->request();
	}
	
	/**
	 * Formata uma resposta HTTP
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $data Dados a serem enviados
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function response($status, $data, $type = Restful::APPLICATION_JSON) {
	
	    die($this->restful->response($status, $data, $type));
	}
	
	/**
	 * Formata uma resposta HTTP para arquivos
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $data Dados a serem enviados
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function responseImage($status, $file) {
	
	    die($this->restful->responseImage($status, $file));
	}
	
	/**
	 * Formata uma resposta HTTP para arquivos
	 *
	 * @param int $status Código do status da mensagem
	 * @param array $data Dados a serem enviados
	 * @param string $type Tipo de conteúdo a ser enviado
	 * @return $data Em formato de resposta
	 */
	public function responseImage64($status, $file) {
	
	    return $this->restful->responseImage64($status, $file);
	}
}