<?php

/**
 * SQL ANSI.
 */

/**
 * Classe responsável pela geracão de SQL.
 * 
 * @version		1.0
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright© 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Select {

    const DISTINCT       = 'distinct';
    const COLUMNS        = 'columns';
    const FROM           = 'from';
    const WHERE          = 'where';
    const GROUP          = 'group';
    const HAVING         = 'having';
    const ORDER          = 'order';
    const LIMIT_COUNT    = 'limitcount';
    const LIMIT_OFFSET   = 'limitoffset';
    const FOR_UPDATE     = 'forupdate';

    const INNER_JOIN     = 'inner join';
    const LEFT_JOIN      = 'left join';
    const RIGHT_JOIN     = 'right join';
    const FULL_JOIN      = 'full join';
    const CROSS_JOIN     = 'cross join';
    const NATURAL_JOIN   = 'natural join';

    const SQL_WILDCARD   = '*';
    const SQL_SELECT     = 'SELECT';
    const SQL_FROM       = 'FROM';
    const SQL_WHERE      = 'WHERE';
    const SQL_DISTINCT   = 'DISTINCT';
    const SQL_GROUP_BY   = 'GROUP BY';
    const SQL_ORDER_BY   = 'ORDER BY';
    const SQL_HAVING     = 'HAVING';
    const SQL_FOR_UPDATE = 'FOR UPDATE';
    const SQL_AND        = 'AND';
    const SQL_AS         = 'AS';
    const SQL_OR         = 'OR';
    const SQL_ON         = 'ON';
    const SQL_ASC        = 'ASC';
    const SQL_DESC       = 'DESC';

    const INT_TYPE    	 = 0;
    const BIGINT_TYPE 	 = 1;
    const FLOAT_TYPE  	 = 2;
    
    /**
     * Os valores iniciais para o array $ _parts.
     * É importante que a parte 'FOR_UPDATE' seja passado por último para garantir
	 * o máximo de compatibilidade com outros adaptadores de banco de dados.
	 * 
     * @var array
     */
    protected static $_partsInit = array(
        self::DISTINCT     => false,
        self::COLUMNS      => array(),
        self::FROM         => array(),
        self::WHERE        => array(),
        self::GROUP        => array(),
        self::HAVING       => array(),
        self::ORDER        => array(),
        self::LIMIT_COUNT  => null,
        self::LIMIT_OFFSET => null,
        self::FOR_UPDATE   => false
    );

    /**
     * Os valores iniciais para o array $ _parts.
     *
     * @var array
     */
    protected static $_joinTypes = array(
        self::INNER_JOIN,
        self::LEFT_JOIN,
        self::RIGHT_JOIN,
        self::FULL_JOIN,
        self::CROSS_JOIN,
        self::NATURAL_JOIN,
    );

    /**
     *Os componentes de uma instrução SELECT.
     *Inicializado com o $_partsInit no construtor.
     *
     * @var array
     */
    protected $_parts = array();

    /**
     * Guarda quais colunas estão a ser selecionadas por cada tabela e join.
     *
     * @var array
     */
    protected $_tableCols = array();

    /**
     * Guarda uma instância válida da classe Db.
     *
     * @var Db
     */
    protected $db;
    
    /**
     * Constructor de classe
     *
     */
    public function __construct() {
    	
    	if(empty($this->db)) {
    		$this->db = App::Db();
    	}
        $this->_parts = self::$_partsInit;
    }

    /**
     * Torna a consulta SELECT DISTINCT.
     *
     * @param bool $flag setar ou não a consulta para distinct (default true).
     * @return Select This Select object.
     */
    public function distinct($flag = true) {
    	
        $this->_parts[self::DISTINCT] = (bool) $flag;
        return $this;
    }

    /**
     * Adiciona a tabela e colunas opcionais para a consulta.
     *
     * O primeiro parâmetro $name pode ser uma string simples, no caso de o que
	 * o nome da correlação é gerada automaticamente. Se você deseja especificar
	 * o nome de correlação, o primeiro parâmetro deve ser um array associativo
	 * em que a chave é o nome da tabela física, e o valor é o nome de correlação. 
	 * Por exemplo, array ('table' => 'alias'). O nome de correlação é anexado a 
	 * todas as colunas buscados para esta tabela.
     *
     * O segundo parametro pode ser uma string simples or Expression object,
     * ou então um array de strings ou Expression objects.
     *
     * O primeiro parametro pode ser nulo ou uma string vazia, neste caso
     * o nome da correlação não é gerado ou adicionado no inicio antes dos 
     * nomes das colunas no segundo parametro.
     *
     * @param  array|string|Expression $name Nome da tabela ou um array associativo informando o nome da tabela e nome de sua correlação.
     * @param  array|string|Expression $cols Colunas que serão selecionadas na consulta a tabela.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function from($name, $cols = '*', $schema = null) {
    	
        return $this->joinInner($name, null, $cols, $schema);
    }

    /**
     * Adiciona um JOIN com tabela e colunas para a consulta. 
     *
     * Os parâmetros $name e $cols seguem a mesma lógica descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  string $cond JOIN ON desta condição.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function join($name, $cond, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->joinInner($name, $cond, $cols, $schema);
    }

    /**
     * Adicionar um INNER JOIN e suas colunas à consulta
	 * em ambas as tabelas, onde são combinados de acordo com a expressão
	 * no argumento $cond. O conjunto de resultados é composta
	 * de todos os casos em que as linhas da tabela à esquerda
	 * batem com as linhas da tabela à direita
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  string $cond JOIN ON desta condição.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinInner($name, $cond, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::INNER_JOIN, $name, $cond, $cols, $schema);
    }

    /**
     * Adiciona um LEFT OUTER JOIN e suas colunas à consulta
	 * Todas as linhas da tabela do operando esquerdo estão incluídos,
	 * e as linhas correspondentes da tabela do operando direito incluídos,
	 * e as colunas da tabela do operando direito são preenchidos com nulos 
	 * se não existe nenhuma linha de correspondência a tabela à esquerda.
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  string $cond JOIN ON desta condição.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinLeft($name, $cond, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::LEFT_JOIN, $name, $cond, $cols, $schema);
    }

    /**
     * Adiciona um RIGHT OUTER JOIN e suas colums à consulta.
	 * Right outer join é o complemento de Left Outer Join.
	 * Todas as linhas da tabela operando direito são incluídos,
	 * linhas correspondentes da tabela operando esquerdo incluídos,
	 * e as colunas da tabela operando à esquerda são preenchidos
	 * com NULLs se nenhuma linha existe correspondência tabela à direita.
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  string $cond JOIN ON desta condição.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinRight($name, $cond, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::RIGHT_JOIN, $name, $cond, $cols, $schema);
    }

    /**
     * Adiciona um FULL OUTER JOIN e suas colunas à consulta.
	 * O Full Outer Join é a combinação de uma Left Outer Join
	 * e um Right Outer Join. Todas as linhas de ambas as tabelas são
	 * incluídas, emparelhados uns com os outros na mesma linha do
	 * conjunto de resultados, desde que satisfaçam a condição de junção,
	 * e de outra forma emparelhado com NULLs no lugar de colunas da outra tabela.
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  string $cond JOIN ON desta condição.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinFull($name, $cond, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::FULL_JOIN, $name, $cond, $cols, $schema);
    }

    /**
     * Adiciona um CROSS JOIN e suas colunas para a consulta.
     * Um Cross Join é um produto cartesiano, não há qualquer condição de junção
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     * 
     * @param  array|string|Expression $name Nome da tabela.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinCross($name, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::CROSS_JOIN, $name, null, $cols, $schema);
    }

    /**
     * Add a NATURAL JOIN table and colums to the query.
     * A natural join assumes an equi-join across any column(s)
     * that appear with the same name in both tables.
     * Only natural inner joins are supported by this API,
     * even though SQL permits natural outer joins as well.
     * 
     * Adiciona um NATURAL JOIN e suas colunas à consulta.
     * A Natural Join assume um Equi-Join Across através de qualquer coluna(s)
     * que aparecem com o mesmo nome em ambas as tabelas.
     * Apenas Natural Inner Joins são suportados por esta API,
     * embora SQL permite Natural Outer Joins de forma eficiente.
     *
     * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
     *
     * @param  array|string|Expression $name Nome da tabela.
     * @param  array|string $cols Colunas que serão selecionadas no join.
     * @param  string $schema Nome do esquema específico.
     * @return Select This Select object.
     */
    public function joinNatural($name, $cols = self::SQL_WILDCARD, $schema = null) {
    	
        return $this->_join(self::NATURAL_JOIN, $name, null, $cols, $schema);
    }

    /**
     * Adiciona uma condição WHERE para a consulta unidas por AND.
     *
     * Se um valor é passado no segundo parâmetro, ele será colocado entre 
     * aspas simples e substituído na condição sempre que um ponto de interrogação
     * aparece. Os valores do array são colocado entre aspas simples e separados por vírgulas.
     *
     * <code>
     * // simplest but non-secure
     * $select->where("id = $id");
     *
     * // secure (ID is quoted but matched anyway)
     * $select->where('id = ?', $id);
     *
     * // alternatively, with named binding
     * $select->where('id = :id');
     * </code>
     *
     * Note que é mais correto usar ligações nomeadas nas suas
     * consultas do que strings. Quando você usa o nome nas ligações,
     * não se esqueça de passar os valores quando fizer uma consulta: 
     *
     * <code>
     * $select->where(array('id' => 5));
     * </code>
     *
     * @param string   $cond  A condição WHERE.
     * @param string   $value OPTIONAL Valor para a condição.
     * @param constant $type  OPTIONAL O tipo do dado
     * @return Select This Select object.
     */
    public function where($cond, $value = null, $type = null) {
    	
        $this->_parts[self::WHERE][] = $this->_where($cond, $value, $type, true);

        return $this;
    }

    /**
     * Adiciona uma condição WHERE para consulta unidas por OR.
     *
     * O restante é identico ao where().
     *
     * @param string   $cond  A condição WHERE.
     * @param string   $value OPTIONAL Valor para a condição.
     * @param constant $type  OPTIONAL O tipo do dado
     * @return Select This Select object.
     *
     * @see where()
     */
    public function orWhere($cond, $value = null, $type = null) {
    	
        $this->_parts[self::WHERE][] = $this->_where($cond, $value, $type, false);

        return $this;
    }

    /**
     * Adiciona um agrupamento para a consulta.
     *
     * @param  array|string $spec A(s) coluna(s) para o Group By.
     * @return Select This Select object.
     */
    public function group($spec) {
    	
        if (!is_array($spec)) {
            $spec = array($spec);
        }

        foreach ($spec as $val) {
            if (preg_match('/\(.*\)/', (string) $val)) {
                $val = new Expression($val);
            }
            $this->_parts[self::GROUP][] = $val;
        }

        return $this;
    }

    /**
     * Adiciona uma condição HAVING à consulta unidas por AND.
     *
     * Se um valor é passado no segundo parâmetro, ele será colocado entre
     * aspas simples e substituído na condição sempre que um ponto de interrogação
     * aparece. Veja {@link where()} para um exemplo 
     *
     * @param string $cond A condição HAVING.
     * @return Select This Select object.
     */
    public function having($cond) {
    	
        if (func_num_args() > 1) {
            $val = func_get_arg(1);
            $cond = $this->quoteInto($cond, $val);
        }

        if ($this->_parts[self::HAVING]) {
            $this->_parts[self::HAVING][] = self::SQL_AND . " ($cond)";
        } else {
            $this->_parts[self::HAVING][] = "($cond)";
        }

        return $this;
    }

    /**
     * Adiciona uma condição HAVING à consulta unidas por OR.
     *
     * O restante é identico ao orHaving().
     *
     * @param string $cond A condição HAVING.
     * @return Select This Select object.
     *
     * @see having()
     */
    public function orHaving($cond) {
    	
        if (func_num_args() > 1) {
            $val = func_get_arg(1);
            $cond = $this->quoteInto($cond, $val);
        }

        if ($this->_parts[self::HAVING]) {
            $this->_parts[self::HAVING][] = self::SQL_OR . " ($cond)";
        } else {
            $this->_parts[self::HAVING][] = "($cond)";
        }

        return $this;
    }

    /**
     * Adiciona uma ordenação para a consulta.
     *
     * @param mixed $spec A(s) coluna(s) e a direção de ordenação.
     * @return Select This Select object.
     */
    public function order($spec) {
    	
        if (!is_array($spec)) {
            $spec = array($spec);
        }

        foreach ($spec as $val) {
            if ($val instanceof Expression) {
                $expr = $val->__toString();
                if (empty($expr)) {
                    continue;
                }
                $this->_parts[self::ORDER][] = $val;
            } else {
                if (empty($val)) {
                    continue;
                }
                $direction = self::SQL_ASC;
                if (preg_match('/(.*\W)(' . self::SQL_ASC . '|' . self::SQL_DESC . ')\b/si', $val, $matches)) {
                    $val = trim($matches[1]);
                    $direction = $matches[2];
                }
                if (preg_match('/\(.*\)/', $val)) {
                    $val = new Expression($val);
                }
                $this->_parts[self::ORDER][] = array($val, $direction);
            }
        }

        return $this;
    }

    /**
     * Define um limite de contagem e deslocamento para a consulta.
     *
     * @param int $count OPTIONAL Número de linhas que será retornada.
     * @param int $offset OPTIONAL Comece retornando após esta quantidade de linhas.
     * @return Select This Select object.
     */
    public function limit($count = null, $offset = null) {
    	
        $this->_parts[self::LIMIT_COUNT]  = (int) $count;
        $this->_parts[self::LIMIT_OFFSET] = (int) $offset;
        return $this;
    }

    /**
     * Define o limite e contagem por número de página.
     *
     * @param int $page Limit results to this page number.
     * @param int $rowCount Utilize este número de linhas por página.
     * @return Select This Select object.
     */
    public function limitPage($page, $rowCount) {
    	
        $page     = ($page > 0)     ? $page     : 1;
        $rowCount = ($rowCount > 0) ? $rowCount : 1;
        $this->_parts[self::LIMIT_COUNT]  = (int) $rowCount;
        $this->_parts[self::LIMIT_OFFSET] = (int) $rowCount * ($page - 1);
        return $this;
    }

    /**
     * Faz a consulta SELECT FOR UPDATE.
     *
     * @param bool $flag setar ou não se o SELECT é um FOR UPDATE (default true).
     * @return Select This Select object.
     */
    public function forUpdate($flag = true) {
    	
        $this->_parts[self::FOR_UPDATE] = (bool) $flag;
        return $this;
    }

    /**
     * Captura part da informação da estrutura da consulta atual. 
     *
     * @param string $part
     * @return mixed
     * @throws Exception
     */
    public function getPart($part) {
    	
        $part = strtolower($part);
        if (!array_key_exists($part, $this->_parts)) {
        	throw new Exception('Class-'.__CLASS__.": Parte de consulta inválida '$part'");
        }
        return $this->_parts[$part];
    }

    /**
     * Limpa as partes do objeto Select, ou uma parte individual.
     *
     * @param string $part OPTIONAL
     * @return Select This Select object.
     */
    public function reset($part = null) {
    	
    	if ($part == null) {
    		$this->_parts = self::$_partsInit;
    	} else if (array_key_exists($part, self::$_partsInit)) {
    		$this->_parts[$part] = self::$_partsInit[$part];
    	}
    	return $this;
    }    
    
    /**
     * Manuseando JOIN... USING... syntax
     *
     * Esta funcionalidade é idêntico aos métodos JOIN existentes, no entanto
     * a condição de join pode ser passada com apenas um nome de coluna. Este método
     * em seguida, completa a condição ON usando o mesmo campo para a tabela
     * FROM e a tabela de JOIN.
     *
     * <code>
     * $select = $db->select()->from('table1')->joinUsing('table2', 'column1');
     *
     * // SELECT * FROM table1 JOIN table2 ON table1.column1 = table2.column2
     * </code>
     *
     * These joins are called by the developer simply by adding 'Using' to the
     * Essas articulações são chamadas pelo desenvolvedor simplesmente adicionando 'Using' ao
     * nome do método. E.g.
     * * joinUsing
     * * joinInnerUsing
     * * joinFullUsing
     * * joinRightUsing
     * * joinLeftUsing
     *
     * @param string $type
     * @param string $name
     * @param string $cond
     * @param string $cols
     * @param string $schema
     * @return Select This Select object.
     */
    protected function _joinUsing($type, $name, $cond, $cols = '*', $schema = null) {
    	
    	if (empty($this->_parts[self::FROM])) {
    		throw new Exception('Class-'.__CLASS__.": Você só pode usar o joinUsing depois de especificar o FROM (tabela)");
    	}
    
    	$join  = $this->quoteIdentifier(key($this->_parts[self::FROM]));
    	$from  = $this->quoteIdentifier($this->_uniqueCorrelation($name));
    
    	$cond1 = $from . '.' . $cond;
    	$cond2 = $join . '.' . $cond;
    	$cond  = $cond1 . ' = ' . $cond2;
    
    	return $this->_join($type, $name, $cond, $cols, $schema);
    }
    
    /**
     * Popula a variável {@link $_parts} das chaves 'join'
     *
     * Faz o trabalho sujo de preencher a chave do join.
     *
	 * Os parâmetros $name e $cols seguem a mesma lógica
     * descrita no método from().
	 * 
     *
     * @param  null|string $type Tipo de junção; inner, left, e null são atualmente suportadas
     * @param  array|string|Expression $name Nome da tabela
     * @param  string $cond Condição Join da consulta
     * @param  array|string $cols As colunas selecionadas no Join
     * @param  string $schema Nome do esquema específico.
     * @return Zend_Db_Select
     * @throws Exception
     */
    protected function _join($type, $name, $cond, $cols, $schema = null) {
    	
        if (!in_array($type, self::$_joinTypes)) {
            throw new Exception('Class-'.__CLASS__.": Invalid join type '$type'");
        }
        if (empty($name)) {
            $correlationName = $tableName = '';
        } else if (is_array($name)) {
            foreach ($name as $_correlationName => $_tableName) {
                if (is_string($_correlationName)) {
                    $tableName = $_tableName;
                    $correlationName = $_correlationName;
                } else {
                    $tableName = $name;
                    $correlationName = $this->_uniqueCorrelation($tableName);
                }
                break;
            }
        } else if ($name instanceof Expression|| $name instanceof Select) {
            $tableName = $name;
            $correlationName = $this->_uniqueCorrelation('t');
        } else if (preg_match('/^(.+)\s+AS\s+(.+)$/i', $name, $m)) {
            $tableName = $m[1];
            $correlationName = $m[2];
        } else {
            $tableName = $name;
            $correlationName = $this->_uniqueCorrelation($tableName);
        }
        if (!is_object($tableName) && false !== strpos($tableName, '.')) {
            list($schema, $tableName) = explode('.', $tableName);
        }
        if (!empty($correlationName)) {
            if (array_key_exists($correlationName, $this->_parts[self::FROM])) {
                throw new Exception('Class-'.__CLASS__.": Você não pode definir um nome da relação '$correlationName' mais de uma vez");
            }

            $this->_parts[self::FROM][$correlationName] = array(
                'joinType'      => $type,
                'schema'        => $schema,
                'tableName'     => $tableName,
                'joinCondition' => $cond
            );
        }
        $this->_tableCols($correlationName, $cols);
        
        return $this;
    }

    /**
     * Gera um nome de correlação único
     *
     * @param string|array $name Um identificador válido.
     * @return string Um nome de correlação único.
     */
    private function _uniqueCorrelation($name) {
    	
        if (is_array($name)) {
            $c = end($name);
        } else {
            $dot = strrpos($name,'.');
            $c = ($dot === false) ? $name : substr($name, $dot+1);
        }
        for ($i = 2; array_key_exists($c, $this->_parts[self::FROM]); ++$i) {
            $c = $name . '_' . (string) $i;
        }
        return $c;
    }

    /**
     * Adiciona um mapeamento interno tabela-para-coluna no array.
     *
     * @param  string $correlationName A TABLE/JOIN para associar-se as colunas do FROM.
     * @param  array|string $cols A lista de colunas, de preferência um array, mas possivelmente como uma string que contém apenas uma coluna.
     * @return void
     */
    protected function _tableCols($correlationName, $cols) {
        if (!is_array($cols)) {
            $cols = array($cols);
        }
        if ($correlationName == null) {
            $correlationName = '';
        }

        foreach ($cols as $alias => $col) {
            $currentCorrelationName = $correlationName;
            if (is_string($col)) {
                if (preg_match('/^(.+)\s+' . self::SQL_AS . '\s+(.+)$/i', $col, $m)) {
                    $col = $m[1];
                    $alias = $m[2];
                }
                if (preg_match('/\(.*\)/', $col)) {
                    $col = new Expression($col);
                } elseif (preg_match('/(.+)\.(.+)/', $col, $m)) {
                    $currentCorrelationName = $m[1];
                    $col = $m[2];
                }
            }
            $this->_parts[self::COLUMNS][] = array($currentCorrelationName, $col, is_string($alias) ? $alias : null);
        }
        
        return $this;
    }

    /**
     * Função interna para criar cláusula WHERE
     *
     * @param string   $condition
     * @param string   $value  optional
     * @param string   $type   optional
     * @param boolean  $bool  true = AND, false = OR
     * @return string  clause
     */
    protected function _where($condition, $value = null, $type = null, $bool = true) {
    	
        if ($value !== null) {
            $condition = $this->quoteInto($condition, $value, $type);
        }

        $cond = "";
        if ($this->_parts[self::WHERE]) {
            if ($bool === true) {
                $cond = self::SQL_AND . ' ';
            } else {
                $cond = self::SQL_OR . ' ';
            }
        }
        
        return $cond . "($condition)";
    }

    /**
     * Tabela vazia
     * 
     * @return array
     */
    protected function _getDummyTable() {
    	
        return array();
    }

    /**
     * Retorna o nome do esquema entre aspas
     *
     * @param string   $schema  Nome do esquema OPCIONAL
     * @return string|null
     */
    protected function _getQuotedSchema($schema = null) {
    	
        if ($schema === null) {
            return null;
        }
        return $this->quoteIdentifier($schema, true) . '.';
    }

    /**
     * Retorna o nome da tabela entre aspas
     *
     * @param string   $tableName        Nome da tabela
     * @param string   $correlationName  Nome da correlação OPTIONAL
     * @return string
     */
    protected function _getQuotedTable($tableName, $correlationName = null) {
    	
        return $this->quoteTableAs($tableName, $correlationName, true);
    }

    /**
     * Gera a cláusula DISTINCT
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderDistinct($sql) {
    	
        if ($this->_parts[self::DISTINCT]) {
            $sql .= ' ' . self::SQL_DISTINCT;
        }

        return $sql;
    }

    /**
     * Gera as colunas
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderColumns($sql) {
    	
        if (!count($this->_parts[self::COLUMNS])) {
            return null;
        }

        $columns = array();
        foreach ($this->_parts[self::COLUMNS] as $columnEntry) {
            list($correlationName, $column, $alias) = $columnEntry;
            if ($column instanceof Expression) {
                $columns[] = $this->quoteColumnAs($column, $alias, true);
            } else {
                if ($column == self::SQL_WILDCARD) {
                    $column = new Expression(self::SQL_WILDCARD);
                    $alias = null;
                }
                if (empty($correlationName)) {
                    $columns[] = $this->quoteColumnAs($column, $alias, true);
                } else {
                    $columns[] = $this->quoteColumnAs(array($correlationName, $column), $alias, true);
                }
            }
        }

        return $sql .= ' ' . implode(', ', $columns);
    }

    /**
     * Gera a cláusula FROM
     *
     * @param string   $sql SQL consulta
     * @return string
     */
    protected function _renderFrom($sql) {
    	
        if (empty($this->_parts[self::FROM])) {
            $this->_parts[self::FROM] = $this->_getDummyTable();
        }

        $from = array();

        foreach ($this->_parts[self::FROM] as $correlationName => $table) {
            $tmp = '';

            if (! empty($from)) {
                $tmp .= ' ' . strtoupper($table['joinType']) . ' ';
            }

            $tmp .= $this->_getQuotedSchema($table['schema']);
            $tmp .= $this->_getQuotedTable($table['tableName'], $correlationName);

            if (!empty($from) && ! empty($table['joinCondition'])) {
                $tmp .= ' ' . self::SQL_ON . ' ' . $table['joinCondition'];
            }

            $from[] = $tmp;
        }

        if (!empty($from)) {
            $sql .= ' ' . self::SQL_FROM . ' ' . implode("\n", $from);
        }

        return $sql;
    }

    /**
     * Gera a cláusula WHERE
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderWhere($sql) {
    	
        if ($this->_parts[self::FROM] && $this->_parts[self::WHERE]) {
            $sql .= ' ' . self::SQL_WHERE . ' ' .  implode(' ', $this->_parts[self::WHERE]);
        }

        return $sql;
    }

    /**
     * Gera a cláusula GROUP
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderGroup($sql) {
    	
        if ($this->_parts[self::FROM] && $this->_parts[self::GROUP]) {
            $group = array();
            foreach ($this->_parts[self::GROUP] as $term) {
                $group[] = $this->quoteIdentifier($term, true);
            }
            $sql .= ' ' . self::SQL_GROUP_BY . ' ' . implode(",\n\t", $group);
        }

        return $sql;
    }

    /**
     * Gera a cláusula HAVING
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderHaving($sql) {
    	
        if ($this->_parts[self::FROM] && $this->_parts[self::HAVING]) {
            $sql .= ' ' . self::SQL_HAVING . ' ' . implode(' ', $this->_parts[self::HAVING]);
        }

        return $sql;
    }

    /**
     * Gera a cláusula ORDER
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderOrder($sql) {
    	
        if ($this->_parts[self::ORDER]) {
            $order = array();
            foreach ($this->_parts[self::ORDER] as $term) {
                if (is_array($term)) {
                    $order[] = $this->quoteIdentifier($term[0], true) . ' ' . $term[1];
                } else {
                    $order[] = $this->quoteIdentifier($term, true);
                }
            }
            $sql .= ' ' . self::SQL_ORDER_BY . ' ' . implode(', ', $order);
        }

        return $sql;
    }

    /**
     * Gera a cláusula LIMIT OFFSET
     *
     * @param string $sql SQL consulta
     * @return string
     */
    protected function _renderLimitoffset($sql) {
    	
        $count = 0;
        $offset = 0;

        if (!empty($this->_parts[self::LIMIT_OFFSET])) {
            $offset = (int) $this->_parts[self::LIMIT_OFFSET];
            $count = intval(9223372036854775807);
        }

        if (!empty($this->_parts[self::LIMIT_COUNT])) {
            $count = (int) $this->_parts[self::LIMIT_COUNT];
        }

        if ($count > 0) {
            $sql = trim($this->db->limit($sql, $count, $offset));
        }

        return $sql;
    }

    /**
     * Gera a cláusula FOR UPDATE
     *
     * @param string   $sql SQL consulta
     * @return string
     */
    protected function _renderForupdate($sql) {
    	
        if ($this->_parts[self::FOR_UPDATE]) {
            $sql .= ' ' . self::SQL_FOR_UPDATE;
        }

        return $sql;
    }

    /**
     * Vira função mágica chamando dentro de uma função não-mágica, mas, exigeo uso de joinUsing na sintaxe
     *
     * @param string $method
     * @param array $args Modificador de consulta Select (OPCIONAL)
     * @return Select
     * @throws Exception Se um método inválido é chamado.
     */
    public function __call($method, array $args) {
    	
        $matches = array();

        if (preg_match('/^join([a-zA-Z]*?)Using$/', $method, $matches)) {
            $type = strtolower($matches[1]);
            if ($type) {
                $type .= ' join';
                if (!in_array($type, self::$_joinTypes)) {
                    throw new Exception('Class-'.__CLASS__.": Método não reconhecido '$method()'");
                }
                if (in_array($type, array(self::CROSS_JOIN, self::NATURAL_JOIN))) {
                    throw new Exception('Class-'.__CLASS__.": Não é possível executar uma joinUsing com o método '$method()'");
                }
            } else {
                $type = self::INNER_JOIN;
            }
            array_unshift($args, $type);
            return call_user_func_array(array($this, '_joinUsing'), $args);
        }
        throw new Exception('Class-'.__CLASS__.": Método não reconhecido '$method()'");
    }

    /**
     * Coloca uma string entre aspas
     *
     * @param string $value     String natural
     * @return string           String entre aspas
     */
    protected function _quote($value) {
    	
    	if (is_int($value) || is_float($value)) {
    		return $value;
    	}
    	return "'" . addcslashes($value, "\000\n\r\\'\"\032") . "'";
    }
    
    /**
     * Segurança no tratamento de aspas na cláusula SQL
     *
     * Se um array é passado como o valor, os valores do array são cotados
	 * e depois voltam como uma string separada por vírgulas
     *
     * @param mixed $value O valor para er colocado entre aspas.
     * @param mixed $type Nome do tipo do SQL OPICIONAL, ou constante, or nulo.
     * @return mixed SQL em modo seguro (ou string com valores separados).
     */
    public function quote($value, $type = null) {
    	
    	if ($value instanceof Select) {
    		return '(' . $value->__toString() . ')';
    	}
    
    	if ($value instanceof Expression) {
    		return $value->__toString();
    	}
    
    	if (is_array($value)) {
    		foreach ($value as &$val) {
    			$val = $this->quote($val, $type);
    		}
    		return implode(', ', $value);
    	}
    
    	if ($type !== null && array_key_exists($type = strtoupper($type), $this->_numericDataTypes)) {
    		switch ($this->_numericDataTypes[$type]) {
    			case self::INT_TYPE: // 32-bit integer
    				return (string) intval($value);
    				break;
    			case self::BIGINT_TYPE: // 64-bit integer
    				// ANSI SQL-style hex literals (e.g. x'[\dA-F]+')
    				// are not supported here, because these are string
    				// literals, not numeric literals.
    				if (preg_match('/^(
                          [+-]?                  # optional sign
                          (?:
                            0[Xx][\da-fA-F]+     # ODBC-style hexadecimal
                            |\d+                 # decimal or octal, or MySQL ZEROFILL decimal
                            (?:[eE][+-]?\d+)?    # optional exponent on decimals or octals
                          )
                        )/x',
    						(string) $value, $matches)) {
    					return $matches[1];
    				}
    				break;
    			case self::FLOAT_TYPE: // float or decimal
    				return (string) floatval($value);
    				break;
    		}
    		return '0';
    	}
    
    	return $this->_quote($value);
    }
    
    /**
     * Coloca entre aspas um valor e coloca em um pedaço do texto em um espaço reservado.
     *
     * The placeholder is a question-mark; all placeholders will be replaced
     * with the quoted value.
     * 
     * O espaço reservado é um ponto de interrogação; todos os marcadores serão substituídos
	 * com o valor entre aspas   
     * 
     * Por examplo:
     *
     * <code>
     * $text = "WHERE date < ?";
     * $date = "2005-01-02";
     * $safe = $sql->quoteInto($text, $date);
     * // $safe = "WHERE date < '2005-01-02'"
     * </code>
     *
     * @param string  $text  Texto com o marcador
     * @param mixed   $value Valor a ser substituido
     * @param string  $type  OPCIONAL tipo de SQL
     * @param integer $count OPCIONAL conta o numero de marcadores para troca
     * @return string SQL Seguro com os valores colocados dentro da string original.
     */
    public function quoteInto($text, $value, $type = null, $count = null) {
    	
    	if ($count === null) {
    		return str_replace('?', $this->quote($value, $type), $text);
    	} else {
    		while ($count > 0) {
    			if (strpos($text, '?') != false) {
    				$text = substr_replace($text, $this->quote($value), strpos($text, '?'), 1);
    			}
    			--$count;
    		}
    		return $text;
    	}
    }
    
    /**
	 * Cita um identificador.
     *
     * Aceita uma cadeia que representa um identificador qualificado. Por exemplo
     * 
     * <code>
     * $adapter->quoteIdentifier('myschema.mytable')
     * </code>
     * Retorno: "myschema"."mytable"
     *
     * Ou, uma matriz de um ou mais identificadores que podem formar um identificador qualificado:
     * <code>
     * $adapter->quoteIdentifier(array('myschema','my.table'))
     * </code>
     * Returns: "myschema"."my.table"
     *
     * @param string|array|Expression $ident O identificador ou expressão.
     * @param boolean $auto Se verdadeiro, acatam a opção IDENTIFICADORES auto citações de configuração.
     * @return string O identificador citado.
     */
    public function quoteIdentifier($ident, $auto=false) {
    	
    	return $this->_quoteIdentifierAs($ident, null, $auto);
    }
    
    /**
     * Quote a column identifier and alias.
     *
     * @param string|array|Expression $ident O identificador ou expressão.
     * @param string $alias Um apelido para a coluna.
     * @param boolean $auto Se verdadeiro, acatam a opção AUTO_QUOTE_IDENTIFIERS de configuração.
     * @return string O identificador citado.
     */
    public function quoteColumnAs($ident, $alias, $auto=false) {
    	
    	return $this->_quoteIdentifierAs($ident, $alias, $auto);
    }
    
    /**
     * Cita a tabela e apelido.
     *
     * @param string|array|Expression $ident O identificador ou expressão.
     * @param string $alias Apelido para a tabela.
     * @param boolean $auto Se verdadeiro, acatam a opção IDENTIFICADORES auto citações de configuração.
     * @return string O identificador citado.
     */
    public function quoteTableAs($ident, $alias = null, $auto=false) {
    	
    	return $this->_quoteIdentifierAs($ident, $alias, $auto);
    }
    
    /**
     * Cita o identificador e e opcional apelido.
     *
     * @param string|array|Expression $ident O identificador ou expressão.
     * @param string $alias Apelido opcional.
     * @param boolean $auto Se verdadeiro, acatam a opção AUTO_QUOTE_IDENTIFIERS de configuração.
     * @param string $as O texto a ser adicionado entre o identificador / expressão e o alias.
     * @return string O identificador citado.
     */
    protected function _quoteIdentifierAs($ident, $alias = null, $auto = false, $as = ' AS ') {
    	
    	if ($ident instanceof Expression) {
    		$quoted = $ident->__toString();
    	} elseif ($ident instanceof Select) {
    		$quoted = '(' . $ident->__toString() . ')';
    	} else {
    		if (is_string($ident)) {
    			$ident = explode('.', $ident);
    		}
    		if (is_array($ident)) {
    			$segments = array();
    			foreach ($ident as $segment) {
    				if ($segment instanceof Expression) {
    					$segments[] = $segment->__toString();
    				} else {
    					$segments[] = $this->_quoteIdentifier($segment, $auto);
    				}
    			}
    			if ($alias !== null && end($ident) == $alias) {
    				$alias = null;
    			}
    			$quoted = implode('.', $segments);
    		} else {
    			$quoted = $this->_quoteIdentifier($ident, $auto);
    		}
    	}
    	if ($alias !== null) {
    		$quoted .= $as . $this->_quoteIdentifier($alias, $auto);
    	}
    	return $quoted;
    }
    
    /**
     * @var bool Marcado como true se for necessário colocar aspas na string
     */
    protected $_autoQuoteIdentifiers;
    
    /**
     * Cita um identificador.
     *
     * @param string $value O identificador ou expressão.
     * @param boolean $auto Se verdadeiro, acatam a opção AUTO_QUOTE_IDENTIFIERS de configuração.
     * @return string Identificador citado e apelido.
     */
    protected function _quoteIdentifier($value, $auto=false) {
    	
    	if ($auto === false || $this->_autoQuoteIdentifiers === true) {
    		$q = $this->getQuoteIdentifierSymbol();
    		return ($q . str_replace("$q", "$q$q", $value) . $q);
    	}
    	return $value;
    }
    
    /**
     * Retorna o símbolo do adaptador usado para identificadores delimitados.
     *
     * @return string
     */
    public function getQuoteIdentifierSymbol() {
    	
    	return '"';
    }
    
    /**
     * Converte este objeto para uma string de consulta SELECT SQL.
     *
     * @return string Este objeto como uma string SELECT.
     */
    public function __toString() {
    	
        $sql = self::SQL_SELECT;
        foreach (array_keys(self::$_partsInit) as $part) {
            $method = '_render' . ucfirst($part);
            if (method_exists($this, $method)) {
                $sql = $this->$method($sql);
            }
        }
        return $sql;
    }
    
    /**
     * Executa o select do objeto atual e retorna o resultado
     *
     * @param integer $fetchMode OPTIONAL
     * @return PDOStatement
     */
    public function query($fetchMode = null) {
    	
    	return $this->db->query($this->__toString());
    }
}
