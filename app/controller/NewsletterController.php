<?php

require_once 'app/controller/AppController.php';
require_once 'lib/Validacao.php';

class NewsletterController extends AppController
{

    public function __construct($action)
    {

        parent::__construct();
        $this->action = $action;
        $this->pdftemp = APP_PATH . DS . '..' . DS . 'pdf' . DS;
        $this->templates = APP_PATH . DS . 'templates' . DS;
        $this->actionsNoValidateJwt = $this->actionsNoValidateCsrf = array('getvisualizarresultado', 'getvisualizarcertificado', 'getcancelar', 'getconfirmarcadastro', 'postcadastro');
    }

    protected function addVar($key, $val)
    {
        $this->Smarty->assign($key, $val);
    }

    protected function parse($template)
    {
        return $this->Smarty->fetch($template);
    }

    protected function send($idEmpresa, $email, $nome, $titulo, $body)
    {
        return $this->sendMail($idEmpresa, $email, $nome, $titulo, $body);
    }

    protected function prepareConstants($base)
    {

        $this->addVar('host', $this->getHost());
        $this->addVar('site', $this->_SITES[$base]);
        $this->addVar('logo', $this->_EMAIL[$base]['logo']);
        $this->addVar('entidade', $this->_EMAIL[$base]['entidade']);
        $this->addVar('auditoria', $this->_EMAIL[$base]['auditoria']);
        $this->addVar('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $this->addVar('email_base', $this->_CONTATO[$base]['email_base']);
        $this->addVar('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $this->addVar('rodape', $this->_CONTATO[$base]['texto_suporte']);
        $this->addVar('redessociais', $this->_REDES_SOCIAIS[$base]);
        $this->addVar('extra', '');
    }

    public function getVisualizarResultadoAction()
    {

        $request = $this->request()->toArray();

        $id = $this->decrypt(Util::base64UrlDecode(current($request['id'])));
        $dados = explode('_', $id);

        if (is_numeric($dados[0])) {
            $idEmpresa = $dados[0];
        } else {
            $idEmpresa = $this->_BASES[$this->_getEnviroment()][strtoupper($dados[0])];
        }

        $bases = $this->_getBasesId();
        $this->prepareConstants($bases[$idEmpresa]);
        $body = $this->gerarHtmlResultado($idEmpresa, str_replace('-', '/', $dados[1]), true);
        $this->addVar('corpo', $body);
        $this->addVar('nome_base', $this->_getNomeBaseNome($bases[$idEmpresa]));

        $html = $this->parse($this->templates . 'mensagem.html');
        die($html);
    }

    public function getVisualizarCertificadoAction()
    {

        $request = $this->request()->toArray();
        $id = $this->decrypt(Util::base64UrlDecode(current($request['id'])));
        if (preg_match('|[0-9]+_[0-9]+_[0-9]+|', $id, $matches)) {
            $ids = explode('_', $id);
            $LINE = __LINE__ + 1;
            Log::logArquivo("[".__METHOD__.":".$LINE."] DEB:[ Campos explode: ".print_r($ids, true)."]\n", LOG_NEWS);

            $idVenda = $ids[0];
            $idCliente = $ids[1];
            $idEdicao = $ids[2];
            $dadosSDS = $this->SdsObterUsuario($idCliente);
            $cliente = $dadosSDS['dados'];
            $html = $this->gerarHtmlCertificado($cliente['id_empresa'], $idVenda, $idEdicao, '');
            die($html);
        }
    }

    public function gerarLinkConfirmar($idNewsletter, $texto)
    {

        $hash = Util::base64UrlEncode($this->encrypt($idNewsletter));
        return '<a href="' . $this->getHost() . '/newsletter/' . $hash . '/confirmar-cadastro" style="font-size:16px">' . $texto . '</a></center>';
    }

    public function getCancelarAction()
    {

        require_once 'app/repository/NewsletterRepositorio.php';
        $NewsletterRepositorio = new NewsletterRepositorio();

        //PARAMETROS

        $request = $this->request()->toArray();
        $id = $this->decrypt(Util::base64UrlDecode(current($request['id'])));
        $Newsletter = $NewsletterRepositorio->selecionar($NewsletterRepositorio->createModel()->setId($id));

        $acao = true;
        if ($Newsletter != null) {
            $Newsletter->setSituacao(0);
            $NewsletterRepositorio->atualizar($Newsletter);
        } else {
            $acao = false;
        }

        $bases = $this->_getBasesId();
        $base = $bases[$Newsletter->getIdEmpresa()];
        $this->prepareConstants($bases[$Newsletter->getIdEmpresa()]);

        $html = '';
        $corpo = '';
        if (strlen($Newsletter->getNome()) > 0) {
            $saudacao = $Newsletter->getNome();
        } else {
            $saudacao = 'Caro Cliente';
        }

        if ($acao) {
            $corpo .= $saudacao . ", é uma pena você ir, mas saiba que sempre estaremos aqui para quando quiser voltar.<br><br>";
            $corpo .= "Nunca esqueça, a sorte vem para quem acredita nela.<br><br>";
            $corpo .= $this->_getNomeBaseNome($base) . ", custa pouco sonhar.<br><br>";
        } else {
            $corpo .= 'Não conseguimos encontrar seu cadastro!, entre contato através do e-mail <a href="mailto:"' . $this->_CONTATO[$base]['email_base'] . '" target="_blank">' . $this->_CONTATO[$base]['email_base'] . '</a><br><br>';
        }

        $this->addVar('tipo', 'cancelamento');
        $this->addVar('saudacao', $saudacao);
        $this->addVar('corpo', Util::utf8D($corpo));

        $html = $this->parse(APP_PATH . DS . 'templates' . DS . 'newsletter.html');

        die(Util::utf8E($html));
    }

    public function getConfirmarCadastroAction()
    {

        require_once 'app/repository/NewsletterRepositorio.php';
        $NewsletterRepositorio = new NewsletterRepositorio();

        //PARAMETROS

        $request = $this->request()->toArray();
        $id = $this->decrypt(Util::base64UrlDecode(current($request['id'])));
        $Newsletter = $NewsletterRepositorio->selecionar($NewsletterRepositorio->createModel()->setId($id));

        $ativado = true;
        if ($Newsletter != null) {
            $Newsletter->setSituacao(1);
            $Newsletter->setDataHoraAtualizacao(date('d/m/Y H:i:s'));
            $NewsletterRepositorio->atualizar($Newsletter);
        } else {
            $ativado = false;
        }

        $bases = $this->_getBasesId();
        $base = $bases[$Newsletter->getIdEmpresa()];
        $this->prepareConstants($bases[$Newsletter->getIdEmpresa()]);

        $html = '';
        $corpo = '';
        if (strlen($Newsletter->getNome()) > 2) {
            $saudacao = $Newsletter->getNome();
        } else {
            $saudacao = 'Caro Usuário';
        }

        if ($ativado) {
            $corpo .= $saudacao . ", agora você faz parte da newsletter do " . $this->_getNomeBaseNome($base) . ".<br><br>";
            $corpo .= "Seja muito bem-vindo e aguarde todas as novidades exclusivas que você só vê por aqui.<br><br>";
            $corpo .= $this->_getNomeBaseNome($base) . ", sua felicidade é aqui.<br><br>";
        } else {
            $corpo .= 'Não conseguimos encontrar seu cadastro!, entre contato através do e-mail <a href="mailto:"' . $this->_CONTATO[$base]['email_base'] . '" target="_blank">' . $this->_CONTATO[$base]['email_base'] . '</a><br><br>';
        }

        $this->addVar('tipo', 'ola');
        $this->addVar('saudacao', $saudacao);
        $this->addVar('corpo', Util::utf8D($corpo));
        $this->addVar('extra', '');

        $html = $this->parse(APP_PATH . DS . 'templates' . DS . 'newsletter.html');

        die(Util::utf8E($html));
    }

    protected function dadosObrigatorios($dados)
    {

        $obrigatorios = array('nome', 'email');

        $campos = array(
            'nome' => array('desc' => 'Nome', 'validar' => in_array('nome', $obrigatorios)),
            'email' => array('desc' => 'E-mail', 'validar' => in_array('email', $obrigatorios))
        );

        $obrigatorios = array();
        foreach ($campos as $nome => $campo) {
            if ($campo['validar'] && strlen($dados[$nome]) == 0) {
                $obrigatorios[] = $campo['desc'];
            }
        }

        if (count($obrigatorios) > 0) {
            if (count($obrigatorios) == 1) {
                return 'O seguinte campo é obrigatório<br> * ' . implode('<br> * ', $obrigatorios);
            } else {
                return 'Os seguintes campos são obrigatórios<br> * ' . implode('<br> * ', $obrigatorios);
            }
        } else {
            return null;
        }
    }

    public function emailAtivacaoContaNewsletter($idEmpresa, $idNewsletter)
    {

        require_once 'app/repository/NewsletterRepositorio.php';
        $NewsletterRepositorio = new NewsletterRepositorio();

        $host = $this->getHost();
        $bases = $this->_getBasesId();
        $base = $bases[$idEmpresa];
        $Newsletter = $NewsletterRepositorio->selecionar($NewsletterRepositorio->createModel()->setId($idNewsletter));

        $smarty = $this->Smarty;
        $html = '';

        $corpo = '<center style="font-size:20px;font-weight:bold">Bem-vindo ao ' . $this->_getNomeBaseNome($base) . '!</center><br><br>';
        $corpo .= "Seu cadastro foi realizado com sucesso. Por favor, confirme seu e-mail clicando no link abaixo.<br><br>";

        $hash = Util::base64UrlEncode($this->encrypt("{$Newsletter->getId()}"));
        $link = $host . "/newsletter/" . $hash . "/confirmar-cadastro";

        $corpo .= '<a href="' . $link . '">[Confirmar meu endereço de e-mail]</a><br><br>';
        $corpo .= "Não se preocupe, não compartilhamos seu email com terceiros e não enviamos SPAM!<br><br><br>";

        $this->addVar('host', $host);
        $this->addVar('logo', $this->_EMAIL[$base]['logo']);
        $this->addVar('entidade', $this->_EMAIL[$base]['entidade']);
        $this->addVar('auditoria', $this->_EMAIL[$base]['auditoria']);
        $this->addVar('nome_base', Util::utf8D($this->_getNomeBaseNome($base)));
        $this->addVar('saudacao', Util::utf8D('Olá ' . $Newsletter->getNome()));
        $this->addVar('corpo', Util::utf8D($corpo));
        $this->addVar('email_base', $this->_CONTATO[$base]['email_base']);
        $this->addVar('telefone_base', $this->_CONTATO[$base]['telefone_base']);
        $this->addVar('site', $this->_SITES[$base]);
        $this->addVar('rodape', '<span style="font-size:10px">' . Util::utf8D($this->_EMAIL[$base]['texto_base']) . '</span>');
        $this->addVar('extra', '');
        $html = $this->parse(APP_PATH . DS . 'templates' . DS . 'mensagem.html');

        return $this->sendMail($Newsletter->getIdEmpresa(), $Newsletter->getEmail(), $Newsletter->getNome(), Util::utf8D('Ativação de Newsletter - ' . $this->_getNomeBaseNome($base)), $html);
    }

    public function postCadastroAction()
    {

        require_once 'app/repository/NewsletterRepositorio.php';
        $NewsletterRepositorio = new NewsletterRepositorio();

        //VARIAVEIS

        $dados = array();
        $success = true;
        $status = self::HTTP_OK;

        //PARÂMETROS

        $request = $this->request()->toArray();
        $request['email'] = strtolower($request['email']);
        $request['nome'] = $this->limparNome($request['nome']);

        try {

            //DADOS

            $validacao = $this->dadosObrigatorios($request);

            if (!empty($validacao)) {
                $dados = array('success' => false, 'message' => $validacao);
            } else if (!Validacao::email($request['email'])) {
                $dados = array('success' => '', 'message' => self::$_MENSAGENS['EMAIL_INVALIDO']);
            } else {
                $Newsletter = $NewsletterRepositorio->createModel();
                $Newsletter->setIdEmpresa($request['id_empresa']);
                $Newsletter->setEmail($request['email']);
                $Newsletter = $NewsletterRepositorio->selecionar($Newsletter);

                if (!empty($Newsletter)) {

                    if ($Newsletter->getSituacao() == 1) {

                        $dados = array('success' => false, 'message' => self::$_MENSAGENS['NEWSLETTER_CADASTRO_EMAIL_EM_USO']);

                    } else {

                        $NewsletterRepositorio->setProperties($Newsletter, $request);
                        $Newsletter->setSituacao(0);
                        $NewsletterRepositorio->atualizar($Newsletter);

                        //ENVIO DE E-MAIL DE ATIVACAO DE CONTA
                        $envio = $this->emailAtivacaoContaNewsletter($Newsletter->getIdEmpresa(), $Newsletter->getId());
                        $dados = array('success' => true, 'message' => self::$_MENSAGENS['NEWSLETTER_CADASTRO_SUCESSO']);
                    }

                } else {

                    $Newsletter = $NewsletterRepositorio->createModel();
                    $NewsletterRepositorio->setProperties($Newsletter, $request);
                    $Newsletter->setSituacao(0);
                    $idNewsletter = $NewsletterRepositorio->inserir($Newsletter);
                    $Newsletter->setId($idNewsletter);

                    //ENVIO DE E-MAIL DE ATIVACAO DE CONTA
                    $envio = $this->emailAtivacaoContaNewsletter($Newsletter->getIdEmpresa(), $Newsletter->getId());

                    if (!empty($envio['errors'])) {
                        //KABUETA
                        $this->KabuetaMessage('ER', 'Cadastro Newsletter', 'O e-mail (' . $Newsletter->getEmail() . ') não pode ser enviado.', array('Erro' => json_encode($envio)), self::KABUETA_GRUPO_NEWSLETTER);
                        $dados = array('success' => false, 'message' => self::$_MENSAGENS['NEWSLETTER_EMAIL_ERRO_ENVIO']);
                    } else {
                        $dados = array('success' => true, 'message' => self::$_MENSAGENS['NEWSLETTER_CADASTRO_SUCESSO']);
                    }
                }
            }

        } catch (Exception $e) {

            $dados = array('success' => false, 'message' => self::$_MENSAGENS['NEWSLETTER_CADASTRO_FALHA']);
            $Exception = new Exceptionn(Util::utf8D($e->getMessage()), $e->getCode(), __FILE__, __LINE__);
            $Exception->log();
        }

        //RETORNO

        $this->response($status, $dados);
    }
}
