<?php

class SparkPost {

    protected $key;
    
    public function __construct() {
        
    }

    public function setKey($key) {
        
        $this->key = $key;
    }
    
    public function restApi($method, $uri, $payload = array(), $headers = array()) {
        
        $method = strtoupper($method);
        $defaultHeaders = array('Content-Type: application/json');
        $finalHeaders = array_merge($defaultHeaders, $headers);
        $url = 'https://api.sparkpost.com:443/api/v1/'.$uri;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        if ($method !== 'GET') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $finalHeaders);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
    
    public function send($nameFrom, $emailFrom, $nameTo, $emailTo, $subject, $body) {

        $headers = array('Authorization: '.$this->key);
        $payload = array(
            'content' => array(
                'from' => array(
                    'name'  => utf8_encode($nameFrom),
                    'email' => utf8_encode($emailFrom)
                ),
                'subject' => utf8_encode($subject),
                'html' => utf8_encode($body),
                'text' => strip_tags(utf8_encode($body))
            ),
            'recipients' => array(
                array(
                    'address' => array('name' => $nameTo, 'email' => $emailTo)
                )
            )
        );
        return $this->restApi('POST', 'transmissions', $payload, $headers);
    }
}