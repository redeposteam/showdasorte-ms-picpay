<?php

/**
 * Criação de Consultas SQL.
 */

/**
 * Select
 */

require_once 'lib/db/Select.php';

/**
 * Criação de Expresões SQL.
 */

/**
 * Expression
 */

require_once 'lib/db/Expression.php';

/**
 * Model
 */

require_once 'lib/db/Model.php';

/**
 * Repository
 */

require_once 'lib/db/Repository.php';

/**
 * Define de forma abstrata os métodos que deverão ser 
 * implementados pelas classes filhas.
 */

/**
 * Classe responsável pela comunicação com bases de dados.
 * 
 * @name		Db
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
abstract class Db {
	
	/**
	* Váriável que controla a geração de log de consultas.
	* 
	* @static
	* @access private
	* @name bool $log
	*/	
    protected static $log;
	
	/**
	 * Guarda uma instância válida da classe Db.
	 * 
	 * @static
	 * @access private
	 * @name Instance $log
	 */
	public static $Instance;

	/**
	 * Construtor da classe
	 *
	 * @param String $dsn
	 * @param String $username Nome do usuário do base de dados.
	 * @param String $password Senha do usuário do base de dados.
	 * @param bool 	 $log Ativa o log de registro de consultas.
	 * @param array  $options
	 * @return bool
	 */
	private function __construct($dsn, $username = null, $password = null, $log = false, $options = array()){}

	/**
	 * Retorna a conexão Db.
	 *
	 * @return Db
	 */
	public static function getInstance($dsn, $username = null, $password = null, $log = false, $options = array()) {
	    
	}
	
	/**
	 * Inicia um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public abstract function beginTransaction();

	/**
	 * Confirma finaliza um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public abstract function commit();

	/**
	 * Reverte uma transação.
	 *
	 * @return bool
	 */
	public abstract function rollBack();
	
	/**
	 * Busca o SQLSTATE associado com a última operação no manipulador da base de dados.
	 *
	 * @return string
	 */
	public abstract function errorCode();

	/**
	 * Busca informações sobre o erro associado com a última operação no manipulador da base de dados.
	 *
	 * @return array
	 */
	public abstract function errorInfo();

	/**
	 * Executa uma instrução SQL e retorna o número de linhas afetadas.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return int Número de linhas afetadas.
	 */
	public abstract function exec($sql);

	/**
	 * Executa uma instrução SQL, retornando um conjunto de resultados como um objeto PDOStatement.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return Db
	 */
	public abstract function query($sql);

	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public abstract function fetch($mode);

	/**
	 * Executa a consulta e retorna uma coluna.
	 *
	 * @param int $numberColumn indice da coluna.
	 * @return array
	 */
	public abstract function fetchColumn($numberColumn = 0);

	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return mixed
	 */
	public abstract function fetchAll($mode);
	
	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public abstract function queryFetchAllNum($sql);
	
	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public abstract function queryFetchAllAssoc($sql);

	/**
	 * Executa a consulta e retorna uma linha em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public abstract function queryFetchRowAssoc($sql);

	/**
	 * Executa a consulta e seleciona apenas uma coluna.
	 *
	 * @param  string $sql Instrução SQL válida para bancos de dados.
	 * @return mixed
	 */
	public abstract function queryFetchColAssoc($sql);
	
	/**
	 * Executa a consulta utilizando binds.
	 *
	 * @param  string $sql Instrução SQL válida com binds para bancos de dados.
	 * @return Db
	 */
	public function bind($sql, $binds){}

	/**
	 * Coloca entre aspas expressões necessárias da consulta.
	 *
	 * @param string $input
	 * @param int $parameter_type
	 * @return string
	 */
	public abstract function quote($input, $parameter_type = 0);

	/**
	 * Adiciona a cláusula LIMIT na consulta.
	 *
	 * @param mixed $sql
	 * @param int $count
	 * @param int $offset
	 * @return string
	 */
	public function limit($sql, $count, $offset = 0) {
	
		$count = intval($count);
	
		if ($count <= 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT count=$count não é válido");
		}
	
		$offset = intval($offset);
	
		if ($offset < 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT offset=$offset não é válido");
		}
	
		$sql .= " LIMIT $count";
	
		if ($offset > 0) {
			$sql .= " OFFSET $offset";
		}
	
		return $sql;
	}

	/**
	 * Lista as tabelas da base de dados.
	 *
	 * @return mixed|string
	 */
	public abstract function listTables($esquema = null);

	/**
	 * Lista os meta-dados da tabela.
	 *
	 * @param string $tabela
	 * @param string $esquema
	 * @return mixed|string
	 */
	public abstract function metadataTable($tabela, $esquema = null);
	
	/**
	 * Retorna o ID da última linha inserida ou valor de sequência.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public abstract function lastSequenceId($sequenceName);
	
	/**
	 * Gera um novo valor para sequência especificada e a retorna.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public abstract function nextSequenceId($sequenceName);
	
	/**
	 * Retorna o ID da última linha inserida.
	 *
	 * @param string $tableName   OPTIONAL Nome da tabela.
	 * @param string $primaryKey  OPTIONAL Nome da coluna chave primaria.
	 * @return string
	 */
	public abstract function lastInsertId($tableName = null, $primaryKey = null, $esquema = null);
	
	/**
	 * Gera um log de consulta.
	 *
	 * @param string $sql
	 * @return void
	 */
	protected static function log($sql) {
		if(self::$log){
			Log::logSql($sql);
		}
	}
}