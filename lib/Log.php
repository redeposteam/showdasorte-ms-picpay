<?php

/**
 * Guarda informações de ações do sistema.
 */

/**
 * Classe para criação de logs no sistema.
 * 
 * @name		Log
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Log {

    const SEPARADOR = "\n--------------------------------------------------------------------------------------------------------------\n";
    
	public static function date($format, $utimestamp) {
	    
	    $timestamp = floor($utimestamp);
	    $ms = floor(($utimestamp - $timestamp) * 1000);
	    $ms = str_pad($ms, 3, '0', STR_PAD_LEFT);
	    return date(preg_replace('|(?<!\\\\)u|', $ms, $format), $timestamp);
	}
	
	/**
	 * Cria log em arquivo passado como parâmetro.
	 * 
	 * @param string $file
	 * @param string $info
	 * @return void
	 */
	public static function logger($file, $info) {

		$fp = fopen($file,'a+');
		fwrite($fp, Util::utf8E($info));
		fclose($fp);
	}
	
	/**
	 * Cria log em arquivo passado como parâmetro.
	 * 
	 * @param string $mensagem
	 * @param string $arquivo
	 * @return void
	 */
	public static function logArquivo($mensagem, $arquivo = LOG_FILE) {
		
		self::logger($arquivo, $mensagem);
	}

	/**
	 * Cria log de sql em arquivo, no local default do sistema.
	 * 
	 * @param string $mensagem
	 * @return void
	 */
	public static function logSql($mensagem) {
		
	    $mensagem = str_replace("\n", "", $mensagem);
	    $mensagem = str_replace("\r", "", $mensagem);
	    $mensagem = preg_replace('|\s{2,}|', ' ', $mensagem);
	    self::logger(LOG_FILE_SQL, Log::date('H:i:s.u', microtime(true)).' DEBUG: '.$mensagem."\n");
	}
	
	/**
	 * Cria o log de erro em arquivo.
	 * 
	 * @param string $mensagem
	 * @return void
	 */
	public static function logErro($mensagem) {
		
	    self::logger(LOG_FILE_ERRO, Log::date('H:i:s.u', microtime(true))."\n".$mensagem.self::SEPARADOR);
	}
	
	/**
	 * Cria o log de exceção em arquivo.
	 * 
	 * @param string $mensagem
	 * @return void
	 */
	public static function logExcecao($mensagem) {
		
		self::logger(LOG_FILE_EXCEPTION, Log::date('H:i:s.u', microtime(true))."\n".$mensagem.self::SEPARADOR);
	}
}