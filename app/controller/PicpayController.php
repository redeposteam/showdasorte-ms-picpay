<?php

require_once 'lib/picpay/PicpayApi.php';
require_once 'lib/picpay/configuracao.php';
require_once 'app/controller/AppController.php';

class PicpayController extends AppController {

    public static $STATUS = array('paid' => 'Paga', 'completed' => 'Paga', 'chargeback' => 'Paga', 'created' => 'Pendente', 'analysis' => 'Pendente', 'refunded' => 'Cancelada', 'expired' => 'Expirado');

    const MENSAGEM_ERRO_ERTIFICADOS_ZERADOS = 'Sem produto disponível no momento.';
    const MENSAGEM_ERRO_COMPRA = 'Não foi possível realizar o pagamento';
    const MENSAGEM_ERRO_RESGATE = 'Não foi possível realizar o registro do formulário de resgate';
    const MENSAGEM_ERRO_REFERENCIA = 'Não foi possível concluir a compra. Tente novamente.';

    public function __construct($action) {

        parent::__construct();
        $this->action = $action;
        $this->actionsNoValidateJwt = $this->actionsNoValidateCsrf = array('postcheckout', 'postnotification');
    }

    //--------------
    //METODOS LOCAIS
    //--------------

    protected function formatarRetornoPicpay($dados) {

        if(isset($dados['message'])) {
            $ret = array('codigo_execucao' => '-999', 'mensagem' => Util::utf8E($dados['message']).(isset($dados['errors']) ? ':'.json_encode($dados['errors']) : ''));
            unset($dados['message']);unset($dados['errors']);
        } else {
            $ret = array('codigo_execucao' => '0', 'dados' => arrayChangeKeyCase($dados));
        }
        return $ret;
    }

    public function PicPayComprar($dados) {

        return $this->formatarRetornoPicpay(PicpayApi::criarPagamento($dados));
    }

    public function PicpayConsultarTransacao($referenceId) {

        return $this->formatarRetornoPicpay(PicpayApi::consultarPagamento($referenceId));
    }

    public function PicpayCancelarTransacao($dados) {

        return $this->formatarRetornoPicpay(PicpayApi::cancelarPagamento($dados));
    }

    public function soNumeros($var) {

        return preg_replace('|[^0-9]+]|', '', $var);
    }

    public function soLetras($var) {

        return preg_replace('|[^a-zA-Z\s]+|', '', $var);
    }

    public function validarAccessToken($idCliente) {

        $token = $this->getTokenPayload();
        return ($token['id'] == $idCliente);
    }

    public function tipoDado($var) {

        $tipo = 'null';
        if(is_string($var)) {
            if(preg_match('/^(([0-9]+(\.)?[0-9]+)|([0-9]+(\,)?[0-9]+))$/', $var, $m)) {
                $t = str_replace(',', '.', $var);
                if(strpos($t, '.') !== false) {
                    $tipo = 'double';
                } else {
                    $tipo = 'integer';
                }
            } else {
                $tipo = gettype($var);
            }
        } else {
            $tipo = gettype($var);
        }
        return $tipo;
    }

    public function validarCampo($campo, $condicoes) {

        $ret  = true;
        if(strlen($campo) > $condicoes['max']) {
            $ret = false;
        }
        return $ret;
    }

    public function validarDadosHash($dados) {

        $validacoes = array
        (
            'id'             => array('min' => null, 'max' =>   7, 'type' => 'integer'),
            'cpf'            => array('min' => null, 'max' =>  11, 'type' => 'integer'),
            'cep'            => array('min' => null, 'max' =>  11, 'type' =>      null),
            'nome'           => array('min' => null, 'max' => 100, 'type' =>  'string'),
            'tipo'           => array('min' => null, 'max' =>   1, 'type' => 'integer'),
            'valor'          => array('min' => null, 'max' =>   6, 'type' =>  'double'),
            'email'          => array('min' => null, 'max' =>  70, 'type' =>  'string'),
            'numero'         => array('min' => null, 'max' =>   5, 'type' =>      null),
            'bairro'         => array('min' => null, 'max' =>  50, 'type' =>  'string'),
            'remote'         => array('min' => null, 'max' =>  10, 'type' =>  'string'),
            'cidade'         => array('min' => null, 'max' =>  50, 'type' =>  'string'),
            'estado'         => array('min' => null, 'max' =>  20, 'type' =>  'string'),
            'prodId'         => array('min' => null, 'max' =>   5, 'type' => 'integer'),
            'numCard'        => array('min' => null, 'max' =>  19, 'type' => 'integer'),
            'telefone'       => array('min' => null, 'max' =>  15, 'type' =>  'string'),
            'parcelas'       => array('min' => null, 'max' =>   1, 'type' => 'integer'),
            'bandCard'       => array('min' => null, 'max' =>  10, 'type' =>  'string'),
            'idEdicao'       => array('min' => null, 'max' =>   5, 'type' => 'integer'),
            'idEmpresa'      => array('min' => null, 'max' =>   5, 'type' => 'integer'),
            'nascimento'     => array('min' => null, 'max' =>  11, 'type' =>  'string'),
            'codSegCard'     => array('min' => null, 'max' =>   4, 'type' => 'integer'),
            'logradouro'     => array('min' => null, 'max' =>  70, 'type' =>  'string'),
            'canalVenda'     => array('min' => null, 'max' =>   1, 'type' => 'integer'),
            'complemento'    => array('min' => null, 'max' =>  30, 'type' =>  'string'),
            'id_transacao'   => array('min' => null, 'max' =>   5, 'type' => 'integer'),
            'prodDescricao'  => array('min' => null, 'max' =>  50, 'type' =>  'string'),
            'idTipoLoteria'  => array('min' => null, 'max' =>   5, 'type' => 'integer'),
            'mesValidadeCard'=> array('min' => null, 'max' =>   2, 'type' => 'integer'),
            'anoValidadeCard'=> array('min' => null, 'max' =>   4, 'type' => 'integer')
        );

        $ret = true;
        foreach ($validacoes as $key => $value) {
            if(!$this->validarCampo($dados[$key], $value)) {
                $ret = false;
                break;
            }
        }
        return $ret;
    }

    public function validarCamposResgate($dados) {

        $validacoes = array
        (
            'rg'              => array('rule' => 'required', 'min' => 0, 'max' =>   11 , 'type' => 'integer', 'nome' => 'RG'),
            'ufDoador'        => array('rule' => 'required', 'min' => 0, 'max' =>   2  , 'type' =>  'string', 'nome' => 'UF'),
            'cpfdoador'       => array('rule' => 'required', 'min' => 0, 'max' =>   14 , 'type' => 'integer', 'nome' => 'CPF'),
            'cepDoador'       => array('rule' => 'required', 'min' => 0, 'max' =>   10 , 'type' =>      null, 'nome' => 'CEP'),
            'emailDoador'     => array('rule' => 'required', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Email'),
            'banco'           => array('rule' => 'required', 'min' => 0, 'max' =>   3  , 'type' =>  'string', 'nome' => 'Banco'),
            'orgao'           => array('rule' => 'required', 'min' => 0, 'max' =>   8  , 'type' =>  'string', 'nome' => 'Órgão Emissor'),
            'conta'           => array('rule' => 'required', 'min' => 0, 'max' =>   14 , 'type' => 'integer', 'nome' => 'Conta'),
            'genero'          => array('rule' => 'required', 'min' => 0, 'max' =>   1  , 'type' =>  'string', 'nome' => 'Gênero'),
            'estadoDoador'    => array('rule' => 'required', 'min' => 0, 'max' =>   30 , 'type' =>  'string', 'nome' => 'Estado'),
            'numeroRes'       => array('rule' => 'required', 'min' => 0, 'max' =>   6  , 'type' =>      null, 'nome' => 'Número'),
            'bairroDoador'    => array('rule' => 'required', 'min' => 0, 'max' =>   30 , 'type' =>  'string', 'nome' => 'Bairro'),
            'cidadeDoador'    => array('rule' => 'required', 'min' => 0, 'max' =>   30 , 'type' =>  'string', 'nome' => 'Cidade'),
            'agencia'         => array('rule' => 'required', 'min' => 0, 'max' =>   6  , 'type' => 'integer', 'nome' => 'Agência'),
            'nomePai'         => array('rule' =>         '', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Nome do Pai'),
            'nomeMae'         => array('rule' => 'required', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Nome da Mãe'),
            'operacao'        => array('rule' => 'required', 'min' => 0, 'max' =>   1  , 'type' => 'integer', 'nome' => 'Operação'),
            'telefoneDoador'  => array('rule' => 'required', 'min' => 0, 'max' =>   16 , 'type' => 'integer', 'nome' => 'Telefone'),
            'profissao'       => array('rule' => 'required', 'min' => 0, 'max' =>   30 , 'type' =>  'string', 'nome' => 'Profissão'),
            'logradouroDoador'=> array('rule' => 'required', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Logradouro'),
            'estadoCivil'     => array('rule' => 'required', 'min' => 0, 'max' =>   1  , 'type' =>  'string', 'nome' => 'Estado Civil'),
            'complementoDoador'=> array('rule' =>         '', 'min' => 0, 'max' =>   30 , 'type' =>  'string', 'nome' => 'Complemento'),
            'dataEmissao'     => array('rule' => 'required', 'min' => 0, 'max' =>   11 , 'type' =>  'string', 'nome' => 'Data de Emissão'),
            'nomeCompleto'    => array('rule' => 'required', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Nome Completo'),
            'naturalidade'    => array('rule' => 'required', 'min' => 0, 'max' =>   20 , 'type' =>  'string', 'nome' => 'Naturalidade'),
            'dataNascimento'  => array('rule' => 'required', 'min' => 0, 'max' =>   11 , 'type' =>  'string', 'nome' => 'Data de Nascimento'),
            'telefoneRecado'  => array('rule' =>         '', 'min' => 0, 'max' =>   16 , 'type' => 'integer', 'nome' => 'Telefone de Recado'),
            'cpfTitularConta' => array('rule' => 'required', 'min' => 0, 'max' =>   14 , 'type' => 'integer', 'nome' => 'CPF do Titular da Conta'),
            'nomeTitularBanco'=> array('rule' => 'required', 'min' => 0, 'max' =>   70 , 'type' =>  'string', 'nome' => 'Nome do Titular da Conta')
        );

        $ret = array();
        foreach ($validacoes as $key => $value) {
            if(!$this->validarCampo($dados[$key], $value)) {
                $ret[] = $validacoes[$key]['nome'];
            }
        }
        return $ret;
    }

    public function limparDadosSensiveis($dados) {

        $campos = array
        (
            'id','cep','tipo', 'remote', 'prodId','numero','bairro','cidade','estado','numCard','bandCard',
            'idEdicao', 'idEmpresa', 'canalVenda', 'logradouro', 'codSegCard', 'complemento', 'id_transacao',
            'prodDescricao', 'idTipoLoteria', 'mesValidadeCard', 'anoValidadeCard', 'idVendaConsolidada'
        );
        foreach ($campos as $key) {
            unset($dados[$key]);
        }
        return $dados;
    }

    protected function prepararDadosPicpay($dadosPortador) {

        $configuracoesVenda = ConfiguracaoVendasPicpay::getPorTipoLoteria($dadosPortador['idTipoLoteria']);
        $s = explode(' ', $dadosPortador['nome']);
        $nome = current($s);
        $sobrenome = implode(' ', array_slice($s, 1, count($s)));
        $dadosEdicao = $this->SdsObterEdicao($dadosPortador['idTipoLoteria'], $dadosPortador['idEdicao']);

        if($dadosEdicao['codigo_execucao'] != 0) {
            return null;
        }
        $dadosEdicao = $dadosEdicao['dados'];
        $registro = array
        (
            'referenceId' => $dadosPortador['idVendaConsolidada'].'_' .$dadosPortador['id'].'_'.$dadosPortador['idEdicao'].'_'.$dadosPortador['qtd'].'_'.$configuracoesVenda['IDENTIFICADOR_APLICACAO'],
            'valor' => $dadosPortador['valor'],
            'cpf' => $dadosPortador['cpf'],
            'email' => $dadosPortador['email'],
            'telefone' => $dadosPortador['telefone'],
            'nomeCliente' => $nome,
            'sobreNomeCliente' => $sobrenome,
        );
        return $registro;
    }

    protected function prepararDadosVenda($dadosCliente) {

        $time = time();
        $configuracoesVenda = ConfiguracaoVendasPicpay::getPorTipoLoteria($dadosCliente['idTipoLoteria']);

        $registro                    = array();
        $registro['id']              = $dadosCliente['idVendaConsolidada'];
        $registro['idEdicao']        = $dadosCliente['idEdicao'];
        $registro['idCliente']       = $dadosCliente['id'];
        $registro['idProduto']       = $dadosCliente['prodId'];
        $registro['idTipoLoteria']   = $dadosCliente['idTipoLoteria'];
        $registro['qtd']             = $dadosCliente['qtd'];
        $registro['valor']           = $dadosCliente['valor'];
        $registro['nomeCliente']     = $dadosCliente['nome'];
        $registro['cpfCliente']      = $dadosCliente['cpf'];
        $registro['telefoneCliente'] = $dadosCliente['telefone'];
        $registro['canalVenda']      = $dadosCliente['canalVenda'];

        $registro['prodId']            = $dadosCliente['prodId'];
        $registro['prod_id']           = $dadosCliente['prodId'];
        $registro['idProduto']         = $dadosCliente['prodId'];
        $registro['transacao_id']      = str_pad($dadosCliente['id'], 8, '0', STR_PAD_LEFT) . '-' . date('Y-md-Hi-', $time) . str_pad($dadosCliente['idVendaConsolidada'], 12, '0', STR_PAD_LEFT);
        $registro['id_cliente']        = $dadosCliente['id'];
        $registro['cli_nome']          = $dadosCliente['nome'];
        $registro['cli_email']         = $dadosCliente['email'];
        $registro['status_transacao']  = 'Em analise';
        $registro['vendedor_email']    = App::getConfig('picpay', 'email');
        $registro['prod_descricao']    = $dadosCliente['prodDesc'];
        $registro['prod_valor']        = ($dadosCliente['qtd'] * $dadosCliente['valor']);
        $registro['referencia']        = $dadosCliente['idVendaConsolidada'].'_' .$dadosCliente['id'].'_'.$dadosCliente['idEdicao'].'_'.$dadosCliente['qtd'].'_'.$configuracoesVenda['IDENTIFICADOR_APLICACAO'];
        $registro['tipo_pagamento']    = 'p';
        $registro['data_transacao']    = date('Y-m-d\TH:i:s', $time);
        $registro['data_hora_registro']= date('Y-m-d H:i:s', $time);
        $registro['situacao_email']    = 1;
        $registro['log_email']         = 'null';
        $registro['cartao']            = -1;

        return $registro;
    }

    public function preparaCertificadosVendaRandomica($dadosSDS) {

        $retorno = array();
        foreach($dadosSDS as $venda) {
            $bilhete = current($venda['bilhetes']);
            if(isset($bilhete['sequencial'])) {
                $retorno[] = $bilhete['sequencial'];
            }
        }
        return $retorno;
    }

    public function checkout() {

        $dados = array();
        $status = self::HTTP_OK;
        $request = $this->request()->toArray();

        try {

            //===================
            //IDENTIFICADOR VENDA
            //===================

            $LINE = __LINE__ + 1;
            $dadosSDS = $this->SdsGerarIdVenda();
            $idVendaConsolidada = $dadosSDS['dados']['transacao_id'];

            if($dadosSDS['codigo_execucao'] != 0) {
                throw new Exception($dadosSDS['mensagem']);
            }

            //===================
            //REGISTRAR PAGSEGURO
            //===================

            $dadosCliente = json_decode($request['transacao'], true);
            $dadosCliente['idVendaConsolidada'] = $idVendaConsolidada;

            $LINE = __LINE__ + 2;

            $registro = $this->prepararDadosPicpay($dadosCliente);

            if (!$this->validarReferenciaShowDaSorte($registro['referenceId'])) {
                Log::logArquivo("[".__METHOD__.":".$LINE."] DEB:[ Campo(s) obrigatorio(s): ".print_r($registro, true)."]\n", LOG_PICPAY);

                throw new Exception(self::MENSAGEM_ERRO_REFERENCIA);
            }

            //=================
            //REGISTRAR RESGATE
            //=================

            $LINE = __LINE__ + 1;

            // Recuperando a edição atual do tipo loteria
            $edicao = $this->SdsObterEdicaoAtual($dadosCliente['idTipoLoteria']);
            $dadosEdicao = $edicao;
            if($dadosEdicao['codigo_execucao'] != 0) {
                throw new Exception(self::MENSAGEM_ERRO_COMPRA);
            }
            $dataEdicao = date('Y-m-d', strtotime($dadosEdicao['dados']['data_hora_sorteio']));

            $registrarResgateDoacao = true;
            $LINE = __LINE__ + 1;
            // Caso contenha os campos de não doadores
            if (isset($request['inputs']) && count($request['inputs']) > 0) {
                $LINE = __LINE__ + 1;
                $registrarResgateDoacao = $this->registrarResgateDoacao($request, $dadosCliente['idTipoLoteria'], $dataEdicao, $idVendaConsolidada, $edicao['dados']['id']);
            }
            // Caso seja erro no não doadores
            if(!$registrarResgateDoacao) {
                throw new Exception(self::MENSAGEM_ERRO_RESGATE);
            }

            /** REGISTRAR VENDA PENDENTE SDS **/
            // Confirmando 
            $LINE = __LINE__ + 2;

            $dadosSDS = $this->SdsObterUsuario($dadosCliente['id']);

            if ($dadosSDS['codigo_execucao'] != 0) {
                throw new Exception(self::MENSAGEM_ERRO_COMPRA);
            }

            $dadosSdsUsuario = $dadosSDS['dados'];

            $dadosClienteTransacao = array();
            $dadosClienteTransacao['idVendaConsolidada'] = $idVendaConsolidada;
            $dadosClienteTransacao['idEdicao']           = $dadosCliente['idEdicao'];
            $dadosClienteTransacao['id']                 = $dadosSdsUsuario['id'];
            // Obtendo a Edi��o
            $edicao = $this->SdsObterEdicaoAtual($dadosCliente['idTipoLoteria']);
            // Setando os dados da edi��o
            $dadosClienteTransacao['prodId']         = $edicao['dados']['produtos'][0]['id'];
            $dadosClienteTransacao['prod_id']        = $edicao['dados']['produtos'][0]['id'];
            $dadosClienteTransacao['idProduto']      = $edicao['dados']['produtos'][0]['id'];
            $dadosClienteTransacao['idTipoLoteria']  = $dadosCliente['idTipoLoteria'];
            // Setando os valores padr�es
            $dadosClienteTransacao['qtd']        = $dadosCliente['qtd'];
            // Setando o valor utilizando a informa��o recebida da edi��o e n�o do navegador
            $dadosClienteTransacao['valor']      = $edicao['dados']['produtos'][0]['valor'];
            // Dados pessoais do cliente
            $dadosClienteTransacao['nome']       = $dadosCliente['nome'];
            $dadosClienteTransacao['cpf']        = $dadosCliente['cpf'];
            $dadosClienteTransacao['telefone']   = $dadosSdsUsuario['telefone'];
            $dadosClienteTransacao['canalVenda'] = 1;
            // C�digo da transa��o que s� ser� informada na confirma��o
            $dadosClienteTransacao['id_transacao']       = $idVendaConsolidada;
            $dadosClienteTransacao['email']              = $dadosSdsUsuario['email'];
            $dadosClienteTransacao['statusTransacao']    = 2;
            $dadosClienteTransacao['tipoPagamento']      = '';
            $dadosClienteTransacao['dataTransacao']      = '';
            $dadosClienteTransacao['cartao']             = -1;

            $LINE = __LINE__ + 2;

            $registroRandomico = $this->SdsVendaRandomica($this->prepararDadosVenda($dadosClienteTransacao));
            // Caso n�o tenha sido poss�vel registrar com sucesso
            if (empty($registroRandomico) || $registroRandomico['codigo_execucao'] != 0) {
                $success = 'false';
                $message = $registroRandomico['mensagem'];
                $dados = array('success' => $success, 'message' => $message);
                throw new Exception(Util::utf8D($message));
            } else {
                //DISPARA O ENVIO DE E-MAIL
                $return = $this->dispararEmailShow($dadosSdsUsuario, $edicao, 2, false, $registroRandomico);
                if (isset($return['errors'])) {
                    $this->KabuetaMessage('ER', 'E-mail', 'O e-mail ' . $registroRandomico['cli_email'] . ' n�o pode ser enviado.', array('Obs' => json_encode($return['errors'])));
                }
            }


            $dadosSDS = $this->PicPayComprar($registro);

            if($dadosSDS['codigo_execucao'] != 0) {
                throw new Exception(self::MENSAGEM_ERRO_COMPRA.'.');
            }

            $dados = array('success' => true, 'data' => $dadosSDS['dados'], 'message' => 'Venda Realizada com Sucesso!', 'url' => $dadosSDS['dados']['paymenturl']);

            

        } catch (Exception $e) {

            $dados = array('success' => false, 'message' => $e->getMessage());
            $Exception = new Exceptionn(Util::utf8D($e->getMessage()), $e->getCode(), __FILE__, $LINE);$Exception->log();
        }

        $this->response($status, $dados);
    }

    public function registrarResgateDoacao($dados, $idTipoLoteria, $dataEdicao, $idVendaConsolidada, $idEdicao, $certificados = array()){

        $ret = true;

        $LINE = __LINE__ + 1;
        $validarCamposResgate = $this->validarCamposResgate($dados['inputs']);

        if(!empty($validarCamposResgate)) {
            $ret = false;
            Log::logArquivo("[".__METHOD__.":".$LINE."] DEB:[ Campo(s) obrigatorio(s): ".implode(', ', $validarCamposResgate)."]\n", LOG_RESGATE);
            // Caso haja algum campo com formatação invalida
            throw new Exception("Falha: FAVOR TENTAR NOVAMENTE!");
        } else {
            // Mapeando os campos a serem submetidos
            foreach($dados['inputs'] as $strChave => $strValor){
                // Caso o campo seja complemento
                if($strChave == 'complemento'){
                    $strValor = empty($strValor) ? 'N/A': $strValor ;
                }else if($strChave == 'telefoneRecado'){
                    // Caso o campo seja telefone recado
                    $strValor = empty($strValor) ? '(00) 000000000': $strValor ;
                }
                // Setando os campos a serem preenchidos / enviados
                $dadosResgate[$strChave] = $strValor;
            }
            // Informando os certificados e o id da transação
            $dadosResgate['idVendaConsolidada'] = $idVendaConsolidada;
            $dadosResgate['certificados'] = $certificados;
            // ENviando os dados para o sds
            $dadosSDS= $this->SdsCadastrarResgateDoacao($dadosResgate, $idTipoLoteria, $dataEdicao, $idVendaConsolidada, $idEdicao);
            // Caso não dê sucesso
            if($dadosSDS['codigo_execucao'] != 0) {
                $ret = false;
                // Caso haja algum campo com formatação invalida
                throw new Exception($dadosSDS["mensagem"]);
            }
        }

        return $ret;
    }

    //----------------------
    //ACTIONS DO CONTROLADOR
    //----------------------

    public function postCheckoutAction() {

        $this->checkout();
    }

    protected function checarConfirmado($notification, $pagseguroTransacao) {

        $success = 'true';
        $dados = array('success' => $success, 'message' => 'Notificação Aciondada');

        if(self::$STATUS[$notification['status']] == 'Paga') {

            $reference = explode('_', $notification['referenceid']);
            $idVenda   = $reference[0];
            $idCliente = $reference[1];
            $nomeLive  = $reference[4];

            //VERIFICA SE JÁ EXISTE
            $dadosSDS = $this->SdsPagseguroConsultarTransacao($idVenda);

            if (!empty($dadosSDS['dados']) && $dadosSDS['codigo_execucao'] == 0 && $dadosSDS['dados'][0]['status_transacao'] == 'Paga') {

                $dados = array('success' => $success, 'data' => $dadosSDS['dados']);

            } else {

                $LINE = __LINE__ + 2;
                $dadosSDS = $this->SdsObterUsuario($idCliente);

                if($dadosSDS['codigo_execucao'] != 0) {
                    throw new Exception(self::MENSAGEM_ERRO_COMPRA);
                }
                // Time Atual
                $time = time();
                // Confirmando
                $registro = $this->SdsConfirmarCertificado($idVenda, $pagseguroTransacao['transacao_id']);
                // Atualizando os dados da transa��o para salvar no banco
                $pagseguroTransacao['status_transacao'] = self::$STATUS[$notification['status']];
                $pagseguroTransacao['transacao_id']     = str_pad($idCliente, 8, '0', STR_PAD_LEFT) . '-' . date('Y-md-Hi-', $time) . str_pad($idVenda, 12, '0', STR_PAD_LEFT);;
                $pagseguroTransacao['tipoPagamento']    = 'p';
                $pagseguroTransacao['dataTransacao']    = date('Y-m-d\TH:i:s', $time);
                $pagseguroTransacao['data_transacao']   = date('Y-m-d\TH:i:s', $time);
                $pagseguroTransacao['cartao']           = -1;

                // atualizando no sds
                $dadosSDSAtualizar = $this->SdsPagseguroAtualizarTransacao($pagseguroTransacao);
                if($dadosSDSAtualizar['codigo_execucao'] != 0) {
                    $success = 'false';
                    $dados = array('success' => $success, 'data' => $pagseguroTransacao);
                } else {
                    // Caso esteja habilitado para enviar email de confirmação
                    if($this->getEnviarEmailConfirmacao()){
                        $dados = array('success' => $success, 'data' => $pagseguroTransacao);
                        $dadosSDS = $this->SdsObterUsuario($idCliente);
                        // Pegando os dados do cliente
                        $dadosSdsUsuario = $dadosSDS['dados'];
                        // Recuperando o tipo loteria
                        $idTipoLoteria = $this->_TIPO_LOTERIA_NOTIFICATION[$this->_getEnviroment()][strtoupper($nomeLive)];
                        // Obtendo a edi��o
                        $edicao = $this->SdsObterEdicaoAtual($idTipoLoteria);
                        // Recuperando as vendas da transa��o
                        $arrVendas = $this->getVendasTransacao($idCliente, 3, $pagseguroTransacao['id'], $edicao["dados"]["id"]);
                        //DISPARA O ENVIO DE E-MAIL
                        $return = $this->dispararEmailShow($dadosSdsUsuario, $edicao, 4, true, $arrVendas);
                        if (isset($return['errors'])) {
                            $this->KabuetaMessage('ER', 'E-mail', 'O e-mail ' . $pagseguroTransacao['cli_email'] . ' n�o pode ser enviado.', array('Obs' => json_encode($return['errors'])));
                        }
                    }
                }
            }

        } else if(self::$STATUS[$notification['status']] == 'Cancelada' || self::$STATUS[$notification['status']] == 'Expirado') {

            $registro = $this->SdsAnularCertificado($pagseguroTransacao['id'], $pagseguroTransacao['transacao_id']);

            $reference = explode('_', $notification['referenceid']);
            $idVenda   = $reference[0];
            $idCliente = $reference[1];
            $nomeLive  = $reference[4];

            if(empty($registro) || $registro['codigo_execucao'] != 0) {

                $dados = array('success' => 'false', 'message' => $registro['mensagem']);
                $Exception = new Exceptionn(Util::utf8D($registro['mensagem']), 0, __FILE__, __LINE__);$Exception->log();

            } else {
                
                $pagseguroTransacao['status_transacao'] = self::$STATUS[$notification['status']];
                $pagseguroTransacao['data_transacao']   = date('Y-m-d\TH:i:s', $time);

                $this->SdsPagseguroAtualizarTransacao($pagseguroTransacao);
                $dados = array('success' => $success, 'data' => $pagseguroTransacao);

                $dadosSDS = $this->SdsObterUsuario($idCliente);
                // Pegando os dados do cliente
                $dadosSdsUsuario = $dadosSDS['dados'];
                // Recuperando o tipo loteria
                $idTipcdoLoteria = $this->_TIPO_LOTERIA_NOTIFICATION[$this->_getEnviroment()][strtoupper($nomeLive)];
                // Obtendo a edi��o
                $edicao = $this->SdsObterEdicaoAtual($idTipcdoLoteria);
                // Recuperando as vendas da transa��o
                $arrVendas = $this->getVendasTransacao($idCliente, 3, $pagseguroTransacao['id'], $edicao["dados"]["id"]);
                //DISPARA O ENVIO DE E-MAIL
                $return = $this->dispararEmailShow($dadosSdsUsuario, $edicao, 6, true, $arrVendas);
                if (isset($return['errors'])) {
                    $this->KabuetaMessage('ER', 'E-mail', 'O e-mail ' . $pagseguroTransacao['cli_email'] . ' n�o pode ser enviado.', array('Obs' => json_encode($return['errors'])));
                } 
            }

        } else {

            $dados = array('success' => $success, 'data' => array('codigo' => $notification['status'], 'status' => self::$STATUS[$notification['status']]));
        }

        return $dados;
    }

    /**
     * Método que ir recuperar as vendas pelo id da transação
     */
    public function getVendasTransacao($intIdCliente, $intSituacao, $idTransacao,$intIdEdicao = null){
        // Array de mapeamento das situações do pagseguro com as situações do banco
        $arrSituacoes   = array(
            // Pendente e em analise como pendente
            1=>1,2=>1,
            // Confirmado disponível como confirmado
            3=>2, 4=>2, 8=>2,
            // cancelado e devolvido
            6=>3, 7=>3,
        );
        // Situações para o couchbase
        $intSituacao    = (int) $arrSituacoes[$intSituacao];
        // Vendas do couchbase
        $arrVendas      = $this->SdsObterTransacoesClienteTipo($intIdCliente, $intSituacao, $intIdEdicao);
        // Variavel que ir· receber o retorno das vendas do banco
        $arrVendasRetorno = array();
        // Array de vendas temporarios
        $arrArrVendas = array();
        $bolCancelado = false;
        // Caso tenha vendas
        if(!empty($arrVendas) && !empty($arrVendas["dados"]["vendas"])){
            $arrArrVendas = $arrVendas["dados"]["vendas"];
        }else{
            $bolCancelado = true;
            $arrArrVendas = $arrVendas["dados"];
        }
        // para cada venda
        foreach($arrArrVendas as $intChave => $arrVenda){
            // Se a venda não for dessa transação atual (confirmada), ignoro!
            if($idTransacao != $arrVenda['id']) continue;

            // Caso seja a primeira venda seto o id da transação padrão
            if($intChave == 0) $arrVendasRetorno["transacao_id"] = $arrVenda["transacao_id"];
            // Sequencial a ser utilizado
            $intSequencial = $arrVenda["sequencial"] ;
            // Caso seja cancelado
            if($bolCancelado){
                $arrNumero = explode(".", $arrVenda["numero"]);
                $arrSequencial = explode("-", $arrNumero[3]);
                // pegando o número
                $intSequencial = $arrNumero[2].$arrSequencial[0];
            }
            // Iniciando o formatador de vendas
            $arrVendaFormatada = array();
            $arrVendaFormatada["valor"] = $arrVenda["valor_venda"];
            $arrVendaFormatada["id_venda"] = $arrVenda["id_venda"];
            $arrVendaFormatada["sequencial_venda"] = $intSequencial;
            $arrVendaFormatada["numero_venda"] = $arrVenda["numero"];
            $arrVendaFormatada["data_hora_venda"] = $arrVenda["data_hora_criacao"];
            // Detalhe da venda
            $arrVendaFormatada["bilhetes"][0]["sequencial"] = $intSequencial;
            $arrVendaFormatada["bilhetes"][0]["dezenas"] = (isset($arrVenda["bolas"]) && !empty($arrVenda["bolas"])) ? $arrVenda["bolas"] : array();
            $arrVendaFormatada["bilhetes"][0]["numero"] = $arrVenda["numero"];

            // setando a venda formatada para retorno
            $arrVendasRetorno["vendas"][] = $arrVendaFormatada;
        }
        // Retornando as vendas
        return $arrVendasRetorno;
    }


    public function postNotificationAction() {

        $dados = array();
        $date = date('d/m/Y');
        $statusHttp  = self::HTTP_OK;
        $request = $this->request()->toArray();
        $time = Log::date('H:i:s.u', microtime(true));

        $dadosPicpay = $this->PicpayConsultarTransacao($request['referenceId']);
        $dadosPicpay['dados']['authorizationid'] = isset($request['authorizationId']) ? $request['authorizationId'] : '';

        if ($dadosPicpay['codigo_execucao'] != 0) {

            $statusHttp  = self::HTTP_INTERNAL_SERVER_ERROR;
            Log::logArquivo($time.' RES:['.$_SERVER['REQUEST_URI'].'] DEB: '.str_replace(array('\/', '/', "\n"), array('\"','"',''), json_encode($request, true))."\n", LOG_PATH.DS.date('Y-m-d').'_notificacao.txt');

        } else {

            $pagseguroTransacao = null;
            if(isset($dadosPicpay['dados']['referenceid'])) {
                $reference          = explode('_', $dadosPicpay['dados']['referenceid']);
                $idVenda            = $reference[0];
                $referencia         = $dadosPicpay['dados']['referenceid'];
                $dadosSDS           = $this->SdsPagseguroConsultarTransacao($idVenda);
                if(!empty($dadosSDS['dados'])) {
                    $pagseguroTransacao = $dadosSDS['dados'][0];
                }
            }

            $sds = $this->checarConfirmado($dadosPicpay['dados'], $pagseguroTransacao);

            if(empty($sds) || $sds['success'] == 'false') {

                $statusHttp  = self::HTTP_INTERNAL_SERVER_ERROR;

                $transacao = array();
                $transacao['reference'] = $referencia;

                Log::logArquivo($time.' Response:['.$_SERVER['REQUEST_URI'].'] DEBUG SDS: '.str_replace('\/', '/',json_encode($transacao, true))."\n", LOG_NOTIFICACAO);
                $msg = 'Referencia - '.$referencia.': Sem resposta do SDS';

                if(isset($sds['success'])) {
                    $msg = 'Referencia - '.$referencia.': '.$sds['message'];
                }
                $this->KabuetaMessage('ER', 'SDS - Checar Confirmado - '.$date.' '.$time, $msg, array('Obs' => 'Verificar SDS'));
            }
            $dados = $sds;
        }

        $this->response($statusHttp, $dados);
    }
}
