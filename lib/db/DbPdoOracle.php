<?php

/**
 * Abstração de Classe de Dados.
 */

/**
 * Db
 */

require_once 'lib/db/DbPdo.php';

/**
 * Responsável pela comunicação exclusiva com PostgreSQL.
 */

/**
 * Extensão da Classe Db responsável pela comunicação com bases de dados, voltada para Oracle.
 *
 * @name		DbOracle
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class DbPdoOracle extends DbPdo {
	
    public function foldCase($key) {
        return strtolower((string) $key);
    }
    
	/**
	 * Lista as tabelas do banco de dados atual
	 *
	 * @return array
	 */
	public function listTables($esquema = null) {

		$sql = "SELECT table_name FROM all_tables".($esquema ? " WHERE OWNER = '".$esquema."'" : "");
		return $this->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);
	}
	
	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @return array
	 */
	public function metadataFk($tabela) {
	
	    $config = App::getConfig();
	
	    return $this->query("SELECT * FROM (
                                SELECT 
                                  a.table_name AS tabela_origem,
                                  rtrim(MAX(DECODE(c.position,1,c.column_name)) ||',' || MAX(DECODE(c.position,2,c.column_name)) ||','|| MAX(DECODE(c.position,3,c.column_name)) ||','|| MAX(DECODE(c.position,4,c.column_name)),',') AS campo_origem,
                                  b.table_name AS tabela_referencia,
                                  rtrim(MAX(DECODE(d.position,1,d.column_name)) ||','|| MAX(DECODE(d.position,2,d.column_name)) ||',' || MAX(DECODE(d.position,3,d.column_name)) ||',' || MAX(DECODE(d.position,4,d.column_name)),',') AS campo_referencia
                                FROM 
                                  all_constraints a, 
                                  all_constraints b, 
                                  all_cons_columns c, 
                                  all_cons_columns d 
                                WHERE 
                                  a.r_constraint_name = b.constraint_name
                                  AND a.constraint_name = c.constraint_name
                                  AND b.constraint_name = d.constraint_name
                                  AND a.constraint_type = 'R'
                                  AND b.constraint_type IN ('P', 'U')
                                  AND a.table_name = '".$tabela."' 
                                GROUP BY a.table_name, b.table_name ORDER BY 1
                              ) FOO ORDER BY tabela_origem, tabela_referencia")->fetchAll(PDO::FETCH_ASSOC);
	}	
	
	/**
	 * Lista as chaves estrangeiras da tabela passada no parametro
	 *
	 * @param string $tabela Nome da tabela
	 * @param string $esquema Nome do esquema
	 * @return array
	 */
	public function metadataTable($tabela, $esquema = null) {

		$sql = "SELECT 
                    TC.TABLE_NAME, 
		            TB.OWNER, 
		            TC.COLUMN_NAME, 
		            TC.DATA_TYPE,
                    TC.DATA_DEFAULT, 
		            TC.NULLABLE, 
		            TC.COLUMN_ID, 
		            TC.DATA_LENGTH,
                    TC.DATA_SCALE, 
		            TC.DATA_PRECISION, 
		            C.CONSTRAINT_TYPE, 
		            CC.POSITION
            FROM ALL_TAB_COLUMNS TC
            LEFT JOIN (ALL_CONS_COLUMNS CC JOIN ALL_CONSTRAINTS C ON 
		              (CC.CONSTRAINT_NAME = C.CONSTRAINT_NAME AND CC.TABLE_NAME = C.TABLE_NAME AND C.CONSTRAINT_TYPE = 'P')) 
		              ON TC.TABLE_NAME = CC.TABLE_NAME AND TC.COLUMN_NAME = CC.COLUMN_NAME
            JOIN ALL_TABLES TB ON (TB.TABLE_NAME = TC.TABLE_NAME AND TB.OWNER = TC.OWNER)
            WHERE UPPER(TC.TABLE_NAME) = UPPER(".$this->quote($tabela).")";
		
        if ($esquema) {
            $sql .= " AND UPPER(TB.OWNER) = UPPER(".$this->quote($esquema).")";
        }
        $sql.= ' ORDER BY TC.COLUMN_ID';

		$result = $this->queryFetchAllNum($sql);
		
		$table_name      = 0;
        $owner           = 1;
        $column_name     = 2;
        $data_type       = 3;
        $data_default    = 4;
        $nullable        = 5;
        $column_id       = 6;
        $data_length     = 7;
        $data_scale      = 8;
        $data_precision  = 9;
        $constraint_type = 10;
        $position        = 11;

        $desc = array();
        
        foreach ($result as $key => $row) {
            list ($primary, $primaryPosition, $identity) = array(false, null, false);
            if ($row[$constraint_type] == 'P') {
                $primary = true;
                $primaryPosition = $row[$position];
                /**
                 * Oracle does not support auto-increment keys.
                 */
                $identity = false;
            }
            $desc[$this->foldCase($row[$column_name])] = array(
                'SCHEMA_NAME'      => $this->foldCase($row[$owner]),
                'TABLE_NAME'       => $this->foldCase($row[$table_name]),
                'COLUMN_NAME'      => $this->foldCase($row[$column_name]),
                'COLUMN_POSITION'  => $row[$column_id],
                'DATA_TYPE'        => $row[$data_type],
                'DEFAULT'          => $row[$data_default],
                'NULLABLE'         => (bool) ($row[$nullable] == 'Y'),
                'LENGTH'           => $row[$data_length],
                'SCALE'            => $row[$data_scale],
                'PRECISION'        => $row[$data_precision],
                'UNSIGNED'         => null,
                'PRIMARY'          => $primary,
                'PRIMARY_POSITION' => $primaryPosition,
                'IDENTITY'         => $identity
            );
        }
        
        $fks = $this->metadataFk($tabela);
         
        return array('FIELDS' => $desc, 'FKS' => $fks);
	}
	
	/**
	 * Adiciona a cláusula LIMIT na consulta.
	 *
	 * @param mixed $sql
	 * @param int $count
	 * @param int $offset
	 * @return string
	 */
	public function limit($sql, $count, $offset = 0) {
	
	    $count = intval($count);
	
	    if ($count <= 0) {
	        throw new Exception('Class-'.__CLASS__.": o argumento LIMIT count=$count não é válido");
	    }
	
	    $offset = intval($offset);
	
	    if ($offset < 0) {
	        throw new Exception('Class-'.__CLASS__.": o argumento LIMIT offset=$offset não é válido");
	    }
	
	    $sql .= " LIMIT $count";
	
	    if ($offset > 0) {
	        $sql .= " OFFSET $offset";
	    }
	
	    return $sql;
	}
	
    /**
     * {@inheritDoc}
     * @see Db::lastSequenceId()
     */
    public function lastSequenceId($sequenceName) {
        
        return $this->query('SELECT '.$sequenceName.'.CURRVAL FROM dual')->fetchColumn();
    }

    /**
     * {@inheritDoc}
     * @see Db::nextSequenceId()
     */
    public function nextSequenceId($sequenceName) {
        
        return $this->query('SELECT '.$sequenceName.'.NEXTVAL FROM dual')->fetchColumn();
    }

    /**
     * {@inheritDoc}
     * @see Db::lastInsertId()
     */
    public function lastInsertId($tableName = null, $primaryKey = null, $esquema = null, $sequence = null) {
       
        $sequenceName = null;
        if($tableName != null) {
            if($sequence == null) {
                $sequenceName = $esquema.'.seq_'.$tableName;
                if($primaryKey){
                    $sequenceName.= '_'.current($primaryKey);
                }
            } else {
                $sequenceName = $esquema.'.'.$sequence;
            }
            return $this->lastSequenceId($sequenceName);
        }
        return null;
    }
}