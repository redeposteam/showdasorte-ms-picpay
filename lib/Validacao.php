<?php

class Validacao {

    public static function cpf($cpf) {

        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    public static function email($email) {

        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function nome($nome) {

        $valido = true;

        $pattern = '|[^A-ZÁ-Úa-zá-ú\s]+|';
        $nome = preg_replace($pattern, '', $nome);

        $pattern = '|\s{2,}|';
        $nome = preg_replace($pattern, ' ', $nome);

        $nome = trim($nome);
        $palavras = explode(' ', $nome);

        if(count($palavras) == 2 && strlen($palavras[1]) < 2) {
            $valido = false;
        }

        if(count($palavras) < 2) {
            $valido = false;
        }
        return $valido;
    }

    public static function data($data) {

        $temp = str_replace('_', '', $data);
        $temp = explode('/', $temp);
        if((strlen($temp[0]) != 2) || $temp[0] > 31) {
            return false;
        }
        if((strlen($temp[1]) != 2) || $temp[1] > 12) {
            return false;
        }
        if((strlen($temp[2]) != 4) || $temp[2] < 1900) {
            return false;
        }
        return checkdate($temp[1], $temp[0], $temp[2]);
    }
}
