<?php

/**
 * Criação de Consultas SQL.
 */

/**
 * Select
 */

require_once 'lib/db/Select.php';

/**
 * Criação de Expresões SQL.
 */

/**
 * Expression
 */

require_once 'lib/db/Expression.php';

/**
 * Model
 */

require_once 'lib/db/Model.php';

/**
 * Repository
 */

require_once 'lib/db/Repository.php';

/**
 * Define de forma abstrata os métodos que deverão ser 
 * implementados pelas classes filhas.
 */

/**
 * Classe responsável pela comunicação com bases de dados.
 * 
 * @name		DbPdo
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
abstract class DbPdo {
	
	/**
	* Váriável que controla a geração de log de consultas.
	* 
	* @static
	* @access private
	* @name bool $log
	*/	
	static private $log;
	
	/**
	 * Guarda uma instância válida da classe PDO.
	 * 
	 * @static
	 * @access private
	 * @name PDOInstance $log
	 */
	static private $PDOInstance;

	/**
	 * Construtor da classe
	 *
	 * @param String $dsn
	 * @param String $username Nome do usuário do base de dados.
	 * @param String $password Senha do usuário do base de dados.
	 * @param bool 	 $log Ativa o log de registro de consultas.
	 * @param array  $options
	 * @return bool
	 */
	public function __construct($dsn, $username = null, $password = null, $log = false, $options = array()) {
		
		if(!self::$PDOInstance) {
			self::$log = $log;
			self::$PDOInstance = new PDO($dsn, $username, $password, $options);
			self::$PDOInstance->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
			self::$PDOInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		return self::$PDOInstance;
	}

	/**
	 * Retorna a conexão PDO.
	 *
	 * @return PDO
	 */
	public function getPDO() {
		return self::$PDOInstance;
	}
	
	/**
	 * Definir um atributo da conexão
	 *
	 * @param int $attribute Atributo de conexão.
	 * @param mixed $value Valor a ser atribuido.
	 * @return bool
	 */
	public function setAttribute($attribute, $value) {
		return self::$PDOInstance->setAttribute($attribute, $value);
	}
	
	/**
	 * Inicia um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public function beginTransaction() {
		return self::$PDOInstance->beginTransaction();
	}

	/**
	 * Confirma finaliza um bloco de transação na base de dados.
	 *
	 * @return bool
	 */
	public function commit() {
		return self::$PDOInstance->commit();
	}

	/**
	 * Reverte uma transação.
	 *
	 * @return bool
	 */
	public function rollBack() {
		return self::$PDOInstance->rollBack();
	}
	
	/**
	 * Busca o SQLSTATE associado com a última operação no manipulador da base de dados.
	 *
	 * @return string
	 */
	public function errorCode() {
		return self::$PDOInstance->errorCode();
	}

	/**
	 * Busca informações sobre o erro associado com a última operação no manipulador da base de dados.
	 *
	 * @return array
	 */
	public function errorInfo() {
		return self::$PDOInstance->errorInfo();
	}

	/**
	 * Executa uma instrução SQL e retorna o número de linhas afetadas.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return int Número de linhas afetadas.
	 */
	public function exec($sql) {
		self::log($sql);
		return self::$PDOInstance->exec($sql);
	}

	/**
	 * Recupera um atributo de conexão da base de dados.
	 *
	 * @param int $attribute
	 * @return mixed Se houve sucesso é retornado o valor do atributo da conexão, caso contrário retorna null.
	 */
	public function getAttribute($attribute) {
		return self::$PDOInstance->getAttribute($attribute);
	}

	/**
	 * Retorna um array de drivers PDO disponíveis.
	 *
	 * @return array Retorna um array de nomes de drivers PDO disponíveis. Se não existe drivers disponíveis, irá retornar um array vazio. 
	 */
	public function getAvailableDrivers(){
		return self::$PDOInstance->getAvailableDrivers();
	}

	/**
	 * Prepara um comando para execução e retorna um objeto de declaração.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @param array $driverOptions Matriz de um ou mais pares chave => valor para definir os valores dos atributos para o objeto PDOStatement.
	 * @return PDOStatement
	 */
	public function prepare ($sql, $driverOptions=false) {
		if(!$driverOptions) $driverOptions=array();
		return self::$PDOInstance->prepare($sql, $driverOptions);
	}

	/**
	 * Executa uma instrução SQL, retornando um conjunto de resultados como um objeto PDOStatement.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return PDOStatement
	 */
	public function query($sql) {
		self::log($sql);
		return self::$PDOInstance->query($sql);
	}

	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchAllNum($sql) {
		self::log($sql);
		return self::$PDOInstance->query($sql)->fetchAll(PDO::FETCH_NUM);
	}
	
	/**
	 * Executa a consulta e retorna todas as linhas em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchAllAssoc($sql) {
		self::log($sql);
		return self::$PDOInstance->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Executa a consulta e retorna uma linha em ordem associativa.
	 *
	 * @param string $sql Instrução SQL válida para bancos de dados.
	 * @return array
	 */
	public function queryFetchRowAssoc($sql) {
		self::log($sql);
		return self::$PDOInstance->query($sql)->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Executa a consulta e seleciona apenas uma coluna.
	 *
	 * @param  string $sql Instrução SQL válida para bancos de dados.
	 * @return mixed
	 */
	public function queryFetchColAssoc($sql) {
		self::log($sql);
		return self::$PDOInstance->query($sql)->fetchColumn();
	}

	/**
	 * Coloca entre aspas expressões necessárias da consulta.
	 *
	 * @param string $input
	 * @param int $parameter_type
	 * @return string
	 */
	public function quote($input, $parameter_type=0) {
		return self::$PDOInstance->quote($input, $parameter_type);
	}

	/**
	 * Adiciona a cláusula LIMIT na consulta.
	 *
	 * @param mixed $sql
	 * @param int $count
	 * @param int $offset
	 * @return string
	 */
	public function limit($sql, $count, $offset = 0) {
	
		$count = intval($count);
	
		if ($count <= 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT count=$count não é válido");
		}
	
		$offset = intval($offset);
	
		if ($offset < 0) {
			throw new Exception('Class-'.__CLASS__.": o argumento LIMIT offset=$offset não é válido");
		}
	
		$sql .= " LIMIT $count";
	
		if ($offset > 0) {
			$sql .= " OFFSET $offset";
		}
	
		return $sql;
	}

	/**
	 * Lista as tabelas da base de dados.
	 *
	 * @return mixed|string
	 */
	public abstract function listTables($esquema = null);

	/**
	 * Lista os meta-dados da tabela.
	 *
	 * @param string $tabela
	 * @param string $esquema
	 * @return mixed|string
	 */
	public abstract function metadataTable($tabela, $esquema = null);
	
	/**
	 * Retorna o ID da última linha inserida ou valor de sequência.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public abstract function lastSequenceId($sequenceName);
	
	/**
	 * Gera um novo valor para sequência especificada e a retorna.
	 *
	 * @param String $sequenceName Nome da sequence da será devolvido o último identificador gerado.
	 * @return int
	 */
	public abstract function nextSequenceId($sequenceName);
	
	/**
	 * Retorna o ID da última linha inserida.
	 *
	 * @param string $tableName   OPTIONAL Nome da tabela.
	 * @param string $primaryKey  OPTIONAL Nome da coluna chave primaria.
	 * @return string
	 */
	public abstract function lastInsertId($tableName = null, $primaryKey = null, $esquema = null);
	
	/**
	 * Gera um log de consulta.
	 *
	 * @param string $sql
	 * @return void
	 */
	protected static function log($sql) {
		if(self::$log){
			Log::logSql($sql);
		}
	}
}