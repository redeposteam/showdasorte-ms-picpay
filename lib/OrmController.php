<?php
/**
 * ORM - Mapping.
 */

/**
 * Classe responsável por iniciar o processo de mapeamento ORM.
 *
 * @name		OrmController
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class OrmController extends ControllerRestful {

	/**
	 * Action principal
	 *
	 * @access public
	 */
	public function getAction() {

	    ini_set('max_execution_time', 0);
	    
		$config = App::getConfig();
		
		if($config['db']['orm'] == 1) {

			$tabelas = App::Db()->listTables($_REQUEST['esquema']);

			$orm = new Orm();
			
			$AppController = $config['mvc']['controller_app'];
			
			$this->gerarArquivo(APP_PATH.DS.'controller'.DS, $AppController.'Controller', $orm->createAppController($AppController));
			
			foreach ($tabelas as $item) {		

				$tabela = current($item);

				if(empty($_REQUEST['tabela']) || ($_REQUEST['tabela'] && $_REQUEST['tabela'] == $tabela)) {

					$metadata = App::Db()->metadataTable($tabela, $_REQUEST['esquema']);
			    
					$this->gerarArquivo(APP_PATH.DS.'abstract'.DS.'model'.DS, 'Abstract'.$orm->objectName($tabela), $orm->createAbstractModel('abstract_'.$tabela, $metadata));
					$this->gerarArquivo(APP_PATH.DS.'abstract'.DS.'controller'.DS, 'Abstract'.$orm->objectName($tabela).'Controller', $orm->createAbstractController('abstract_'.$tabela.'_controller', $AppController));
					$this->gerarArquivo(APP_PATH.DS.'abstract'.DS.'repository'.DS, 'Abstract'.$orm->objectName($tabela).'Repositorio', $orm->createAbstractRepository('abstract_'.$tabela.'_repositorio', $metadata));
						
					$this->gerarArquivo(APP_PATH.DS.'model'.DS, $orm->objectName($tabela), $orm->createModel($tabela, $metadata));
					$this->gerarArquivo(APP_PATH.DS.'controller'.DS, $orm->objectName($tabela).'Controller', $orm->createController($tabela, $metadata));
					$this->gerarArquivo(APP_PATH.DS.'repository'.DS, $orm->objectName($tabela).'Repositorio', $orm->createRepository($tabela, $metadata));
				}
			}
		}
	}	
	
	/**
	 * Método para geração dos arquivos PHP-ORM
	 *
	 * @access public
	 * @param string $diretorio Nome do diretórios destino
	 * @param string $nome Nome do arquivo
	 * @param string $conteudo Conteúdo do arquivo
	 */
	protected function gerarArquivo($diretorio, $nome, $conteudo) {

		if(!file_exists($diretorio.$nome.'.php')) {
			
			$handle = fopen($diretorio.$nome.'.php',"x");
			fwrite($handle,$conteudo);
			fclose($handle);
		
		}else{
			
			if(strpos($nome, 'Abstract') !== false) {
				unlink($diretorio.$nome.'.php');
				$handle = fopen($diretorio.$nome.'.php',"x");
				fwrite($handle,$conteudo);
				fclose($handle);
			}
		}
	}
}