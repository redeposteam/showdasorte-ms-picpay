<?php

/**
 *  Responsável pela formatação de valores com o uso de máscaras.
 *
 * @name		Formato
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 * @example
 * Entrada: '8134567889'
 * Saída:'(81)3456-7889'
 * formato($mascara, $string);
 * formato('(##)####-####', '8134567889');
 */

class Formato {

	const _LETRA  = '@';
	const _NUMERO = '#';
	const _TODOS1 = '%';
	const _TODOS2 = '*';

	/**
	 * Checa se o valor é um tipo inteiro.
	 * @param String $valor
	 * @return boolean
	 */
	public static function isInt($valor){
		
		return (preg_match( '|^\d*$|', $valor) == 1);
	}

	/**
	 * Checa se o dado é válido para uso.
	 * @param Mixed $valor
	 * @return boolean
	 */
	public static function dadoValido($valor) {
		
		$retorno = false;
		if(!is_null($valor) && $valor!='') {
			$retorno = true;
		}
		
		return $retorno;
	}
	
	/**
	 * Define que caractere vai ser lido, se o caracter da mascara
	 * ou o caracter do valor.
	 * @param String $caracter_mascara Caracter da mascara
	 * @param String $caracter Caracter a ser checado
	 * @return String
	 */
	public static function definirCaracter($caracter_mascara, $caracter){

		$retorno = '';
		
		switch ($caracter_mascara) {
			case self::_LETRA:
				if(!self::isInt($caracter) && $caracter != ' '){
					$retorno.= $caracter;
				}
			break;
			case self::_NUMERO:
				if(self::isInt($caracter) && $caracter != ' '){
					$retorno.= $caracter;
				}
			break;
			case self::_TODOS1: case self::_TODOS2: case $caracter:
				$retorno.= $caracter;
			break;
			default:
				$retorno.= $caracter_mascara;
			break;
		}

		return $retorno;
	}	

	/**
	 * Formata a string da maneira deseja a partir
	 * do formato da mascara passada como parâmetro.
	 * @param String $mascara Formato da mascara
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function formatar($mascara, $valor) {

		$retorno = '';
		$mask = array();
		$string = array();
		
		for ($i = 0; $i < strlen($mascara); $i++) {
			$mask[] = substr($mascara, $i, 1);
		}
		
		for ($i = 0; $i < strlen($valor); $i++) {
			$string[] = substr($valor, $i, 1);
		}
		
		for ($i = 0, $j = 0; $i < count($mask); $i++, $j++) {
			if($mask[$i]){
				$char = self::definirCaracter($mask[$i], $string[$j]);
				if($char != $string[$j]){
					$j--;
				}
				$retorno.=$char;
			}
		}

        return $retorno;
	}

	/**
	 * Calcula o byte a partir de divisões especificadas
	 * para atingir determinada casa decimal com precisão.
	 * @param int $valor Valor a ser dividido
	 * @param int $numeroDivisoes Quantidade de divisões
	 * @param int $precisao Precisão de casas decimais
	 * @return float
	 */
	public static function calcularByte($valor, $numeroDivisoes, $precisao = 2) {
		
		for ($i = 0; $i < $numeroDivisoes; $i++) {
			$valor = $valor / 1024;
		}
		
		$retorno = $valor;
		
		if(strpos($valor, '.') !== false){
			$parteInteira = substr($valor,0,strpos($valor, '.'));
			$parteDecimal = substr($valor,strpos($valor, '.')+1,$precisao); 
			$retorno = $parteInteira.'.'.$parteDecimal;
		}
		
		return (float) $retorno;
	}

	/**
	 * Formata a um número em bytes para um formato
	 * humano legível.
	 * @param int $bytes Quantidade de bytes
	 * @param int $precisao Precisão de casas decimais
	 * @return String
	 */
    public static function byte ($bytes, $precisao = 2) {
    
        $retorno = null;
        $label = "";
        $divisoes = 0;
        if (self::dadoValido($bytes)) {
            if (strlen($bytes) < 7) {
                $label = " KB";
                $divisoes = 1;
            } elseif (strlen($bytes) < 10 && strlen($bytes) > 6) {
                $label = " MB";
                $divisoes = 2;
            } elseif (strlen($bytes) < 13 && strlen($bytes) > 9) {
                $label = " GB";
                $divisoes = 3;
            } elseif (strlen($bytes) < 16 && strlen($bytes) > 12) {
                $label = " TB";
                $divisoes = 4;
            }
            $retorno = self::calcularByte($bytes, $divisoes, $precisao) . $label;
        }
        return $retorno;
    }
    
	/**
	 * Formata a string em um formato de CPF.
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function cpf($valor) {
		
		return self::formatar('###.###.###-##', $valor);
	}

	/**
	 * Formata a string em um formato de CNPJ.
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function cnpj($valor) {
		
		return self::formatar('##.###.###/####-##', $valor);
	}

	/**
	 * Formata a string em um formato de CEP.
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function cep($valor) {
		
		return self::formatar('#####-###', $valor);
	}

	/**
	 * Formata a string em um formato de fone
	 * sem o (ddd).
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function fone($valor) {
		
		return self::formatar('####-####', $valor);
	}

	/**
	 * Formata a string em um formato de fone
	 * com (ddd).
	 * @param String $valor Valor a ser formatado
	 * @return String
	 */
	public static function foneDDD($valor) {
		
		return self::formatar('(##)####-####', $valor);
	}
}