<?php

class OneSignal {

    const NOTIFICACAO_COMPRA_APROVADA  = 1;
    const NOTIFICACAO_COMPRA_PENDENTE  = 2;
    const NOTIFICACAO_COMPRA_CANCELADA = 3;
    
    const BASE_URL = "https://onesignal.com/api/v1/notifications";

    public function __construct() {

    }

    private function criarMensagemPorNotificacao($notificacao) {

        switch ($notificacao) {
            case self::NOTIFICACAO_COMPRA_APROVADA:
                return 'Sua contribuição foi aprovada!';
                break;
            case self::NOTIFICACAO_COMPRA_CANCELADA:
                return 'Sua contribuição foi cancelada!';
                break;
            case self::NOTIFICACAO_COMPRA_PENDENTE:
                return 'Sua contribuição está pendente!';
                break;

            default:
                return null;
                break;
        }
    }

    public function notificar($playerId, $base, $tipoNotificacao, $appId) {
        
        $mensagem = $this->criarMensagemPorNotificacao($tipoNotificacao);
        if ($mensagem === null) {
            return;
        }

        $content = array(
            "pt" => $mensagem,
            "en" => $mensagem
        );

        $small_icon = 'ic_stat_onesignal_'.$base;
        $fields = array(
            'app_id' => $appId,
            'include_player_ids' => array($playerId),
            'small_icon' => $small_icon,
            'data' => array("notificationType" => $tipoNotificacao),
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::BASE_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}