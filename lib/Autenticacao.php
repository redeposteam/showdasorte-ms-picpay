<?php

/**
 * Facilita o processo de autenticação de usuários.
 */

/**
 * Classe responsável pela autenticação de usuários.
 *
 * @name		Autenticacao
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Autenticacao {

	/**
	 * Váriável que guarda o nome da tabela a ser utilizada no processo de login.
	 *
	 * @static
	 * @access private
	 * @name string $tabela
	 */
	private $tabela;
	
	/**
	 * Váriável que guarda o nome da coluna a ser utilizada para senha da tabela.
	 *
	 * @static
	 * @access private
	 * @name string $colunaSenha
	 */
	private $colunaSenha;
	
	/**
	 * Váriável que guarda o nome da coluna a ser utilizada para usuário na tabela.
	 *
	 * @static
	 * @access private
	 * @name string $colunaUsuario
	 */
	private $colunaUsuario;

	/**
	 * Construtor padrão
	 */
	public function __construct() {
		
		$config = App::getConfig();
		$autenticacao 		 = $config['autenticacao'];
		$this->tabela 		 = $autenticacao['tabela'];
		$this->colunaSenha 	 = $autenticacao['colunaSenha'];
		$this->colunaUsuario = $autenticacao['colunaUsuario'];
	}
	
	/**
	 * Definir um atributo da conexão
	 *
	 * @param string $usuario Nome do usuário a ser autenticado.
	 * @param string $senha Senha do usuário a ser autenticado.
	 * @return bool
	 */
	public function login($usuario, $senha) {

		$ret = false;
		$db = App::Db();
		$dados = $db->queryFetchRowAssoc("SELECT * FROM ".$this->tabela." WHERE ".$this->colunaUsuario." = ".$db->quote($usuario));
		if($senha == $dados[$this->colunaSenha]) {
			$ret = true;
		}
		return $ret;
	}

}