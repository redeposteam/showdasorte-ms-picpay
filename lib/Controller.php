<?php

/**
 * Roteamento MVC.
 */

/**
 * Classe responsável pelo roteamento de requisições MVC.
 *
 * @name		Controller
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Controller {

	/**
	 * Método a ser chamado antes de qualquer outro método da classe.
	 * 
	 * @param mixed
	 * @return mixed
	 */
	public function ini() {

	}

	/**
	 * Construtor padrão
	 */
	public function __construct() {

		$this->ini();
	}
}