<?php

/**
 * ORM - Respository
 */

/**
 * Classe responsável por trazer dados de uma ou mais tabelas.
 *
 * @name		Repository
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Db
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Repository extends Select
{

	/**
	 * @var array|string Chave(s) primária(s)
	 */
	protected $pk = null;

	/**
	 * @var array|string Chave(s) estrangeira(s)
	 */
	protected $fk = array();

	/**
	 * @var string Campos da tabela
	 */
	protected $campos = array();

	/**
	 * @var string Nome da tabela
	 */
	protected $tabela = null;

	/**
	 * @var string Esquema da tabela
	 */
	protected $esquema = null;

	/**
	 * @var string Sequence da tabela
	 */
	protected $sequence = null;

	/**
	 * @var string Campo utilizado no próximo Join
	 */
	protected $_keyJoin_ = null;

	/**
	 * @var string Colunas selecionadas
	 */
	protected $_columns_ = array();

	/**
	 * Construtor padrão.
	 *
	 * @return void
	 */
	public function __construct()
	{

		parent::__construct();
		$this->from((!is_null($this->esquema) ? $this->esquema . '.' : '') . $this->tabela);
	}

	/**
	 * Retorna a chave primaria da tabela que se conecta.
	 *
	 * @return array $pk
	 */
	public function getPk()
	{

		return $this->pk;
	}

	/**
	 * Retorna o nome da tabela que se conecta.
	 *
	 * @return string $tabela
	 */
	public function getTabela()
	{

		return $this->tabela;
	}

	/**
	 * Seta a chave de junção do objeto para o próximo JOIN.
	 * 
	 * @param string $campo
	 * @return void
	 */
	protected function keyJoin($campo)
	{

		$this->_keyJoin_ = $campo;
	}

	/**
	 * Obtém a chave de junção do objeto para o próximo JOIN.
	 *
	 * @return void
	 */
	protected function getKeyJoin()
	{

		return $this->_keyJoin_;
	}

	/**
	 * Adiciona um atributo para ser selecionado na cláusula SQL.
	 *
	 * @param string $column
	 * @param string $alias
	 * @return void
	 */
	public function addCol($column, $alias = null)
	{

		$this->addColumm($column, $alias);
	}

	/**
	 * Deleta todos os atributos que foram selecionadas para cláusula SQL.
	 *
	 * @return void
	 */
	public function delCols()
	{

		$this->delColumms();
	}

	/**
	 * Implementacao real de @see addCol.
	 *
	 * @param string $campo
	 * @param string $alias 
	 * @return void
	 */
	protected function addColumm($campo, $alias = null)
	{

		if (count($this->_columns_) == 0 && count($this->getPart(Select::COLUMNS)) == 1) {
			$this->resetCols();
		}
		if (is_null($alias)) {
			$this->_columns_[] = $campo;
		} else {
			$this->_columns_[$alias] = $campo;
		}
	}

	/**
	 * Exclui uma coluna da selecão.
	 *
	 * @param string $campo
	 * @return void
	 */
	protected function delColumm($campo)
	{

		$new = array();
		foreach ($this->_columns_ as $key => $value) {
			if (!($key == $campo || $value == $campo)) {
				$new[$key] = $value;
			}
		}
		$this->_columns_ = $new;
	}

	/**
	 * Implementacao real de @see delCols.
	 *
	 * @return void
	 */
	protected function delColumms()
	{

		$this->_columns_ = array();
	}

	/**
	 * Seta atributos de um objeto.
	 *
	 * @param Object $object Objeto a ser setado os dados.
	 * @param array $properties Valores dos atributos.
	 * @return Object
	 */
	public function setProperties(&$object, $properties)
	{

		$reflection = new ReflectionObject($object);
		$propriedades = $reflection->getProperties();
		foreach ($propriedades as $propriedade) {
			if (isset($properties[$propriedade->getName()])) {
				$propriedade->setAccessible(true);
				$propriedade->setValue($object, $properties[$propriedade->getName()]);
			}
		}
	}

	/**
	 * Extrai atributos e valores de um objeto.
	 *
	 * @param stdClass $object Objeto a ser setado os dados.
	 * @return array Valores dos atributos.
	 */
	public function getProperties(&$object)
	{

		$properties = array();
		$reflection = new ReflectionObject($object);
		$propriedades = $reflection->getProperties();
		foreach ($propriedades as $propriedade) {
			$propriedade->setAccessible(true);
			$properties[$propriedade->getName()] = $propriedade->getValue($object);
		}

		return $properties;
	}

	/**
	 * Seleciona um objeto da base de dados.
	 *
	 * @param Object $object Objeto a ser selecionado utilizando sua pk.
	 * @param bool $orWhere Define se a seleção de dados utilizará AND ou OR.
	 * @return Model Objeto selecionado.
	 */
	public function selecionar($object, $orWhere = false)
	{

		$this->reset(Select::WHERE);
		$tabela = $this->getPart(Select::FROM);
		if (empty($tabela)) {
			$this->from($this->tabela, '*', $this->esquema);
		}

		$properties = $this->getProperties($object);
		$filtros = array_filter($properties, function ($value) {
			return ($value != null);
		});

		if (count($filtros) > 0) {
			if ($orWhere) {
				foreach ($filtros as $filtro => $valor) {
					$this->orWhere($filtro . " = '" . $valor . "'");
				}
			} else {
				foreach ($filtros as $filtro => $valor) {
					$this->where($filtro . " = '" . $valor . "'");
				}
			}
			$dados = $this->query()->fetch();
			if ($dados != null && $dados !== false) {
				$this->setProperties($object, $dados);
			} else {
				$object = null;
			}
		} else {
			throw new Exception(Util::utf8D('Class-' . __class__ . ': Method-' . __FUNCTION__ . ': É necessário informar pelo menos um atributo/filtro.'));
		}

		return $object;
	}

	/**
	 * Inseri um objeto na base de dados.
	 *
	 * @param Model $object Objeto a ser inserido.
	 * @return int Id gerado para o objeto na base de dados.
	 */
	public function inserir($object)
	{

		$_valores_ = array();
		$_campos_ = array();
		$sql = 'INSERT INTO ' . (!is_null($this->esquema) ? $this->esquema . '.' : '') . $this->tabela;
		$registro = $this->getProperties($object);
		/*
		$sequence = $this->db->nextSequenceId((!is_null($this->esquema) ? $this->esquema.'.' : '').$this->sequence);
		if(!is_null($sequence)) {
			$registro[current($this->pk)] = $this->db->nextSequenceId((!is_null($this->esquema) ? $this->esquema.'.' : '').$this->sequence);
		}
		 */
		$sequence = $registro[current($this->pk)] = $this->db->nextSequenceId((!is_null($this->esquema) ? $this->esquema . '.' : '') . $this->sequence);
		foreach ($registro as $campo => $valor) {
			if (in_array($campo, $this->campos)) {
				if (!empty($valor) || (is_numeric($valor) && $valor == 0)) {
					$_campos_[] = $campo;
					$_valores_[] = "'" . addslashes($valor) . "'";
				}
			}
		}
		$sql .= " (" . implode(',', $_campos_) . ") VALUES (" . implode(',', $_valores_) . ")";

		$success = $this->db->exec($sql);

		if (!$success) {
			$error = $this->db->errorInfo();
			throw new Exception($error['message'], $error['code']);
		}
		return $sequence;//$this->db->lastInsertId($this->tabela, $this->pk, $this->esquema, $this->sequence);
	}

	/**
	 * Atualiza um objeto da base de dados.
	 *
	 * @param Model $object Objeto a ser atualizado.
	 * @return int Número de linhas afetadas.
	 */
	public function atualizar($object)
	{

		$_campos_ = array();
		$sql = 'UPDATE '.(!is_null($this->esquema) ? $this->esquema.'.' : '').$this->tabela.' SET ';
		$registro = $this->getProperties($object);
		foreach($registro as $campo => $valor) {
			if(in_array($campo, $this->campos)) {
				if(!empty($valor) || (is_numeric($valor) && $valor == 0)){
					if(!in_array($campo, $this->pk)) {
						if(in_array($campo, array('_excluido'))) {
							$_campos_[]= '"'.strtoupper($campo).'"'."='".addslashes($valor)."'";
						} else {
							$_campos_[]= $campo."='".addslashes($valor)."'";
						}
					}
				}
			}
		}		
		$sql .= implode(',', $_campos_) . ' WHERE ';
		$where = array();
		foreach ($this->pk as $pk) {
			$where[] = $pk . ' = ' . $registro[$pk];
		}
		$sql .= implode(' AND ', $where);		
		
		$success = $this->db->exec($sql);		

		if (!$success) {
			$error = $this->db->errorInfo();
			throw new Exception($error['message'], $error['code']);
		}

		return $success;
	}

	/**
	 * Exclui um objeto da base de dados.
	 *
	 * @param Model $object Objeto a ser excluido.
	 * @return int Número de linhas afetadas.
	 */
	public function excluir($object)
	{

		$registro = $this->getProperties($object);
		$where = array();
		foreach ($this->pk as $pk) {
			$where[] = $pk . ' = ' . $registro[$pk];
		}
		$sql = 'DELETE FROM ' . (!is_null($this->esquema) ? $this->esquema . '.' : '') . $this->tabela . ' WHERE ' . implode(' AND ', $where);

		$success = $this->db->exec($sql);

		if (!$success) {
			$error = $this->db->errorInfo();
			throw new Exception($error['message'], $error['code']);
		}

		return $success;
	}

	/**
	 * Retorna objetos da base de dados.
	 *
	 * @param Model $object Objeto a ser utilizado como parametro de consultas.
	 * @param bool $orWhere Define se a seleção de dados utilizará AND ou OR.
	 * @param int $count Contador.
	 * @param int $offset A partir de.
	 * @param bool $objectFormat Define se será um retorno em formato de objeto.
	 * @return Model[] Número de linhas afetadas.
	 */
	public function todos($object, $orWhere = true, $count = null, $offset = 0, $objectFormat = false)
	{

		$tabela = $this->getPart(Select::FROM);
		if (empty($tabela)) {
			$this->from($this->tabela, '*', $this->esquema);
		}

		if (!empty($object)) {
			$parametros = $this->getProperties($object);

			foreach ($parametros as $field => $value) {
				if (!empty($value)) {
					if ($orWhere) {
						if ((is_int($value) || ctype_digit($value))) {
							$this->orWhere($field . " = '" . $value . "'");
						} else {
							$this->orWhere($field . " LIKE '%" . $value . "%'");
						}
					} else {
						if ((is_int($value) || ctype_digit($value))) {
							$this->where($field . " = '" . $value . "'");
						} else {
							$this->where($field . " LIKE '%" . $value . "%'");
						}
					}
				}
			}
		}

		if (!empty($count)) {
			$this->limit($count, $offset);
		}

		$data = null;
		$records = $this->query()->fetchAll();

		if ($objectFormat) {
			$data = array();
			$class = get_class($object);
			foreach ($records as $record) {
				$obj = new $class();
				$this->setProperties($obj, $record);
				$data[] = $obj;
			}
		} else {
			$data = $records;
		}

		return $data;
	}

	/**
	 * Reseta as colunas da cláusula Select.
	 *
	 * @return void
	 */
	protected function resetCols()
	{

		$this->reset(Select::COLUMNS);
	}

	/**
	 * Implementação real das clásulas Join.
	 * 
	 * @param Repository $repositorio1
	 * @param Repository $repositorio2
	 * @return void
	 */
	protected function _joinObject(Repository $repositorio1, Repository $repositorio2 = null)
	{

		if (empty($repositorio2)) {

			$condition1 = $repositorio1->getTabela() . '.' . $repositorio1->getKeyJoin();
			$condition2 = $this->getTabela() . '.' . $this->getKeyJoin();
			$properties = $this->getProperties($repositorio1);

		} else {

			$condition1 = $repositorio2->getTabela() . '.' . $repositorio2->getKeyJoin();
			$condition2 = $repositorio1->getTabela() . '.' . $repositorio1->getKeyJoin();
			$properties = $this->getProperties($repositorio2);
		}

		$cols = array();
		if (!empty($properties['_columns_'])) {
			$cols = $properties['_columns_'];
		}

		return array($condition1, $condition2, $cols);
	}

	/**
	 * Cria um Join.
	 *
	 * @param Repository $repositorio1
	 * @param Repository $repositorio2
	 * @return void
	 */
	public function joinObject(Repository $repositorio1, Repository $repositorio2 = null)
	{

		$array = $this->_joinObject($repositorio1, $repositorio2);

		$condition1 = $array[0];
		$condition2 = $array[1];
		$cols = $array[2];

		$this->join(($repositorio2 ? $repositorio2->getTabela() : $repositorio1->getTabela()), $condition1 . ' = ' . $condition2, $cols);
	}

	/**
	 * Cria um Left Join.
	 *
	 * @param Repository $repositorio1
	 * @param Repository $repositorio2
	 * @return void
	 */
	public function joinLeftObject(Repository $repositorio1, Repository $repositorio2 = null)
	{

		$array = $this->_joinObject($repositorio1, $repositorio2);

		$condition1 = $array[0];
		$condition2 = $array[1];
		$cols = $array[2];

		$this->joinLeft(($repositorio2 ? $repositorio2->getTabela() : $this->getTabela()), $condition1 . ' = ' . $condition2, $cols);
	}

	/**
	 * Cria um Right Join.
	 *
	 * @param Repository $repositorio1
	 * @param Repository $repositorio2
	 * @return void
	 */
	public function joinRightObject(Repository $repositorio1, Repository $repositorio2 = null)
	{

		$array = $this->_joinObject($repositorio1, $repositorio2);

		$condition1 = $array[0];
		$condition2 = $array[1];
		$cols = $array[2];

		$this->joinRight(($repositorio2 ? $repositorio2->getTabela() : $this->getTabela()), $condition1 . ' = ' . $condition2, $cols);
	}

	/**
	 * Cria um Where usando os valores dos atributos do Model.
	 *
	 * @param Repository $repositorio1
	 * @param Repository $repositorio2
	 * @param string $operator
	 * @return void
	 */
	public function whereInnerObject(Repository $repositorio1, Repository $repositorio2, $operator = ' = ')
	{

		$this->where($repositorio1->getTabela() . '.' . $repositorio1->getKeyJoin() . $operator . $repositorio2->getTabela() . '.' . $repositorio2->getKeyJoin());
	}

	/**
	 * Cria um Where usando os valores dos atributos do Model.
	 *
	 * @param Repository $repositorio
	 * @param Model $model
	 * @param string $operator
	 * @return void
	 */
	public function whereObject(Repository $repositorio, Model $model, $operator = '=')
	{

		$properties = $this->getProperties($model);
		foreach ($properties as $key => $value) {
			if (!is_null($value)) {
				$this->where($repositorio->getTabela() . '.' . $key . " " . $operator . " '" . $value . "'");
			}
		}
	}

	/**
	 * Cria um OR Where usando os valores dos atributos do Model.
	 *
	 * @param Repository $repositorio
	 * @param Model $model
	 * @param string $operator
	 * @return void
	 */
	public function orWhereObject(Repository $repositorio, Model $model, $operator = '=')
	{

		$properties = $this->getProperties($model);
		foreach ($properties as $key => $value) {
			if (!is_null($value)) {
				$this->orWhere($repositorio->getTabela() . '.' . $key . " " . $operator . " '" . $value . "'");
			}
		}
	}

	/**
	 * Cria um Order usando os valores dos atributos do Model.
	 *
	 * @param Repository $repositorio
	 * @param Model $model
	 * @param string $direction 
	 * @return void
	 */
	public function orderObject(Repository $repositorio, Model $model, $direction = 'ASC')
	{

		$properties = $this->getProperties($model);
		foreach ($properties as $key => $value) {
			if (!is_null($value)) {
				$this->order($repositorio->getTabela() . '.' . $key . " $direction");
			}
		}
	}

	/**
	 * Cria um Group usando os valores dos atributos do Model.
	 *
	 * @param Repository $repositorio
	 * @param Model $model
	 * @return void
	 */
	public function groupObject(Repository $repositorio, Model $model)
	{

		$properties = $this->getProperties($model);
		foreach ($properties as $key => $value) {
			if (!is_null($value)) {
				$this->group($repositorio->getTabela() . '.' . $key);
			}
		}
	}

	/**
	 * Retorna o SQL final.
	 *
	 * @return string Consulta SQL
	 */
	public function __toString()
	{

		if (count($this->_columns_) > 0) {
			$columns = $this->_parts[self::COLUMNS];
			$this->_parts[self::COLUMNS] = null;
			$this->_tableCols($this->tabela, $this->_columns_);
			foreach ($columns as $column) {
				$this->_tableCols($column[0], $column[1]);
			}
		}
		return parent::__toString();
	}
}
