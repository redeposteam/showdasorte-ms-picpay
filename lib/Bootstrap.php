<?php

/**
 * Roteador de requisições do sistema.
 */

/**
 * Classe responsável pelo roteamento controllers e actions.
 *
 * @name		Bootstrap
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Bootstrap {

	const RESTFUL_GROUP = 'group';

	/**
	 * @var string Caminho base da aplicação
	 */
	private static $basePath;

	/**
	 * Inclui o arquivo da classe solicitado
	 * 
	 * @param string $module
	 * @param string $controller
	 */
	private static function includeClass($module = null, $controller) {

		$file = '';
		
		if($controller == 'Orm') {
			
			$file = LIB_PATH.DS.$controller.'Controller.php';
			
		} else {
			
			if($module) {
				$file = APP_PATH.DS.$module.DS.CONTROLLER_DIR.DS.$controller.'Controller.php';
			} else {
				$file = APP_PATH.DS.CONTROLLER_DIR.DS.$controller.'Controller.php';
			}
		}
		
		if(file_exists($file)) {
			include $file;
		} else {
			if(empty($controller)) {
				throw new Exception('Recom - Restful Controller Model Framework', 0);
			} else {
				throw new Exception(Util::utf8D('Class-'.__CLASS__.': Method-'.__FUNCTION__.': Arquivo de classe não encontrado.'));
			}
		}
	}
	
	/**
	 * Seleciona o diretório atual
	 * 
	 * @param string $module
	 * @param string $controller
	 */
	private static function diretorioAtual($module = null, $controller) {

		if($module) {
			$diretorio = APP_PATH.DS.$module.DS.CONTROLLER_DIR.DS;
		} else {
			$diretorio = APP_PATH.DS.CONTROLLER_DIR.DS;
		}
		
		Diretorio::$currentDirectory = $diretorio.'..'.DS.'view'; 
	}

	/**
	 * Capitaliza as primeiras letras das palavras
	 * 
	 * @param string $text
	 * @param bool $force
	 * @return string
	 */
	private static function capitalize($text, $force = false) {

		$words = explode('-', $text);
		$text = '';
		if(count($words) > 1 || $force) {
			foreach ($words as $word) {
				$text.= ucfirst($word);
			}
		} else {
			$text = current($words);
		}
		
		return $text;
	}
	
	/**
	 * Redireciona a resuisição para o Controller e Action solicitados
	 * 
	 * @param string $module
	 * @param string $controller
	 * @param string $action
	 */
	private static function redirect($module = null, $controller, $action = null) {

		try{
			
			$action = self::capitalize($action);
			$controller = self::capitalize($controller, true);
			self::includeClass($module, $controller);
			self::diretorioAtual($module, $controller);
			$controllerName = $controller.'Controller';
			$class = new $controllerName($action);
			
			$Method0 = new ReflectionMethod($controller.'Controller', $action.'Action');
			
			/**
			 * Método chamado antes de qualquer método da classe, com exceção do __construct
			 */
			if(method_exists($class, '_beforeCall')) {
				$Method1 = new ReflectionMethod($controller.'Controller', '_beforeCall');
				$Method1->setAccessible(true);
				$Method1->invoke($class);
			}
			
			/**
			 * Método principal a ser chamado
			 */
			$Method0->invoke($class);

			/**
			 * Método chamado depois de qualquer método da classe
			 */
			if(method_exists($class, '_afterCall')) {
				$Method2 = new ReflectionMethod($controller.'Controller', '_afterCall');
				$Method2->setAccessible(true);
				$Method2->invoke($class);
			}
			
		} catch (Exception $e) {

			if(MVC_ON) {
				throw new Exception($e->getMessage(),$e->getCode());
			} else {
				$Restful = new Restful();
				$status = ControllerRestful::HTTP_BAD_REQUEST;
				if($e->getMessage() == 0){
					//echo $Restful->response($status, array('status' => $status, 'message' => $Restful->getHttpStatusMessage($status), 'msg' => $e->getMessage()));
				} else {
					$metodo = preg_replace('@Method|Controller|Action|\(\)@', '', $e->getMessage());
					//echo $Restful->response($status, array('status' => $status, 'message' => $Restful->getHttpStatusMessage($status), 'err' => ControllerRestful::ERROR_SERVICE, 'msg' => $metodo));
					throw new Exception(Util::utf8E($e->getMessage()));
				}
			}
		}
	}

	/**
	 * Devolve a URL base do sistema
	 *
	 * @return string
	 */
	private static function baseUrl() {

		if(strpos($_SERVER['REQUEST_URI'], '?') !== false) {		
			$params = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'],'?')+1, strlen($_SERVER['REQUEST_URI']));
			if($params) {
				$vars = explode('&', $params);
				foreach ($vars as $var) {
					$var = explode('=',$var);
					$_GET[$var[0]] = $var[1];
				}
			}
		}
		
		$uri = (strpos($_SERVER['REQUEST_URI'], '?') !== false ? explode('/',substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'],'?'))) : explode('/',$_SERVER['REQUEST_URI']));
		$scr = explode('/',$_SERVER['SCRIPT_NAME']);

		for($i = 0 ; $i < count($scr) ; $i++) {
			if ($uri[$i] == $scr[$i]) {
				unset($uri[$i]);
			}
		}

		$tmp = '';
		self::$basePath = '';
		for($i = 0 ; $i < (count($uri)-1); $i++) {
			$tmp.= '../';
		}
		self::$basePath = $tmp;
		return implode('/',$uri);
	}
	
	/**
	 * Dispacha o pedido da requisição
	 */
	public static function dispatch() {

		$module     = null;
		$controller = null;
		$action     = null;
		$uri		= self::baseUrl();
		
		if ($uri != '/') {
			$string = explode('/', $uri);
			
			if(MVC_ON) {
				if(MODULE_ACTIVE) {
					$module     = $string[0];
					$controller = $string[1];
					$action     = (isset($string[2]) ? $string[2] : null);
				} else {
					$controller = (isset($string[0]) ? $string[0] : null);
					$action     = (isset($string[1]) ? $string[1] : null);
				}
			} else {
				
				$partes = explode('/', $uri);
				$actions = array(strtolower($_SERVER['REQUEST_METHOD']));
				
				switch ($partes[0]) {
					
					case self::RESTFUL_GROUP:
					
						for ($i = 0; $i < count($partes); $i++) {
							if($i == 1) {
								$controller = $partes[$i];
							} else if($i == 0) {
								$action = $partes[$i];
							} else {
								if($i % 2 == 0) {
									$valor = $partes[$i];									
								} else {
									$_GET[$partes[$i]] = $valor;
								}
							}
						}
						
					break;
					default:
						
						for ($i = 0; $i < count($partes); $i++) {
							if($i == 0) {
								$controller = $partes[$i];
							} else {
								if($i % 2 == 0) {
									$actions[] = $partes[$i];
								} else {
									$_GET['id'][] = $partes[$i];
								}
							}
						}
						$action = implode('-', $actions);
						
					break;
				}
			}
		}
		
		if(MVC_ON) {

			if (empty($action)) {
				$action = 'index';
			}
			if (empty($controller)) {
				$controller = 'index';
			}
		}
		
		self::redirect($module, $controller, $action);
	}
}