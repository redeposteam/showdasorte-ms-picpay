<?php

class ConfiguracaoVendasPicpay {

    const DEV = 'DEV';
    const PRO = 'PROD';
    const STG = 'STAGE';

    protected static $_EMPRESAS = array (

        self::STG => array(
                           69 => array('UF'=>'L1', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L1'),
                           70 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           71 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                            0 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                            0 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
                           ),
        self::PRO => array(
                           59 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           60 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           61 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                           62 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                           63 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
        ),
        self::DEV => array(
                          197 => array('UF'=>'L1', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L1'),
                          199 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                          200 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                          201 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                          202 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
        )
    );

    protected static $_TIPO_LOTERIA = array (

        self::STG => array(
                           410 => array('UF'=>'L1', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L1'),
                           450 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           430 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                           000 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                           000 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
                           ),
        self::PRO => array(
                           400 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           401 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           402 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                           000 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                           000 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
        ),
        self::DEV => array(
                           400 => array('UF'=>'L1', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L1'),
                           401 => array('UF'=>'L2', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L2'),
                           402 => array('UF'=>'L3', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L3'),
                           410 => array('UF'=>'L4', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L4'),
                           411 => array('UF'=>'L5', 'DESCRICAO_APLICACAO' => 'Show da Sorte' ,'IDENTIFICADOR_APLICACAO' => 'L5'),
        )
    );

    public static function getPorEmpresa($idEmpresa) {
        $config = App::getConfig();
        return ConfiguracaoVendasPicpay::$_EMPRESAS[$config['db']['db']][$idEmpresa];
    }

    public static function getPorTipoLoteria($idTipoLoteria) {
        $config = App::getConfig();
        return ConfiguracaoVendasPicpay::$_TIPO_LOTERIA[$config['db']['db']][$idTipoLoteria];
    }
}