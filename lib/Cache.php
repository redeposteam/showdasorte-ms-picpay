<?php

/**
 * Criação de cache.
 */

/**
 * Classe responsável pela geração de Caches.
 * 
 * @name		Cache
 * @version		1.0
 * @access		public
 * @package		Remcom
 * @subpackage	Lib
 * @copyright	Copyright (c) 2016, RM Sistemas de Tecnologia LTDA.
 * @author		Ricardo Minzé <ricardominze@gmail.com>
 */
class Cache {
    
    protected $time;
    protected $path;
    protected $cacheId;
    const CACHE_TIME = '__cache__time__';
    
    public function __construct($cacheId, $path, $time) {
        
        $this->time = $time;
        $this->path = (!is_null($path) ? $path : sys_get_temp_dir());
        $this->cacheId = $cacheId;
    }

    protected function writeFile($content) {
        
        if(!isset($content[self::CACHE_TIME])) {
            $content[self::CACHE_TIME] = time();
        }
        if(is_dir($this->path)) {
            $fp = fopen($this->path.DS.$this->cacheId, 'w');
            fwrite($fp, json_encode(Util::utf8E($content)));
            fclose($fp);
        } else {
            throw new Exception('Diretório não encontrado para criação do arquivo');
        }
    }
    
    protected function readFile() {
        
        $file = $this->path.DS.$this->cacheId;
        if(file_exists($file)) {
            return Util::utf8D(json_decode(file_get_contents($file), true));
        } else {
            throw new Exception('Arquivo não encontrado (read)');
        }
    }
    
    protected function deleteFile() {
        
        $file = $this->path.DS.$this->cacheId;
        if(file_exists($file)) {
            unlink($file);
        } else {
            throw new Exception('Arquivo não encontrado (delete)');
        }
    }

    public function expired() {

        $expired = false;
        if (file_exists($this->path.DS.$this->cacheId)) {
            $content = $this->readFile();
            if(((int) $content[self::CACHE_TIME] + $this->time) < time()) {
                $expired = true;
                $this->deleteFile($this->path.DS.$this->cacheId);
            }
        } else {
            $expired = true;
        }
        return $expired;
    }

    public function set($key, $val) {

        try{
            $content = $this->readFile();
        } catch(Exception $e) {
            $content = array();
        }
        $content[$key] = $val;
        $this->writeFile($content);
    }   

    public function get($key) {

        $value = null;
        try{
            $content = $this->readFile();
            $value = $content[$key];
        } catch(Exception $e) {
            $value = null;
        }
        return $value;
    }   
}